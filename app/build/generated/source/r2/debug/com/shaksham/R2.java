// Generated code from Butter Knife gradle plugin. Do not modify!
package com.shaksham;

import androidx.annotation.AnimRes;
import androidx.annotation.AttrRes;
import androidx.annotation.BoolRes;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.IntegerRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.MenuRes;
import androidx.annotation.PluralsRes;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import androidx.annotation.StyleableRes;

public final class R2 {
  public static final class anim {
    @AnimRes
    public static final int abc_fade_in = 0x7f010000;

    @AnimRes
    public static final int abc_fade_out = 0x7f010001;

    @AnimRes
    public static final int abc_grow_fade_in_from_bottom = 0x7f010002;

    @AnimRes
    public static final int abc_popup_enter = 0x7f010003;

    @AnimRes
    public static final int abc_popup_exit = 0x7f010004;

    @AnimRes
    public static final int abc_shrink_fade_out_from_bottom = 0x7f010005;

    @AnimRes
    public static final int abc_slide_in_bottom = 0x7f010006;

    @AnimRes
    public static final int abc_slide_in_top = 0x7f010007;

    @AnimRes
    public static final int abc_slide_out_bottom = 0x7f010008;

    @AnimRes
    public static final int abc_slide_out_top = 0x7f010009;

    @AnimRes
    public static final int abc_tooltip_enter = 0x7f01000a;

    @AnimRes
    public static final int abc_tooltip_exit = 0x7f01000b;

    @AnimRes
    public static final int btn_checkbox_to_checked_box_inner_merged_animation = 0x7f01000c;

    @AnimRes
    public static final int btn_checkbox_to_checked_box_outer_merged_animation = 0x7f01000d;

    @AnimRes
    public static final int btn_checkbox_to_checked_icon_null_animation = 0x7f01000e;

    @AnimRes
    public static final int btn_checkbox_to_unchecked_box_inner_merged_animation = 0x7f01000f;

    @AnimRes
    public static final int btn_checkbox_to_unchecked_check_path_merged_animation = 0x7f010010;

    @AnimRes
    public static final int btn_checkbox_to_unchecked_icon_null_animation = 0x7f010011;

    @AnimRes
    public static final int btn_radio_to_off_mtrl_dot_group_animation = 0x7f010012;

    @AnimRes
    public static final int btn_radio_to_off_mtrl_ring_outer_animation = 0x7f010013;

    @AnimRes
    public static final int btn_radio_to_off_mtrl_ring_outer_path_animation = 0x7f010014;

    @AnimRes
    public static final int btn_radio_to_on_mtrl_dot_group_animation = 0x7f010015;

    @AnimRes
    public static final int btn_radio_to_on_mtrl_ring_outer_animation = 0x7f010016;

    @AnimRes
    public static final int btn_radio_to_on_mtrl_ring_outer_path_animation = 0x7f010017;

    @AnimRes
    public static final int design_bottom_sheet_slide_in = 0x7f010018;

    @AnimRes
    public static final int design_bottom_sheet_slide_out = 0x7f010019;

    @AnimRes
    public static final int design_snackbar_in = 0x7f01001a;

    @AnimRes
    public static final int design_snackbar_out = 0x7f01001b;

    @AnimRes
    public static final int mtrl_bottom_sheet_slide_in = 0x7f01001c;

    @AnimRes
    public static final int mtrl_bottom_sheet_slide_out = 0x7f01001d;

    @AnimRes
    public static final int mtrl_card_lowers_interpolator = 0x7f01001e;

    @AnimRes
    public static final int nav_default_enter_anim = 0x7f01001f;

    @AnimRes
    public static final int nav_default_exit_anim = 0x7f010020;

    @AnimRes
    public static final int nav_default_pop_enter_anim = 0x7f010021;

    @AnimRes
    public static final int nav_default_pop_exit_anim = 0x7f010022;

    @AnimRes
    public static final int slide_in = 0x7f010023;
  }

  public static final class attr {
    @AttrRes
    public static final int action = 0x7f030000;

    @AttrRes
    public static final int actionBarDivider = 0x7f030001;

    @AttrRes
    public static final int actionBarItemBackground = 0x7f030002;

    @AttrRes
    public static final int actionBarPopupTheme = 0x7f030003;

    @AttrRes
    public static final int actionBarSize = 0x7f030004;

    @AttrRes
    public static final int actionBarSplitStyle = 0x7f030005;

    @AttrRes
    public static final int actionBarStyle = 0x7f030006;

    @AttrRes
    public static final int actionBarTabBarStyle = 0x7f030007;

    @AttrRes
    public static final int actionBarTabStyle = 0x7f030008;

    @AttrRes
    public static final int actionBarTabTextStyle = 0x7f030009;

    @AttrRes
    public static final int actionBarTheme = 0x7f03000a;

    @AttrRes
    public static final int actionBarWidgetTheme = 0x7f03000b;

    @AttrRes
    public static final int actionButtonStyle = 0x7f03000c;

    @AttrRes
    public static final int actionDropDownStyle = 0x7f03000d;

    @AttrRes
    public static final int actionLayout = 0x7f03000e;

    @AttrRes
    public static final int actionMenuTextAppearance = 0x7f03000f;

    @AttrRes
    public static final int actionMenuTextColor = 0x7f030010;

    @AttrRes
    public static final int actionModeBackground = 0x7f030011;

    @AttrRes
    public static final int actionModeCloseButtonStyle = 0x7f030012;

    @AttrRes
    public static final int actionModeCloseDrawable = 0x7f030013;

    @AttrRes
    public static final int actionModeCopyDrawable = 0x7f030014;

    @AttrRes
    public static final int actionModeCutDrawable = 0x7f030015;

    @AttrRes
    public static final int actionModeFindDrawable = 0x7f030016;

    @AttrRes
    public static final int actionModePasteDrawable = 0x7f030017;

    @AttrRes
    public static final int actionModePopupWindowStyle = 0x7f030018;

    @AttrRes
    public static final int actionModeSelectAllDrawable = 0x7f030019;

    @AttrRes
    public static final int actionModeShareDrawable = 0x7f03001a;

    @AttrRes
    public static final int actionModeSplitBackground = 0x7f03001b;

    @AttrRes
    public static final int actionModeStyle = 0x7f03001c;

    @AttrRes
    public static final int actionModeWebSearchDrawable = 0x7f03001d;

    @AttrRes
    public static final int actionOverflowButtonStyle = 0x7f03001e;

    @AttrRes
    public static final int actionOverflowMenuStyle = 0x7f03001f;

    @AttrRes
    public static final int actionProviderClass = 0x7f030020;

    @AttrRes
    public static final int actionTextColorAlpha = 0x7f030021;

    @AttrRes
    public static final int actionViewClass = 0x7f030022;

    @AttrRes
    public static final int activityChooserViewStyle = 0x7f030023;

    @AttrRes
    public static final int alertDialogButtonGroupStyle = 0x7f030024;

    @AttrRes
    public static final int alertDialogCenterButtons = 0x7f030025;

    @AttrRes
    public static final int alertDialogStyle = 0x7f030026;

    @AttrRes
    public static final int alertDialogTheme = 0x7f030027;

    @AttrRes
    public static final int allowStacking = 0x7f030028;

    @AttrRes
    public static final int alpha = 0x7f030029;

    @AttrRes
    public static final int alphabeticModifiers = 0x7f03002a;

    @AttrRes
    public static final int animationMode = 0x7f03002b;

    @AttrRes
    public static final int appBarLayoutStyle = 0x7f03002c;

    @AttrRes
    public static final int argType = 0x7f03002d;

    @AttrRes
    public static final int arrowHeadLength = 0x7f03002e;

    @AttrRes
    public static final int arrowShaftLength = 0x7f03002f;

    @AttrRes
    public static final int autoCompleteTextViewStyle = 0x7f030030;

    @AttrRes
    public static final int autoSizeMaxTextSize = 0x7f030031;

    @AttrRes
    public static final int autoSizeMinTextSize = 0x7f030032;

    @AttrRes
    public static final int autoSizePresetSizes = 0x7f030033;

    @AttrRes
    public static final int autoSizeStepGranularity = 0x7f030034;

    @AttrRes
    public static final int autoSizeTextType = 0x7f030035;

    @AttrRes
    public static final int background = 0x7f030036;

    @AttrRes
    public static final int backgroundColor = 0x7f030037;

    @AttrRes
    public static final int backgroundInsetBottom = 0x7f030038;

    @AttrRes
    public static final int backgroundInsetEnd = 0x7f030039;

    @AttrRes
    public static final int backgroundInsetStart = 0x7f03003a;

    @AttrRes
    public static final int backgroundInsetTop = 0x7f03003b;

    @AttrRes
    public static final int backgroundOverlayColorAlpha = 0x7f03003c;

    @AttrRes
    public static final int backgroundSplit = 0x7f03003d;

    @AttrRes
    public static final int backgroundStacked = 0x7f03003e;

    @AttrRes
    public static final int backgroundTint = 0x7f03003f;

    @AttrRes
    public static final int backgroundTintMode = 0x7f030040;

    @AttrRes
    public static final int badgeGravity = 0x7f030041;

    @AttrRes
    public static final int badgeStyle = 0x7f030042;

    @AttrRes
    public static final int badgeTextColor = 0x7f030043;

    @AttrRes
    public static final int barLength = 0x7f030044;

    @AttrRes
    public static final int barrierAllowsGoneWidgets = 0x7f030045;

    @AttrRes
    public static final int barrierDirection = 0x7f030046;

    @AttrRes
    public static final int behavior_autoHide = 0x7f030047;

    @AttrRes
    public static final int behavior_autoShrink = 0x7f030048;

    @AttrRes
    public static final int behavior_draggable = 0x7f030049;

    @AttrRes
    public static final int behavior_expandedOffset = 0x7f03004a;

    @AttrRes
    public static final int behavior_fitToContents = 0x7f03004b;

    @AttrRes
    public static final int behavior_halfExpandedRatio = 0x7f03004c;

    @AttrRes
    public static final int behavior_hideable = 0x7f03004d;

    @AttrRes
    public static final int behavior_overlapTop = 0x7f03004e;

    @AttrRes
    public static final int behavior_peekHeight = 0x7f03004f;

    @AttrRes
    public static final int behavior_saveFlags = 0x7f030050;

    @AttrRes
    public static final int behavior_skipCollapsed = 0x7f030051;

    @AttrRes
    public static final int borderWidth = 0x7f030052;

    @AttrRes
    public static final int borderlessButtonStyle = 0x7f030053;

    @AttrRes
    public static final int bottomAppBarStyle = 0x7f030054;

    @AttrRes
    public static final int bottomNavigationStyle = 0x7f030055;

    @AttrRes
    public static final int bottomSheetDialogTheme = 0x7f030056;

    @AttrRes
    public static final int bottomSheetStyle = 0x7f030057;

    @AttrRes
    public static final int boxBackgroundColor = 0x7f030058;

    @AttrRes
    public static final int boxBackgroundMode = 0x7f030059;

    @AttrRes
    public static final int boxCollapsedPaddingTop = 0x7f03005a;

    @AttrRes
    public static final int boxCornerRadiusBottomEnd = 0x7f03005b;

    @AttrRes
    public static final int boxCornerRadiusBottomStart = 0x7f03005c;

    @AttrRes
    public static final int boxCornerRadiusTopEnd = 0x7f03005d;

    @AttrRes
    public static final int boxCornerRadiusTopStart = 0x7f03005e;

    @AttrRes
    public static final int boxStrokeColor = 0x7f03005f;

    @AttrRes
    public static final int boxStrokeErrorColor = 0x7f030060;

    @AttrRes
    public static final int boxStrokeWidth = 0x7f030061;

    @AttrRes
    public static final int boxStrokeWidthFocused = 0x7f030062;

    @AttrRes
    public static final int buttonBarButtonStyle = 0x7f030063;

    @AttrRes
    public static final int buttonBarNegativeButtonStyle = 0x7f030064;

    @AttrRes
    public static final int buttonBarNeutralButtonStyle = 0x7f030065;

    @AttrRes
    public static final int buttonBarPositiveButtonStyle = 0x7f030066;

    @AttrRes
    public static final int buttonBarStyle = 0x7f030067;

    @AttrRes
    public static final int buttonCompat = 0x7f030068;

    @AttrRes
    public static final int buttonGravity = 0x7f030069;

    @AttrRes
    public static final int buttonIconDimen = 0x7f03006a;

    @AttrRes
    public static final int buttonPanelSideLayout = 0x7f03006b;

    @AttrRes
    public static final int buttonStyle = 0x7f03006c;

    @AttrRes
    public static final int buttonStyleSmall = 0x7f03006d;

    @AttrRes
    public static final int buttonTint = 0x7f03006e;

    @AttrRes
    public static final int buttonTintMode = 0x7f03006f;

    @AttrRes
    public static final int cardBackgroundColor = 0x7f030070;

    @AttrRes
    public static final int cardCornerRadius = 0x7f030071;

    @AttrRes
    public static final int cardElevation = 0x7f030072;

    @AttrRes
    public static final int cardForegroundColor = 0x7f030073;

    @AttrRes
    public static final int cardMaxElevation = 0x7f030074;

    @AttrRes
    public static final int cardPreventCornerOverlap = 0x7f030075;

    @AttrRes
    public static final int cardUseCompatPadding = 0x7f030076;

    @AttrRes
    public static final int cardViewStyle = 0x7f030077;

    @AttrRes
    public static final int chainUseRtl = 0x7f030078;

    @AttrRes
    public static final int checkboxStyle = 0x7f030079;

    @AttrRes
    public static final int checkedButton = 0x7f03007a;

    @AttrRes
    public static final int checkedChip = 0x7f03007b;

    @AttrRes
    public static final int checkedIcon = 0x7f03007c;

    @AttrRes
    public static final int checkedIconEnabled = 0x7f03007d;

    @AttrRes
    public static final int checkedIconTint = 0x7f03007e;

    @AttrRes
    public static final int checkedIconVisible = 0x7f03007f;

    @AttrRes
    public static final int checkedTextViewStyle = 0x7f030080;

    @AttrRes
    public static final int chipBackgroundColor = 0x7f030081;

    @AttrRes
    public static final int chipCornerRadius = 0x7f030082;

    @AttrRes
    public static final int chipEndPadding = 0x7f030083;

    @AttrRes
    public static final int chipGroupStyle = 0x7f030084;

    @AttrRes
    public static final int chipIcon = 0x7f030085;

    @AttrRes
    public static final int chipIconEnabled = 0x7f030086;

    @AttrRes
    public static final int chipIconSize = 0x7f030087;

    @AttrRes
    public static final int chipIconTint = 0x7f030088;

    @AttrRes
    public static final int chipIconVisible = 0x7f030089;

    @AttrRes
    public static final int chipMinHeight = 0x7f03008a;

    @AttrRes
    public static final int chipMinTouchTargetSize = 0x7f03008b;

    @AttrRes
    public static final int chipSpacing = 0x7f03008c;

    @AttrRes
    public static final int chipSpacingHorizontal = 0x7f03008d;

    @AttrRes
    public static final int chipSpacingVertical = 0x7f03008e;

    @AttrRes
    public static final int chipStandaloneStyle = 0x7f03008f;

    @AttrRes
    public static final int chipStartPadding = 0x7f030090;

    @AttrRes
    public static final int chipStrokeColor = 0x7f030091;

    @AttrRes
    public static final int chipStrokeWidth = 0x7f030092;

    @AttrRes
    public static final int chipStyle = 0x7f030093;

    @AttrRes
    public static final int chipSurfaceColor = 0x7f030094;

    @AttrRes
    public static final int closeIcon = 0x7f030095;

    @AttrRes
    public static final int closeIconEnabled = 0x7f030096;

    @AttrRes
    public static final int closeIconEndPadding = 0x7f030097;

    @AttrRes
    public static final int closeIconSize = 0x7f030098;

    @AttrRes
    public static final int closeIconStartPadding = 0x7f030099;

    @AttrRes
    public static final int closeIconTint = 0x7f03009a;

    @AttrRes
    public static final int closeIconVisible = 0x7f03009b;

    @AttrRes
    public static final int closeItemLayout = 0x7f03009c;

    @AttrRes
    public static final int collapseContentDescription = 0x7f03009d;

    @AttrRes
    public static final int collapseIcon = 0x7f03009e;

    @AttrRes
    public static final int collapsedTitleGravity = 0x7f03009f;

    @AttrRes
    public static final int collapsedTitleTextAppearance = 0x7f0300a0;

    @AttrRes
    public static final int color = 0x7f0300a1;

    @AttrRes
    public static final int colorAccent = 0x7f0300a2;

    @AttrRes
    public static final int colorBackgroundFloating = 0x7f0300a3;

    @AttrRes
    public static final int colorButtonNormal = 0x7f0300a4;

    @AttrRes
    public static final int colorControlActivated = 0x7f0300a5;

    @AttrRes
    public static final int colorControlHighlight = 0x7f0300a6;

    @AttrRes
    public static final int colorControlNormal = 0x7f0300a7;

    @AttrRes
    public static final int colorError = 0x7f0300a8;

    @AttrRes
    public static final int colorOnBackground = 0x7f0300a9;

    @AttrRes
    public static final int colorOnError = 0x7f0300aa;

    @AttrRes
    public static final int colorOnPrimary = 0x7f0300ab;

    @AttrRes
    public static final int colorOnPrimarySurface = 0x7f0300ac;

    @AttrRes
    public static final int colorOnSecondary = 0x7f0300ad;

    @AttrRes
    public static final int colorOnSurface = 0x7f0300ae;

    @AttrRes
    public static final int colorPrimary = 0x7f0300af;

    @AttrRes
    public static final int colorPrimaryDark = 0x7f0300b0;

    @AttrRes
    public static final int colorPrimarySurface = 0x7f0300b1;

    @AttrRes
    public static final int colorPrimaryVariant = 0x7f0300b2;

    @AttrRes
    public static final int colorSecondary = 0x7f0300b3;

    @AttrRes
    public static final int colorSecondaryVariant = 0x7f0300b4;

    @AttrRes
    public static final int colorSurface = 0x7f0300b5;

    @AttrRes
    public static final int colorSwitchThumbNormal = 0x7f0300b6;

    @AttrRes
    public static final int commitIcon = 0x7f0300b7;

    @AttrRes
    public static final int constraintSet = 0x7f0300b8;

    @AttrRes
    public static final int constraint_referenced_ids = 0x7f0300b9;

    @AttrRes
    public static final int content = 0x7f0300ba;

    @AttrRes
    public static final int contentDescription = 0x7f0300bb;

    @AttrRes
    public static final int contentInsetEnd = 0x7f0300bc;

    @AttrRes
    public static final int contentInsetEndWithActions = 0x7f0300bd;

    @AttrRes
    public static final int contentInsetLeft = 0x7f0300be;

    @AttrRes
    public static final int contentInsetRight = 0x7f0300bf;

    @AttrRes
    public static final int contentInsetStart = 0x7f0300c0;

    @AttrRes
    public static final int contentInsetStartWithNavigation = 0x7f0300c1;

    @AttrRes
    public static final int contentPadding = 0x7f0300c2;

    @AttrRes
    public static final int contentPaddingBottom = 0x7f0300c3;

    @AttrRes
    public static final int contentPaddingLeft = 0x7f0300c4;

    @AttrRes
    public static final int contentPaddingRight = 0x7f0300c5;

    @AttrRes
    public static final int contentPaddingTop = 0x7f0300c6;

    @AttrRes
    public static final int contentScrim = 0x7f0300c7;

    @AttrRes
    public static final int controlBackground = 0x7f0300c8;

    @AttrRes
    public static final int coordinatorLayoutStyle = 0x7f0300c9;

    @AttrRes
    public static final int cornerFamily = 0x7f0300ca;

    @AttrRes
    public static final int cornerFamilyBottomLeft = 0x7f0300cb;

    @AttrRes
    public static final int cornerFamilyBottomRight = 0x7f0300cc;

    @AttrRes
    public static final int cornerFamilyTopLeft = 0x7f0300cd;

    @AttrRes
    public static final int cornerFamilyTopRight = 0x7f0300ce;

    @AttrRes
    public static final int cornerRadius = 0x7f0300cf;

    @AttrRes
    public static final int cornerSize = 0x7f0300d0;

    @AttrRes
    public static final int cornerSizeBottomLeft = 0x7f0300d1;

    @AttrRes
    public static final int cornerSizeBottomRight = 0x7f0300d2;

    @AttrRes
    public static final int cornerSizeTopLeft = 0x7f0300d3;

    @AttrRes
    public static final int cornerSizeTopRight = 0x7f0300d4;

    @AttrRes
    public static final int counterEnabled = 0x7f0300d5;

    @AttrRes
    public static final int counterMaxLength = 0x7f0300d6;

    @AttrRes
    public static final int counterOverflowTextAppearance = 0x7f0300d7;

    @AttrRes
    public static final int counterOverflowTextColor = 0x7f0300d8;

    @AttrRes
    public static final int counterTextAppearance = 0x7f0300d9;

    @AttrRes
    public static final int counterTextColor = 0x7f0300da;

    @AttrRes
    public static final int customNavigationLayout = 0x7f0300db;

    @AttrRes
    public static final int data = 0x7f0300dc;

    @AttrRes
    public static final int dataPattern = 0x7f0300dd;

    @AttrRes
    public static final int dayInvalidStyle = 0x7f0300de;

    @AttrRes
    public static final int daySelectedStyle = 0x7f0300df;

    @AttrRes
    public static final int dayStyle = 0x7f0300e0;

    @AttrRes
    public static final int dayTodayStyle = 0x7f0300e1;

    @AttrRes
    public static final int defaultNavHost = 0x7f0300e2;

    @AttrRes
    public static final int defaultQueryHint = 0x7f0300e3;

    @AttrRes
    public static final int destination = 0x7f0300e4;

    @AttrRes
    public static final int dialogCornerRadius = 0x7f0300e5;

    @AttrRes
    public static final int dialogPreferredPadding = 0x7f0300e6;

    @AttrRes
    public static final int dialogTheme = 0x7f0300e7;

    @AttrRes
    public static final int displayOptions = 0x7f0300e8;

    @AttrRes
    public static final int divider = 0x7f0300e9;

    @AttrRes
    public static final int dividerHorizontal = 0x7f0300ea;

    @AttrRes
    public static final int dividerPadding = 0x7f0300eb;

    @AttrRes
    public static final int dividerVertical = 0x7f0300ec;

    @AttrRes
    public static final int dotDiameter = 0x7f0300ed;

    @AttrRes
    public static final int dotEmptyBackground = 0x7f0300ee;

    @AttrRes
    public static final int dotFilledBackground = 0x7f0300ef;

    @AttrRes
    public static final int dotSpacing = 0x7f0300f0;

    @AttrRes
    public static final int drawableBottomCompat = 0x7f0300f1;

    @AttrRes
    public static final int drawableEndCompat = 0x7f0300f2;

    @AttrRes
    public static final int drawableLeftCompat = 0x7f0300f3;

    @AttrRes
    public static final int drawableRightCompat = 0x7f0300f4;

    @AttrRes
    public static final int drawableSize = 0x7f0300f5;

    @AttrRes
    public static final int drawableStartCompat = 0x7f0300f6;

    @AttrRes
    public static final int drawableTint = 0x7f0300f7;

    @AttrRes
    public static final int drawableTintMode = 0x7f0300f8;

    @AttrRes
    public static final int drawableTopCompat = 0x7f0300f9;

    @AttrRes
    public static final int drawerArrowStyle = 0x7f0300fa;

    @AttrRes
    public static final int dropDownListViewStyle = 0x7f0300fb;

    @AttrRes
    public static final int dropdownListPreferredItemHeight = 0x7f0300fc;

    @AttrRes
    public static final int editTextBackground = 0x7f0300fd;

    @AttrRes
    public static final int editTextColor = 0x7f0300fe;

    @AttrRes
    public static final int editTextStyle = 0x7f0300ff;

    @AttrRes
    public static final int elevation = 0x7f030100;

    @AttrRes
    public static final int elevationOverlayColor = 0x7f030101;

    @AttrRes
    public static final int elevationOverlayEnabled = 0x7f030102;

    @AttrRes
    public static final int emptyVisibility = 0x7f030103;

    @AttrRes
    public static final int endIconCheckable = 0x7f030104;

    @AttrRes
    public static final int endIconContentDescription = 0x7f030105;

    @AttrRes
    public static final int endIconDrawable = 0x7f030106;

    @AttrRes
    public static final int endIconMode = 0x7f030107;

    @AttrRes
    public static final int endIconTint = 0x7f030108;

    @AttrRes
    public static final int endIconTintMode = 0x7f030109;

    @AttrRes
    public static final int enforceMaterialTheme = 0x7f03010a;

    @AttrRes
    public static final int enforceTextAppearance = 0x7f03010b;

    @AttrRes
    public static final int ensureMinTouchTargetSize = 0x7f03010c;

    @AttrRes
    public static final int enterAnim = 0x7f03010d;

    @AttrRes
    public static final int errorContentDescription = 0x7f03010e;

    @AttrRes
    public static final int errorEnabled = 0x7f03010f;

    @AttrRes
    public static final int errorIconDrawable = 0x7f030110;

    @AttrRes
    public static final int errorIconTint = 0x7f030111;

    @AttrRes
    public static final int errorIconTintMode = 0x7f030112;

    @AttrRes
    public static final int errorTextAppearance = 0x7f030113;

    @AttrRes
    public static final int errorTextColor = 0x7f030114;

    @AttrRes
    public static final int exitAnim = 0x7f030115;

    @AttrRes
    public static final int expandActivityOverflowButtonDrawable = 0x7f030116;

    @AttrRes
    public static final int expanded = 0x7f030117;

    @AttrRes
    public static final int expandedTitleGravity = 0x7f030118;

    @AttrRes
    public static final int expandedTitleMargin = 0x7f030119;

    @AttrRes
    public static final int expandedTitleMarginBottom = 0x7f03011a;

    @AttrRes
    public static final int expandedTitleMarginEnd = 0x7f03011b;

    @AttrRes
    public static final int expandedTitleMarginStart = 0x7f03011c;

    @AttrRes
    public static final int expandedTitleMarginTop = 0x7f03011d;

    @AttrRes
    public static final int expandedTitleTextAppearance = 0x7f03011e;

    @AttrRes
    public static final int extendMotionSpec = 0x7f03011f;

    @AttrRes
    public static final int extendedFloatingActionButtonStyle = 0x7f030120;

    @AttrRes
    public static final int fabAlignmentMode = 0x7f030121;

    @AttrRes
    public static final int fabAnimationMode = 0x7f030122;

    @AttrRes
    public static final int fabCradleMargin = 0x7f030123;

    @AttrRes
    public static final int fabCradleRoundedCornerRadius = 0x7f030124;

    @AttrRes
    public static final int fabCradleVerticalOffset = 0x7f030125;

    @AttrRes
    public static final int fabCustomSize = 0x7f030126;

    @AttrRes
    public static final int fabSize = 0x7f030127;

    @AttrRes
    public static final int fastScrollEnabled = 0x7f030128;

    @AttrRes
    public static final int fastScrollHorizontalThumbDrawable = 0x7f030129;

    @AttrRes
    public static final int fastScrollHorizontalTrackDrawable = 0x7f03012a;

    @AttrRes
    public static final int fastScrollVerticalThumbDrawable = 0x7f03012b;

    @AttrRes
    public static final int fastScrollVerticalTrackDrawable = 0x7f03012c;

    @AttrRes
    public static final int firstBaselineToTopHeight = 0x7f03012d;

    @AttrRes
    public static final int floatingActionButtonStyle = 0x7f03012e;

    @AttrRes
    public static final int font = 0x7f03012f;

    @AttrRes
    public static final int fontFamily = 0x7f030130;

    @AttrRes
    public static final int fontProviderAuthority = 0x7f030131;

    @AttrRes
    public static final int fontProviderCerts = 0x7f030132;

    @AttrRes
    public static final int fontProviderFetchStrategy = 0x7f030133;

    @AttrRes
    public static final int fontProviderFetchTimeout = 0x7f030134;

    @AttrRes
    public static final int fontProviderPackage = 0x7f030135;

    @AttrRes
    public static final int fontProviderQuery = 0x7f030136;

    @AttrRes
    public static final int fontStyle = 0x7f030137;

    @AttrRes
    public static final int fontVariationSettings = 0x7f030138;

    @AttrRes
    public static final int fontWeight = 0x7f030139;

    @AttrRes
    public static final int foregroundInsidePadding = 0x7f03013a;

    @AttrRes
    public static final int gapBetweenBars = 0x7f03013b;

    @AttrRes
    public static final int gestureInsetBottomIgnored = 0x7f03013c;

    @AttrRes
    public static final int goIcon = 0x7f03013d;

    @AttrRes
    public static final int graph = 0x7f03013e;

    @AttrRes
    public static final int haloColor = 0x7f03013f;

    @AttrRes
    public static final int haloRadius = 0x7f030140;

    @AttrRes
    public static final int headerLayout = 0x7f030141;

    @AttrRes
    public static final int height = 0x7f030142;

    @AttrRes
    public static final int helperText = 0x7f030143;

    @AttrRes
    public static final int helperTextEnabled = 0x7f030144;

    @AttrRes
    public static final int helperTextTextAppearance = 0x7f030145;

    @AttrRes
    public static final int helperTextTextColor = 0x7f030146;

    @AttrRes
    public static final int hideMotionSpec = 0x7f030147;

    @AttrRes
    public static final int hideOnContentScroll = 0x7f030148;

    @AttrRes
    public static final int hideOnScroll = 0x7f030149;

    @AttrRes
    public static final int hintAnimationEnabled = 0x7f03014a;

    @AttrRes
    public static final int hintEnabled = 0x7f03014b;

    @AttrRes
    public static final int hintTextAppearance = 0x7f03014c;

    @AttrRes
    public static final int hintTextColor = 0x7f03014d;

    @AttrRes
    public static final int homeAsUpIndicator = 0x7f03014e;

    @AttrRes
    public static final int homeLayout = 0x7f03014f;

    @AttrRes
    public static final int horizontalOffset = 0x7f030150;

    @AttrRes
    public static final int hoveredFocusedTranslationZ = 0x7f030151;

    @AttrRes
    public static final int icon = 0x7f030152;

    @AttrRes
    public static final int iconEndPadding = 0x7f030153;

    @AttrRes
    public static final int iconGravity = 0x7f030154;

    @AttrRes
    public static final int iconPadding = 0x7f030155;

    @AttrRes
    public static final int iconSize = 0x7f030156;

    @AttrRes
    public static final int iconStartPadding = 0x7f030157;

    @AttrRes
    public static final int iconTint = 0x7f030158;

    @AttrRes
    public static final int iconTintMode = 0x7f030159;

    @AttrRes
    public static final int iconifiedByDefault = 0x7f03015a;

    @AttrRes
    public static final int imageButtonStyle = 0x7f03015b;

    @AttrRes
    public static final int indeterminateProgressStyle = 0x7f03015c;

    @AttrRes
    public static final int initialActivityCount = 0x7f03015d;

    @AttrRes
    public static final int insetForeground = 0x7f03015e;

    @AttrRes
    public static final int isLightTheme = 0x7f03015f;

    @AttrRes
    public static final int isMaterialTheme = 0x7f030160;

    @AttrRes
    public static final int itemBackground = 0x7f030161;

    @AttrRes
    public static final int itemFillColor = 0x7f030162;

    @AttrRes
    public static final int itemHorizontalPadding = 0x7f030163;

    @AttrRes
    public static final int itemHorizontalTranslationEnabled = 0x7f030164;

    @AttrRes
    public static final int itemIconPadding = 0x7f030165;

    @AttrRes
    public static final int itemIconSize = 0x7f030166;

    @AttrRes
    public static final int itemIconTint = 0x7f030167;

    @AttrRes
    public static final int itemMaxLines = 0x7f030168;

    @AttrRes
    public static final int itemPadding = 0x7f030169;

    @AttrRes
    public static final int itemRippleColor = 0x7f03016a;

    @AttrRes
    public static final int itemShapeAppearance = 0x7f03016b;

    @AttrRes
    public static final int itemShapeAppearanceOverlay = 0x7f03016c;

    @AttrRes
    public static final int itemShapeFillColor = 0x7f03016d;

    @AttrRes
    public static final int itemShapeInsetBottom = 0x7f03016e;

    @AttrRes
    public static final int itemShapeInsetEnd = 0x7f03016f;

    @AttrRes
    public static final int itemShapeInsetStart = 0x7f030170;

    @AttrRes
    public static final int itemShapeInsetTop = 0x7f030171;

    @AttrRes
    public static final int itemSpacing = 0x7f030172;

    @AttrRes
    public static final int itemStrokeColor = 0x7f030173;

    @AttrRes
    public static final int itemStrokeWidth = 0x7f030174;

    @AttrRes
    public static final int itemTextAppearance = 0x7f030175;

    @AttrRes
    public static final int itemTextAppearanceActive = 0x7f030176;

    @AttrRes
    public static final int itemTextAppearanceInactive = 0x7f030177;

    @AttrRes
    public static final int itemTextColor = 0x7f030178;

    @AttrRes
    public static final int keylines = 0x7f030179;

    @AttrRes
    public static final int keypadButtonBackgroundDrawable = 0x7f03017a;

    @AttrRes
    public static final int keypadButtonSize = 0x7f03017b;

    @AttrRes
    public static final int keypadDeleteButtonDrawable = 0x7f03017c;

    @AttrRes
    public static final int keypadDeleteButtonPressedColor = 0x7f03017d;

    @AttrRes
    public static final int keypadDeleteButtonSize = 0x7f03017e;

    @AttrRes
    public static final int keypadHorizontalSpacing = 0x7f03017f;

    @AttrRes
    public static final int keypadShowDeleteButton = 0x7f030180;

    @AttrRes
    public static final int keypadTextColor = 0x7f030181;

    @AttrRes
    public static final int keypadTextSize = 0x7f030182;

    @AttrRes
    public static final int keypadVerticalSpacing = 0x7f030183;

    @AttrRes
    public static final int labelBehavior = 0x7f030184;

    @AttrRes
    public static final int labelStyle = 0x7f030185;

    @AttrRes
    public static final int labelVisibilityMode = 0x7f030186;

    @AttrRes
    public static final int lastBaselineToBottomHeight = 0x7f030187;

    @AttrRes
    public static final int launchSingleTop = 0x7f030188;

    @AttrRes
    public static final int layout = 0x7f030189;

    @AttrRes
    public static final int layoutManager = 0x7f03018a;

    @AttrRes
    public static final int layout_anchor = 0x7f03018b;

    @AttrRes
    public static final int layout_anchorGravity = 0x7f03018c;

    @AttrRes
    public static final int layout_behavior = 0x7f03018d;

    @AttrRes
    public static final int layout_collapseMode = 0x7f03018e;

    @AttrRes
    public static final int layout_collapseParallaxMultiplier = 0x7f03018f;

    @AttrRes
    public static final int layout_constrainedHeight = 0x7f030190;

    @AttrRes
    public static final int layout_constrainedWidth = 0x7f030191;

    @AttrRes
    public static final int layout_constraintBaseline_creator = 0x7f030192;

    @AttrRes
    public static final int layout_constraintBaseline_toBaselineOf = 0x7f030193;

    @AttrRes
    public static final int layout_constraintBottom_creator = 0x7f030194;

    @AttrRes
    public static final int layout_constraintBottom_toBottomOf = 0x7f030195;

    @AttrRes
    public static final int layout_constraintBottom_toTopOf = 0x7f030196;

    @AttrRes
    public static final int layout_constraintCircle = 0x7f030197;

    @AttrRes
    public static final int layout_constraintCircleAngle = 0x7f030198;

    @AttrRes
    public static final int layout_constraintCircleRadius = 0x7f030199;

    @AttrRes
    public static final int layout_constraintDimensionRatio = 0x7f03019a;

    @AttrRes
    public static final int layout_constraintEnd_toEndOf = 0x7f03019b;

    @AttrRes
    public static final int layout_constraintEnd_toStartOf = 0x7f03019c;

    @AttrRes
    public static final int layout_constraintGuide_begin = 0x7f03019d;

    @AttrRes
    public static final int layout_constraintGuide_end = 0x7f03019e;

    @AttrRes
    public static final int layout_constraintGuide_percent = 0x7f03019f;

    @AttrRes
    public static final int layout_constraintHeight_default = 0x7f0301a0;

    @AttrRes
    public static final int layout_constraintHeight_max = 0x7f0301a1;

    @AttrRes
    public static final int layout_constraintHeight_min = 0x7f0301a2;

    @AttrRes
    public static final int layout_constraintHeight_percent = 0x7f0301a3;

    @AttrRes
    public static final int layout_constraintHorizontal_bias = 0x7f0301a4;

    @AttrRes
    public static final int layout_constraintHorizontal_chainStyle = 0x7f0301a5;

    @AttrRes
    public static final int layout_constraintHorizontal_weight = 0x7f0301a6;

    @AttrRes
    public static final int layout_constraintLeft_creator = 0x7f0301a7;

    @AttrRes
    public static final int layout_constraintLeft_toLeftOf = 0x7f0301a8;

    @AttrRes
    public static final int layout_constraintLeft_toRightOf = 0x7f0301a9;

    @AttrRes
    public static final int layout_constraintRight_creator = 0x7f0301aa;

    @AttrRes
    public static final int layout_constraintRight_toLeftOf = 0x7f0301ab;

    @AttrRes
    public static final int layout_constraintRight_toRightOf = 0x7f0301ac;

    @AttrRes
    public static final int layout_constraintStart_toEndOf = 0x7f0301ad;

    @AttrRes
    public static final int layout_constraintStart_toStartOf = 0x7f0301ae;

    @AttrRes
    public static final int layout_constraintTop_creator = 0x7f0301af;

    @AttrRes
    public static final int layout_constraintTop_toBottomOf = 0x7f0301b0;

    @AttrRes
    public static final int layout_constraintTop_toTopOf = 0x7f0301b1;

    @AttrRes
    public static final int layout_constraintVertical_bias = 0x7f0301b2;

    @AttrRes
    public static final int layout_constraintVertical_chainStyle = 0x7f0301b3;

    @AttrRes
    public static final int layout_constraintVertical_weight = 0x7f0301b4;

    @AttrRes
    public static final int layout_constraintWidth_default = 0x7f0301b5;

    @AttrRes
    public static final int layout_constraintWidth_max = 0x7f0301b6;

    @AttrRes
    public static final int layout_constraintWidth_min = 0x7f0301b7;

    @AttrRes
    public static final int layout_constraintWidth_percent = 0x7f0301b8;

    @AttrRes
    public static final int layout_dodgeInsetEdges = 0x7f0301b9;

    @AttrRes
    public static final int layout_editor_absoluteX = 0x7f0301ba;

    @AttrRes
    public static final int layout_editor_absoluteY = 0x7f0301bb;

    @AttrRes
    public static final int layout_goneMarginBottom = 0x7f0301bc;

    @AttrRes
    public static final int layout_goneMarginEnd = 0x7f0301bd;

    @AttrRes
    public static final int layout_goneMarginLeft = 0x7f0301be;

    @AttrRes
    public static final int layout_goneMarginRight = 0x7f0301bf;

    @AttrRes
    public static final int layout_goneMarginStart = 0x7f0301c0;

    @AttrRes
    public static final int layout_goneMarginTop = 0x7f0301c1;

    @AttrRes
    public static final int layout_insetEdge = 0x7f0301c2;

    @AttrRes
    public static final int layout_keyline = 0x7f0301c3;

    @AttrRes
    public static final int layout_optimizationLevel = 0x7f0301c4;

    @AttrRes
    public static final int layout_scrollFlags = 0x7f0301c5;

    @AttrRes
    public static final int layout_scrollInterpolator = 0x7f0301c6;

    @AttrRes
    public static final int liftOnScroll = 0x7f0301c7;

    @AttrRes
    public static final int liftOnScrollTargetViewId = 0x7f0301c8;

    @AttrRes
    public static final int lineHeight = 0x7f0301c9;

    @AttrRes
    public static final int lineSpacing = 0x7f0301ca;

    @AttrRes
    public static final int listChoiceBackgroundIndicator = 0x7f0301cb;

    @AttrRes
    public static final int listChoiceIndicatorMultipleAnimated = 0x7f0301cc;

    @AttrRes
    public static final int listChoiceIndicatorSingleAnimated = 0x7f0301cd;

    @AttrRes
    public static final int listDividerAlertDialog = 0x7f0301ce;

    @AttrRes
    public static final int listItemLayout = 0x7f0301cf;

    @AttrRes
    public static final int listLayout = 0x7f0301d0;

    @AttrRes
    public static final int listMenuViewStyle = 0x7f0301d1;

    @AttrRes
    public static final int listPopupWindowStyle = 0x7f0301d2;

    @AttrRes
    public static final int listPreferredItemHeight = 0x7f0301d3;

    @AttrRes
    public static final int listPreferredItemHeightLarge = 0x7f0301d4;

    @AttrRes
    public static final int listPreferredItemHeightSmall = 0x7f0301d5;

    @AttrRes
    public static final int listPreferredItemPaddingEnd = 0x7f0301d6;

    @AttrRes
    public static final int listPreferredItemPaddingLeft = 0x7f0301d7;

    @AttrRes
    public static final int listPreferredItemPaddingRight = 0x7f0301d8;

    @AttrRes
    public static final int listPreferredItemPaddingStart = 0x7f0301d9;

    @AttrRes
    public static final int logo = 0x7f0301da;

    @AttrRes
    public static final int logoDescription = 0x7f0301db;

    @AttrRes
    public static final int materialAlertDialogBodyTextStyle = 0x7f0301dc;

    @AttrRes
    public static final int materialAlertDialogTheme = 0x7f0301dd;

    @AttrRes
    public static final int materialAlertDialogTitleIconStyle = 0x7f0301de;

    @AttrRes
    public static final int materialAlertDialogTitlePanelStyle = 0x7f0301df;

    @AttrRes
    public static final int materialAlertDialogTitleTextStyle = 0x7f0301e0;

    @AttrRes
    public static final int materialButtonOutlinedStyle = 0x7f0301e1;

    @AttrRes
    public static final int materialButtonStyle = 0x7f0301e2;

    @AttrRes
    public static final int materialButtonToggleGroupStyle = 0x7f0301e3;

    @AttrRes
    public static final int materialCalendarDay = 0x7f0301e4;

    @AttrRes
    public static final int materialCalendarFullscreenTheme = 0x7f0301e5;

    @AttrRes
    public static final int materialCalendarHeaderConfirmButton = 0x7f0301e6;

    @AttrRes
    public static final int materialCalendarHeaderDivider = 0x7f0301e7;

    @AttrRes
    public static final int materialCalendarHeaderLayout = 0x7f0301e8;

    @AttrRes
    public static final int materialCalendarHeaderSelection = 0x7f0301e9;

    @AttrRes
    public static final int materialCalendarHeaderTitle = 0x7f0301ea;

    @AttrRes
    public static final int materialCalendarHeaderToggleButton = 0x7f0301eb;

    @AttrRes
    public static final int materialCalendarStyle = 0x7f0301ec;

    @AttrRes
    public static final int materialCalendarTheme = 0x7f0301ed;

    @AttrRes
    public static final int materialCardViewStyle = 0x7f0301ee;

    @AttrRes
    public static final int materialThemeOverlay = 0x7f0301ef;

    @AttrRes
    public static final int maxActionInlineWidth = 0x7f0301f0;

    @AttrRes
    public static final int maxButtonHeight = 0x7f0301f1;

    @AttrRes
    public static final int maxCharacterCount = 0x7f0301f2;

    @AttrRes
    public static final int maxImageSize = 0x7f0301f3;

    @AttrRes
    public static final int maxLines = 0x7f0301f4;

    @AttrRes
    public static final int measureWithLargestChild = 0x7f0301f5;

    @AttrRes
    public static final int menu = 0x7f0301f6;

    @AttrRes
    public static final int met_accentTypeface = 0x7f0301f7;

    @AttrRes
    public static final int met_autoValidate = 0x7f0301f8;

    @AttrRes
    public static final int met_baseColor = 0x7f0301f9;

    @AttrRes
    public static final int met_bottomTextSize = 0x7f0301fa;

    @AttrRes
    public static final int met_clearButton = 0x7f0301fb;

    @AttrRes
    public static final int met_errorColor = 0x7f0301fc;

    @AttrRes
    public static final int met_floatingLabel = 0x7f0301fd;

    @AttrRes
    public static final int met_floatingLabelAlwaysShown = 0x7f0301fe;

    @AttrRes
    public static final int met_floatingLabelAnimating = 0x7f0301ff;

    @AttrRes
    public static final int met_floatingLabelPadding = 0x7f030200;

    @AttrRes
    public static final int met_floatingLabelText = 0x7f030201;

    @AttrRes
    public static final int met_floatingLabelTextColor = 0x7f030202;

    @AttrRes
    public static final int met_floatingLabelTextSize = 0x7f030203;

    @AttrRes
    public static final int met_helperText = 0x7f030204;

    @AttrRes
    public static final int met_helperTextAlwaysShown = 0x7f030205;

    @AttrRes
    public static final int met_helperTextColor = 0x7f030206;

    @AttrRes
    public static final int met_hideUnderline = 0x7f030207;

    @AttrRes
    public static final int met_iconLeft = 0x7f030208;

    @AttrRes
    public static final int met_iconPadding = 0x7f030209;

    @AttrRes
    public static final int met_iconRight = 0x7f03020a;

    @AttrRes
    public static final int met_maxCharacters = 0x7f03020b;

    @AttrRes
    public static final int met_minBottomTextLines = 0x7f03020c;

    @AttrRes
    public static final int met_minCharacters = 0x7f03020d;

    @AttrRes
    public static final int met_primaryColor = 0x7f03020e;

    @AttrRes
    public static final int met_singleLineEllipsis = 0x7f03020f;

    @AttrRes
    public static final int met_textColor = 0x7f030210;

    @AttrRes
    public static final int met_textColorHint = 0x7f030211;

    @AttrRes
    public static final int met_typeface = 0x7f030212;

    @AttrRes
    public static final int met_underlineColor = 0x7f030213;

    @AttrRes
    public static final int minTouchTargetSize = 0x7f030214;

    @AttrRes
    public static final int multiChoiceItemLayout = 0x7f030215;

    @AttrRes
    public static final int navGraph = 0x7f030216;

    @AttrRes
    public static final int navigationContentDescription = 0x7f030217;

    @AttrRes
    public static final int navigationIcon = 0x7f030218;

    @AttrRes
    public static final int navigationMode = 0x7f030219;

    @AttrRes
    public static final int navigationViewStyle = 0x7f03021a;

    @AttrRes
    public static final int nullable = 0x7f03021b;

    @AttrRes
    public static final int number = 0x7f03021c;

    @AttrRes
    public static final int numericModifiers = 0x7f03021d;

    @AttrRes
    public static final int optRoundCardBackgroundColor = 0x7f03021e;

    @AttrRes
    public static final int optRoundCardBottomEdges = 0x7f03021f;

    @AttrRes
    public static final int optRoundCardCornerRadius = 0x7f030220;

    @AttrRes
    public static final int optRoundCardElevation = 0x7f030221;

    @AttrRes
    public static final int optRoundCardLeftBottomCorner = 0x7f030222;

    @AttrRes
    public static final int optRoundCardLeftEdges = 0x7f030223;

    @AttrRes
    public static final int optRoundCardLeftTopCorner = 0x7f030224;

    @AttrRes
    public static final int optRoundCardMaxElevation = 0x7f030225;

    @AttrRes
    public static final int optRoundCardPreventCornerOverlap = 0x7f030226;

    @AttrRes
    public static final int optRoundCardRightBottomCorner = 0x7f030227;

    @AttrRes
    public static final int optRoundCardRightEdges = 0x7f030228;

    @AttrRes
    public static final int optRoundCardRightTopCorner = 0x7f030229;

    @AttrRes
    public static final int optRoundCardTopEdges = 0x7f03022a;

    @AttrRes
    public static final int optRoundCardUseCompatPadding = 0x7f03022b;

    @AttrRes
    public static final int optRoundContentPadding = 0x7f03022c;

    @AttrRes
    public static final int optRoundContentPaddingBottom = 0x7f03022d;

    @AttrRes
    public static final int optRoundContentPaddingLeft = 0x7f03022e;

    @AttrRes
    public static final int optRoundContentPaddingRight = 0x7f03022f;

    @AttrRes
    public static final int optRoundContentPaddingTop = 0x7f030230;

    @AttrRes
    public static final int overlapAnchor = 0x7f030231;

    @AttrRes
    public static final int paddingBottomNoButtons = 0x7f030232;

    @AttrRes
    public static final int paddingBottomSystemWindowInsets = 0x7f030233;

    @AttrRes
    public static final int paddingEnd = 0x7f030234;

    @AttrRes
    public static final int paddingLeftSystemWindowInsets = 0x7f030235;

    @AttrRes
    public static final int paddingRightSystemWindowInsets = 0x7f030236;

    @AttrRes
    public static final int paddingStart = 0x7f030237;

    @AttrRes
    public static final int paddingTopNoTitle = 0x7f030238;

    @AttrRes
    public static final int panelBackground = 0x7f030239;

    @AttrRes
    public static final int panelMenuListTheme = 0x7f03023a;

    @AttrRes
    public static final int panelMenuListWidth = 0x7f03023b;

    @AttrRes
    public static final int passwordToggleContentDescription = 0x7f03023c;

    @AttrRes
    public static final int passwordToggleDrawable = 0x7f03023d;

    @AttrRes
    public static final int passwordToggleEnabled = 0x7f03023e;

    @AttrRes
    public static final int passwordToggleTint = 0x7f03023f;

    @AttrRes
    public static final int passwordToggleTintMode = 0x7f030240;

    @AttrRes
    public static final int pinAnimationType = 0x7f030241;

    @AttrRes
    public static final int pinBackgroundDrawable = 0x7f030242;

    @AttrRes
    public static final int pinBackgroundIsSquare = 0x7f030243;

    @AttrRes
    public static final int pinCharacterMask = 0x7f030244;

    @AttrRes
    public static final int pinCharacterSpacing = 0x7f030245;

    @AttrRes
    public static final int pinLength = 0x7f030246;

    @AttrRes
    public static final int pinLineColors = 0x7f030247;

    @AttrRes
    public static final int pinLineStroke = 0x7f030248;

    @AttrRes
    public static final int pinLineStrokeSelected = 0x7f030249;

    @AttrRes
    public static final int pinRepeatedHint = 0x7f03024a;

    @AttrRes
    public static final int pinTextBottomPadding = 0x7f03024b;

    @AttrRes
    public static final int placeholderText = 0x7f03024c;

    @AttrRes
    public static final int placeholderTextAppearance = 0x7f03024d;

    @AttrRes
    public static final int placeholderTextColor = 0x7f03024e;

    @AttrRes
    public static final int popEnterAnim = 0x7f03024f;

    @AttrRes
    public static final int popExitAnim = 0x7f030250;

    @AttrRes
    public static final int popUpTo = 0x7f030251;

    @AttrRes
    public static final int popUpToInclusive = 0x7f030252;

    @AttrRes
    public static final int popupMenuBackground = 0x7f030253;

    @AttrRes
    public static final int popupMenuStyle = 0x7f030254;

    @AttrRes
    public static final int popupTheme = 0x7f030255;

    @AttrRes
    public static final int popupWindowStyle = 0x7f030256;

    @AttrRes
    public static final int prefixText = 0x7f030257;

    @AttrRes
    public static final int prefixTextAppearance = 0x7f030258;

    @AttrRes
    public static final int prefixTextColor = 0x7f030259;

    @AttrRes
    public static final int preserveIconSpacing = 0x7f03025a;

    @AttrRes
    public static final int pressedTranslationZ = 0x7f03025b;

    @AttrRes
    public static final int progressBarPadding = 0x7f03025c;

    @AttrRes
    public static final int progressBarStyle = 0x7f03025d;

    @AttrRes
    public static final int queryBackground = 0x7f03025e;

    @AttrRes
    public static final int queryHint = 0x7f03025f;

    @AttrRes
    public static final int radioButtonStyle = 0x7f030260;

    @AttrRes
    public static final int rangeFillColor = 0x7f030261;

    @AttrRes
    public static final int ratingBarStyle = 0x7f030262;

    @AttrRes
    public static final int ratingBarStyleIndicator = 0x7f030263;

    @AttrRes
    public static final int ratingBarStyleSmall = 0x7f030264;

    @AttrRes
    public static final int recyclerViewStyle = 0x7f030265;

    @AttrRes
    public static final int reverseLayout = 0x7f030266;

    @AttrRes
    public static final int rippleColor = 0x7f030267;

    @AttrRes
    public static final int scrimAnimationDuration = 0x7f030268;

    @AttrRes
    public static final int scrimBackground = 0x7f030269;

    @AttrRes
    public static final int scrimVisibleHeightTrigger = 0x7f03026a;

    @AttrRes
    public static final int searchHintIcon = 0x7f03026b;

    @AttrRes
    public static final int searchIcon = 0x7f03026c;

    @AttrRes
    public static final int searchViewStyle = 0x7f03026d;

    @AttrRes
    public static final int seekBarStyle = 0x7f03026e;

    @AttrRes
    public static final int selectableItemBackground = 0x7f03026f;

    @AttrRes
    public static final int selectableItemBackgroundBorderless = 0x7f030270;

    @AttrRes
    public static final int selectionRequired = 0x7f030271;

    @AttrRes
    public static final int shapeAppearance = 0x7f030272;

    @AttrRes
    public static final int shapeAppearanceLargeComponent = 0x7f030273;

    @AttrRes
    public static final int shapeAppearanceMediumComponent = 0x7f030274;

    @AttrRes
    public static final int shapeAppearanceOverlay = 0x7f030275;

    @AttrRes
    public static final int shapeAppearanceSmallComponent = 0x7f030276;

    @AttrRes
    public static final int showAsAction = 0x7f030277;

    @AttrRes
    public static final int showDividers = 0x7f030278;

    @AttrRes
    public static final int showMotionSpec = 0x7f030279;

    @AttrRes
    public static final int showText = 0x7f03027a;

    @AttrRes
    public static final int showTitle = 0x7f03027b;

    @AttrRes
    public static final int shrinkMotionSpec = 0x7f03027c;

    @AttrRes
    public static final int singleChoiceItemLayout = 0x7f03027d;

    @AttrRes
    public static final int singleLine = 0x7f03027e;

    @AttrRes
    public static final int singleSelection = 0x7f03027f;

    @AttrRes
    public static final int sliderStyle = 0x7f030280;

    @AttrRes
    public static final int snackbarButtonStyle = 0x7f030281;

    @AttrRes
    public static final int snackbarStyle = 0x7f030282;

    @AttrRes
    public static final int snackbarTextViewStyle = 0x7f030283;

    @AttrRes
    public static final int spanCount = 0x7f030284;

    @AttrRes
    public static final int spinBars = 0x7f030285;

    @AttrRes
    public static final int spinnerDropDownItemStyle = 0x7f030286;

    @AttrRes
    public static final int spinnerStyle = 0x7f030287;

    @AttrRes
    public static final int splitTrack = 0x7f030288;

    @AttrRes
    public static final int srcCompat = 0x7f030289;

    @AttrRes
    public static final int stackFromEnd = 0x7f03028a;

    @AttrRes
    public static final int startDestination = 0x7f03028b;

    @AttrRes
    public static final int startIconCheckable = 0x7f03028c;

    @AttrRes
    public static final int startIconContentDescription = 0x7f03028d;

    @AttrRes
    public static final int startIconDrawable = 0x7f03028e;

    @AttrRes
    public static final int startIconTint = 0x7f03028f;

    @AttrRes
    public static final int startIconTintMode = 0x7f030290;

    @AttrRes
    public static final int state_above_anchor = 0x7f030291;

    @AttrRes
    public static final int state_collapsed = 0x7f030292;

    @AttrRes
    public static final int state_collapsible = 0x7f030293;

    @AttrRes
    public static final int state_dragged = 0x7f030294;

    @AttrRes
    public static final int state_liftable = 0x7f030295;

    @AttrRes
    public static final int state_lifted = 0x7f030296;

    @AttrRes
    public static final int statusBarBackground = 0x7f030297;

    @AttrRes
    public static final int statusBarForeground = 0x7f030298;

    @AttrRes
    public static final int statusBarScrim = 0x7f030299;

    @AttrRes
    public static final int strokeColor = 0x7f03029a;

    @AttrRes
    public static final int strokeWidth = 0x7f03029b;

    @AttrRes
    public static final int subMenuArrow = 0x7f03029c;

    @AttrRes
    public static final int submitBackground = 0x7f03029d;

    @AttrRes
    public static final int subtitle = 0x7f03029e;

    @AttrRes
    public static final int subtitleTextAppearance = 0x7f03029f;

    @AttrRes
    public static final int subtitleTextColor = 0x7f0302a0;

    @AttrRes
    public static final int subtitleTextStyle = 0x7f0302a1;

    @AttrRes
    public static final int suffixText = 0x7f0302a2;

    @AttrRes
    public static final int suffixTextAppearance = 0x7f0302a3;

    @AttrRes
    public static final int suffixTextColor = 0x7f0302a4;

    @AttrRes
    public static final int suggestionRowLayout = 0x7f0302a5;

    @AttrRes
    public static final int switchMinWidth = 0x7f0302a6;

    @AttrRes
    public static final int switchPadding = 0x7f0302a7;

    @AttrRes
    public static final int switchStyle = 0x7f0302a8;

    @AttrRes
    public static final int switchTextAppearance = 0x7f0302a9;

    @AttrRes
    public static final int tabBackground = 0x7f0302aa;

    @AttrRes
    public static final int tabContentStart = 0x7f0302ab;

    @AttrRes
    public static final int tabGravity = 0x7f0302ac;

    @AttrRes
    public static final int tabIconTint = 0x7f0302ad;

    @AttrRes
    public static final int tabIconTintMode = 0x7f0302ae;

    @AttrRes
    public static final int tabIndicator = 0x7f0302af;

    @AttrRes
    public static final int tabIndicatorAnimationDuration = 0x7f0302b0;

    @AttrRes
    public static final int tabIndicatorColor = 0x7f0302b1;

    @AttrRes
    public static final int tabIndicatorFullWidth = 0x7f0302b2;

    @AttrRes
    public static final int tabIndicatorGravity = 0x7f0302b3;

    @AttrRes
    public static final int tabIndicatorHeight = 0x7f0302b4;

    @AttrRes
    public static final int tabInlineLabel = 0x7f0302b5;

    @AttrRes
    public static final int tabMaxWidth = 0x7f0302b6;

    @AttrRes
    public static final int tabMinWidth = 0x7f0302b7;

    @AttrRes
    public static final int tabMode = 0x7f0302b8;

    @AttrRes
    public static final int tabPadding = 0x7f0302b9;

    @AttrRes
    public static final int tabPaddingBottom = 0x7f0302ba;

    @AttrRes
    public static final int tabPaddingEnd = 0x7f0302bb;

    @AttrRes
    public static final int tabPaddingStart = 0x7f0302bc;

    @AttrRes
    public static final int tabPaddingTop = 0x7f0302bd;

    @AttrRes
    public static final int tabRippleColor = 0x7f0302be;

    @AttrRes
    public static final int tabSelectedTextColor = 0x7f0302bf;

    @AttrRes
    public static final int tabStyle = 0x7f0302c0;

    @AttrRes
    public static final int tabTextAppearance = 0x7f0302c1;

    @AttrRes
    public static final int tabTextColor = 0x7f0302c2;

    @AttrRes
    public static final int tabUnboundedRipple = 0x7f0302c3;

    @AttrRes
    public static final int targetPackage = 0x7f0302c4;

    @AttrRes
    public static final int textAllCaps = 0x7f0302c5;

    @AttrRes
    public static final int textAppearanceBody1 = 0x7f0302c6;

    @AttrRes
    public static final int textAppearanceBody2 = 0x7f0302c7;

    @AttrRes
    public static final int textAppearanceButton = 0x7f0302c8;

    @AttrRes
    public static final int textAppearanceCaption = 0x7f0302c9;

    @AttrRes
    public static final int textAppearanceHeadline1 = 0x7f0302ca;

    @AttrRes
    public static final int textAppearanceHeadline2 = 0x7f0302cb;

    @AttrRes
    public static final int textAppearanceHeadline3 = 0x7f0302cc;

    @AttrRes
    public static final int textAppearanceHeadline4 = 0x7f0302cd;

    @AttrRes
    public static final int textAppearanceHeadline5 = 0x7f0302ce;

    @AttrRes
    public static final int textAppearanceHeadline6 = 0x7f0302cf;

    @AttrRes
    public static final int textAppearanceLargePopupMenu = 0x7f0302d0;

    @AttrRes
    public static final int textAppearanceLineHeightEnabled = 0x7f0302d1;

    @AttrRes
    public static final int textAppearanceListItem = 0x7f0302d2;

    @AttrRes
    public static final int textAppearanceListItemSecondary = 0x7f0302d3;

    @AttrRes
    public static final int textAppearanceListItemSmall = 0x7f0302d4;

    @AttrRes
    public static final int textAppearanceOverline = 0x7f0302d5;

    @AttrRes
    public static final int textAppearancePopupMenuHeader = 0x7f0302d6;

    @AttrRes
    public static final int textAppearanceSearchResultSubtitle = 0x7f0302d7;

    @AttrRes
    public static final int textAppearanceSearchResultTitle = 0x7f0302d8;

    @AttrRes
    public static final int textAppearanceSmallPopupMenu = 0x7f0302d9;

    @AttrRes
    public static final int textAppearanceSubtitle1 = 0x7f0302da;

    @AttrRes
    public static final int textAppearanceSubtitle2 = 0x7f0302db;

    @AttrRes
    public static final int textColorAlertDialogListItem = 0x7f0302dc;

    @AttrRes
    public static final int textColorSearchUrl = 0x7f0302dd;

    @AttrRes
    public static final int textEndPadding = 0x7f0302de;

    @AttrRes
    public static final int textInputLayoutFocusedRectEnabled = 0x7f0302df;

    @AttrRes
    public static final int textInputStyle = 0x7f0302e0;

    @AttrRes
    public static final int textLocale = 0x7f0302e1;

    @AttrRes
    public static final int textStartPadding = 0x7f0302e2;

    @AttrRes
    public static final int theme = 0x7f0302e3;

    @AttrRes
    public static final int themeLineHeight = 0x7f0302e4;

    @AttrRes
    public static final int thickness = 0x7f0302e5;

    @AttrRes
    public static final int thumbColor = 0x7f0302e6;

    @AttrRes
    public static final int thumbElevation = 0x7f0302e7;

    @AttrRes
    public static final int thumbRadius = 0x7f0302e8;

    @AttrRes
    public static final int thumbTextPadding = 0x7f0302e9;

    @AttrRes
    public static final int thumbTint = 0x7f0302ea;

    @AttrRes
    public static final int thumbTintMode = 0x7f0302eb;

    @AttrRes
    public static final int tickColor = 0x7f0302ec;

    @AttrRes
    public static final int tickColorActive = 0x7f0302ed;

    @AttrRes
    public static final int tickColorInactive = 0x7f0302ee;

    @AttrRes
    public static final int tickMark = 0x7f0302ef;

    @AttrRes
    public static final int tickMarkTint = 0x7f0302f0;

    @AttrRes
    public static final int tickMarkTintMode = 0x7f0302f1;

    @AttrRes
    public static final int tint = 0x7f0302f2;

    @AttrRes
    public static final int tintMode = 0x7f0302f3;

    @AttrRes
    public static final int title = 0x7f0302f4;

    @AttrRes
    public static final int titleEnabled = 0x7f0302f5;

    @AttrRes
    public static final int titleMargin = 0x7f0302f6;

    @AttrRes
    public static final int titleMarginBottom = 0x7f0302f7;

    @AttrRes
    public static final int titleMarginEnd = 0x7f0302f8;

    @AttrRes
    public static final int titleMarginStart = 0x7f0302f9;

    @AttrRes
    public static final int titleMarginTop = 0x7f0302fa;

    @AttrRes
    public static final int titleMargins = 0x7f0302fb;

    @AttrRes
    public static final int titleTextAppearance = 0x7f0302fc;

    @AttrRes
    public static final int titleTextColor = 0x7f0302fd;

    @AttrRes
    public static final int titleTextStyle = 0x7f0302fe;

    @AttrRes
    public static final int toolbarId = 0x7f0302ff;

    @AttrRes
    public static final int toolbarNavigationButtonStyle = 0x7f030300;

    @AttrRes
    public static final int toolbarStyle = 0x7f030301;

    @AttrRes
    public static final int tooltipForegroundColor = 0x7f030302;

    @AttrRes
    public static final int tooltipFrameBackground = 0x7f030303;

    @AttrRes
    public static final int tooltipStyle = 0x7f030304;

    @AttrRes
    public static final int tooltipText = 0x7f030305;

    @AttrRes
    public static final int track = 0x7f030306;

    @AttrRes
    public static final int trackColor = 0x7f030307;

    @AttrRes
    public static final int trackColorActive = 0x7f030308;

    @AttrRes
    public static final int trackColorInactive = 0x7f030309;

    @AttrRes
    public static final int trackHeight = 0x7f03030a;

    @AttrRes
    public static final int trackTint = 0x7f03030b;

    @AttrRes
    public static final int trackTintMode = 0x7f03030c;

    @AttrRes
    public static final int transitionShapeAppearance = 0x7f03030d;

    @AttrRes
    public static final int ttcIndex = 0x7f03030e;

    @AttrRes
    public static final int uri = 0x7f03030f;

    @AttrRes
    public static final int useCompatPadding = 0x7f030310;

    @AttrRes
    public static final int useMaterialThemeColors = 0x7f030311;

    @AttrRes
    public static final int values = 0x7f030312;

    @AttrRes
    public static final int verticalOffset = 0x7f030313;

    @AttrRes
    public static final int viewInflaterClass = 0x7f030314;

    @AttrRes
    public static final int voiceIcon = 0x7f030315;

    @AttrRes
    public static final int windowActionBar = 0x7f030316;

    @AttrRes
    public static final int windowActionBarOverlay = 0x7f030317;

    @AttrRes
    public static final int windowActionModeOverlay = 0x7f030318;

    @AttrRes
    public static final int windowFixedHeightMajor = 0x7f030319;

    @AttrRes
    public static final int windowFixedHeightMinor = 0x7f03031a;

    @AttrRes
    public static final int windowFixedWidthMajor = 0x7f03031b;

    @AttrRes
    public static final int windowFixedWidthMinor = 0x7f03031c;

    @AttrRes
    public static final int windowMinWidthMajor = 0x7f03031d;

    @AttrRes
    public static final int windowMinWidthMinor = 0x7f03031e;

    @AttrRes
    public static final int windowNoTitle = 0x7f03031f;

    @AttrRes
    public static final int yearSelectedStyle = 0x7f030320;

    @AttrRes
    public static final int yearStyle = 0x7f030321;

    @AttrRes
    public static final int yearTodayStyle = 0x7f030322;
  }

  public static final class bool {
    @BoolRes
    public static final int abc_action_bar_embed_tabs = 0x7f040000;

    @BoolRes
    public static final int abc_allow_stacked_button_bar = 0x7f040001;

    @BoolRes
    public static final int abc_config_actionMenuItemAllCaps = 0x7f040002;

    @BoolRes
    public static final int mtrl_btn_textappearance_all_caps = 0x7f040003;
  }

  public static final class color {
    @ColorRes
    public static final int MainColor = 0x7f050000;

    @ColorRes
    public static final int Samad = 0x7f050001;

    @ColorRes
    public static final int abc_background_cache_hint_selector_material_dark = 0x7f050002;

    @ColorRes
    public static final int abc_background_cache_hint_selector_material_light = 0x7f050003;

    @ColorRes
    public static final int abc_btn_colored_borderless_text_material = 0x7f050004;

    @ColorRes
    public static final int abc_btn_colored_text_material = 0x7f050005;

    @ColorRes
    public static final int abc_color_highlight_material = 0x7f050006;

    @ColorRes
    public static final int abc_hint_foreground_material_dark = 0x7f050007;

    @ColorRes
    public static final int abc_hint_foreground_material_light = 0x7f050008;

    @ColorRes
    public static final int abc_input_method_navigation_guard = 0x7f050009;

    @ColorRes
    public static final int abc_primary_text_disable_only_material_dark = 0x7f05000a;

    @ColorRes
    public static final int abc_primary_text_disable_only_material_light = 0x7f05000b;

    @ColorRes
    public static final int abc_primary_text_material_dark = 0x7f05000c;

    @ColorRes
    public static final int abc_primary_text_material_light = 0x7f05000d;

    @ColorRes
    public static final int abc_search_url_text = 0x7f05000e;

    @ColorRes
    public static final int abc_search_url_text_normal = 0x7f05000f;

    @ColorRes
    public static final int abc_search_url_text_pressed = 0x7f050010;

    @ColorRes
    public static final int abc_search_url_text_selected = 0x7f050011;

    @ColorRes
    public static final int abc_secondary_text_material_dark = 0x7f050012;

    @ColorRes
    public static final int abc_secondary_text_material_light = 0x7f050013;

    @ColorRes
    public static final int abc_tint_btn_checkable = 0x7f050014;

    @ColorRes
    public static final int abc_tint_default = 0x7f050015;

    @ColorRes
    public static final int abc_tint_edittext = 0x7f050016;

    @ColorRes
    public static final int abc_tint_seek_thumb = 0x7f050017;

    @ColorRes
    public static final int abc_tint_spinner = 0x7f050018;

    @ColorRes
    public static final int abc_tint_switch_track = 0x7f050019;

    @ColorRes
    public static final int accent_material_dark = 0x7f05001a;

    @ColorRes
    public static final int accent_material_light = 0x7f05001b;

    @ColorRes
    public static final int background_floating_material_dark = 0x7f05001c;

    @ColorRes
    public static final int background_floating_material_light = 0x7f05001d;

    @ColorRes
    public static final int background_material_dark = 0x7f05001e;

    @ColorRes
    public static final int background_material_light = 0x7f05001f;

    @ColorRes
    public static final int bright_foreground_disabled_material_dark = 0x7f050020;

    @ColorRes
    public static final int bright_foreground_disabled_material_light = 0x7f050021;

    @ColorRes
    public static final int bright_foreground_inverse_material_dark = 0x7f050022;

    @ColorRes
    public static final int bright_foreground_inverse_material_light = 0x7f050023;

    @ColorRes
    public static final int bright_foreground_material_dark = 0x7f050024;

    @ColorRes
    public static final int bright_foreground_material_light = 0x7f050025;

    @ColorRes
    public static final int button_material_dark = 0x7f050026;

    @ColorRes
    public static final int button_material_light = 0x7f050027;

    @ColorRes
    public static final int cardview_dark_background = 0x7f050028;

    @ColorRes
    public static final int cardview_light_background = 0x7f050029;

    @ColorRes
    public static final int cardview_shadow_end_color = 0x7f05002a;

    @ColorRes
    public static final int cardview_shadow_start_color = 0x7f05002b;

    @ColorRes
    public static final int checkbox_themeable_attribute_color = 0x7f05002c;

    @ColorRes
    public static final int colorAccent = 0x7f05002d;

    @ColorRes
    public static final int colorAppName = 0x7f05002e;

    @ColorRes
    public static final int colorBlackHader = 0x7f05002f;

    @ColorRes
    public static final int colorBlackdark = 0x7f050030;

    @ColorRes
    public static final int colorBoxDark = 0x7f050031;

    @ColorRes
    public static final int colorBoxLight = 0x7f050032;

    @ColorRes
    public static final int colorBoxMedium = 0x7f050033;

    @ColorRes
    public static final int colorBoxOutLine = 0x7f050034;

    @ColorRes
    public static final int colorButtonBackground = 0x7f050035;

    @ColorRes
    public static final int colorHintDark = 0x7f050036;

    @ColorRes
    public static final int colorLightHighlight = 0x7f050037;

    @ColorRes
    public static final int colorScreenBackGround = 0x7f050038;

    @ColorRes
    public static final int colorScreenBackGroundDark = 0x7f050039;

    @ColorRes
    public static final int colorScreenBackGroundPink = 0x7f05003a;

    @ColorRes
    public static final int colorTextBlack = 0x7f05003b;

    @ColorRes
    public static final int colorTextButton = 0x7f05003c;

    @ColorRes
    public static final int colorTextwhite = 0x7f05003d;

    @ColorRes
    public static final int colorTitleBackground = 0x7f05003e;

    @ColorRes
    public static final int colorTitleText = 0x7f05003f;

    @ColorRes
    public static final int colorViewLine = 0x7f050040;

    @ColorRes
    public static final int colorWhite = 0x7f050041;

    @ColorRes
    public static final int colorYellow = 0x7f050042;

    @ColorRes
    public static final int color_848484 = 0x7f050043;

    @ColorRes
    public static final int color_green = 0x7f050044;

    @ColorRes
    public static final int color_red = 0x7f050045;

    @ColorRes
    public static final int color_report = 0x7f050046;

    @ColorRes
    public static final int colourToolBarScreenLight = 0x7f050047;

    @ColorRes
    public static final int design_bottom_navigation_shadow_color = 0x7f050048;

    @ColorRes
    public static final int design_box_stroke_color = 0x7f050049;

    @ColorRes
    public static final int design_dark_default_color_background = 0x7f05004a;

    @ColorRes
    public static final int design_dark_default_color_error = 0x7f05004b;

    @ColorRes
    public static final int design_dark_default_color_on_background = 0x7f05004c;

    @ColorRes
    public static final int design_dark_default_color_on_error = 0x7f05004d;

    @ColorRes
    public static final int design_dark_default_color_on_primary = 0x7f05004e;

    @ColorRes
    public static final int design_dark_default_color_on_secondary = 0x7f05004f;

    @ColorRes
    public static final int design_dark_default_color_on_surface = 0x7f050050;

    @ColorRes
    public static final int design_dark_default_color_primary = 0x7f050051;

    @ColorRes
    public static final int design_dark_default_color_primary_dark = 0x7f050052;

    @ColorRes
    public static final int design_dark_default_color_primary_variant = 0x7f050053;

    @ColorRes
    public static final int design_dark_default_color_secondary = 0x7f050054;

    @ColorRes
    public static final int design_dark_default_color_secondary_variant = 0x7f050055;

    @ColorRes
    public static final int design_dark_default_color_surface = 0x7f050056;

    @ColorRes
    public static final int design_default_color_background = 0x7f050057;

    @ColorRes
    public static final int design_default_color_error = 0x7f050058;

    @ColorRes
    public static final int design_default_color_on_background = 0x7f050059;

    @ColorRes
    public static final int design_default_color_on_error = 0x7f05005a;

    @ColorRes
    public static final int design_default_color_on_primary = 0x7f05005b;

    @ColorRes
    public static final int design_default_color_on_secondary = 0x7f05005c;

    @ColorRes
    public static final int design_default_color_on_surface = 0x7f05005d;

    @ColorRes
    public static final int design_default_color_primary = 0x7f05005e;

    @ColorRes
    public static final int design_default_color_primary_dark = 0x7f05005f;

    @ColorRes
    public static final int design_default_color_primary_variant = 0x7f050060;

    @ColorRes
    public static final int design_default_color_secondary = 0x7f050061;

    @ColorRes
    public static final int design_default_color_secondary_variant = 0x7f050062;

    @ColorRes
    public static final int design_default_color_surface = 0x7f050063;

    @ColorRes
    public static final int design_error = 0x7f050064;

    @ColorRes
    public static final int design_fab_shadow_end_color = 0x7f050065;

    @ColorRes
    public static final int design_fab_shadow_mid_color = 0x7f050066;

    @ColorRes
    public static final int design_fab_shadow_start_color = 0x7f050067;

    @ColorRes
    public static final int design_fab_stroke_end_inner_color = 0x7f050068;

    @ColorRes
    public static final int design_fab_stroke_end_outer_color = 0x7f050069;

    @ColorRes
    public static final int design_fab_stroke_top_inner_color = 0x7f05006a;

    @ColorRes
    public static final int design_fab_stroke_top_outer_color = 0x7f05006b;

    @ColorRes
    public static final int design_icon_tint = 0x7f05006c;

    @ColorRes
    public static final int design_snackbar_background_color = 0x7f05006d;

    @ColorRes
    public static final int dim_foreground_disabled_material_dark = 0x7f05006e;

    @ColorRes
    public static final int dim_foreground_disabled_material_light = 0x7f05006f;

    @ColorRes
    public static final int dim_foreground_material_dark = 0x7f050070;

    @ColorRes
    public static final int dim_foreground_material_light = 0x7f050071;

    @ColorRes
    public static final int error_color_material_dark = 0x7f050072;

    @ColorRes
    public static final int error_color_material_light = 0x7f050073;

    @ColorRes
    public static final int footerColor = 0x7f050074;

    @ColorRes
    public static final int foreground_material_dark = 0x7f050075;

    @ColorRes
    public static final int foreground_material_light = 0x7f050076;

    @ColorRes
    public static final int greyish = 0x7f050077;

    @ColorRes
    public static final int highlighted_text_material_dark = 0x7f050078;

    @ColorRes
    public static final int highlighted_text_material_light = 0x7f050079;

    @ColorRes
    public static final int lightBackground = 0x7f05007a;

    @ColorRes
    public static final int light_black = 0x7f05007b;

    @ColorRes
    public static final int material_blue_grey_800 = 0x7f05007c;

    @ColorRes
    public static final int material_blue_grey_900 = 0x7f05007d;

    @ColorRes
    public static final int material_blue_grey_950 = 0x7f05007e;

    @ColorRes
    public static final int material_deep_teal_200 = 0x7f05007f;

    @ColorRes
    public static final int material_deep_teal_500 = 0x7f050080;

    @ColorRes
    public static final int material_grey_100 = 0x7f050081;

    @ColorRes
    public static final int material_grey_300 = 0x7f050082;

    @ColorRes
    public static final int material_grey_50 = 0x7f050083;

    @ColorRes
    public static final int material_grey_600 = 0x7f050084;

    @ColorRes
    public static final int material_grey_800 = 0x7f050085;

    @ColorRes
    public static final int material_grey_850 = 0x7f050086;

    @ColorRes
    public static final int material_grey_900 = 0x7f050087;

    @ColorRes
    public static final int material_on_background_disabled = 0x7f050088;

    @ColorRes
    public static final int material_on_background_emphasis_high_type = 0x7f050089;

    @ColorRes
    public static final int material_on_background_emphasis_medium = 0x7f05008a;

    @ColorRes
    public static final int material_on_primary_disabled = 0x7f05008b;

    @ColorRes
    public static final int material_on_primary_emphasis_high_type = 0x7f05008c;

    @ColorRes
    public static final int material_on_primary_emphasis_medium = 0x7f05008d;

    @ColorRes
    public static final int material_on_surface_disabled = 0x7f05008e;

    @ColorRes
    public static final int material_on_surface_emphasis_high_type = 0x7f05008f;

    @ColorRes
    public static final int material_on_surface_emphasis_medium = 0x7f050090;

    @ColorRes
    public static final int material_on_surface_stroke = 0x7f050091;

    @ColorRes
    public static final int material_slider_active_tick_marks_color = 0x7f050092;

    @ColorRes
    public static final int material_slider_active_track_color = 0x7f050093;

    @ColorRes
    public static final int material_slider_halo_color = 0x7f050094;

    @ColorRes
    public static final int material_slider_inactive_tick_marks_color = 0x7f050095;

    @ColorRes
    public static final int material_slider_inactive_track_color = 0x7f050096;

    @ColorRes
    public static final int material_slider_thumb_color = 0x7f050097;

    @ColorRes
    public static final int mine_shaft = 0x7f050098;

    @ColorRes
    public static final int mtrl_bottom_nav_colored_item_tint = 0x7f050099;

    @ColorRes
    public static final int mtrl_bottom_nav_colored_ripple_color = 0x7f05009a;

    @ColorRes
    public static final int mtrl_bottom_nav_item_tint = 0x7f05009b;

    @ColorRes
    public static final int mtrl_bottom_nav_ripple_color = 0x7f05009c;

    @ColorRes
    public static final int mtrl_btn_bg_color_selector = 0x7f05009d;

    @ColorRes
    public static final int mtrl_btn_ripple_color = 0x7f05009e;

    @ColorRes
    public static final int mtrl_btn_stroke_color_selector = 0x7f05009f;

    @ColorRes
    public static final int mtrl_btn_text_btn_bg_color_selector = 0x7f0500a0;

    @ColorRes
    public static final int mtrl_btn_text_btn_ripple_color = 0x7f0500a1;

    @ColorRes
    public static final int mtrl_btn_text_color_disabled = 0x7f0500a2;

    @ColorRes
    public static final int mtrl_btn_text_color_selector = 0x7f0500a3;

    @ColorRes
    public static final int mtrl_btn_transparent_bg_color = 0x7f0500a4;

    @ColorRes
    public static final int mtrl_calendar_item_stroke_color = 0x7f0500a5;

    @ColorRes
    public static final int mtrl_calendar_selected_range = 0x7f0500a6;

    @ColorRes
    public static final int mtrl_card_view_foreground = 0x7f0500a7;

    @ColorRes
    public static final int mtrl_card_view_ripple = 0x7f0500a8;

    @ColorRes
    public static final int mtrl_chip_background_color = 0x7f0500a9;

    @ColorRes
    public static final int mtrl_chip_close_icon_tint = 0x7f0500aa;

    @ColorRes
    public static final int mtrl_chip_ripple_color = 0x7f0500ab;

    @ColorRes
    public static final int mtrl_chip_surface_color = 0x7f0500ac;

    @ColorRes
    public static final int mtrl_chip_text_color = 0x7f0500ad;

    @ColorRes
    public static final int mtrl_choice_chip_background_color = 0x7f0500ae;

    @ColorRes
    public static final int mtrl_choice_chip_ripple_color = 0x7f0500af;

    @ColorRes
    public static final int mtrl_choice_chip_text_color = 0x7f0500b0;

    @ColorRes
    public static final int mtrl_error = 0x7f0500b1;

    @ColorRes
    public static final int mtrl_fab_bg_color_selector = 0x7f0500b2;

    @ColorRes
    public static final int mtrl_fab_icon_text_color_selector = 0x7f0500b3;

    @ColorRes
    public static final int mtrl_fab_ripple_color = 0x7f0500b4;

    @ColorRes
    public static final int mtrl_filled_background_color = 0x7f0500b5;

    @ColorRes
    public static final int mtrl_filled_icon_tint = 0x7f0500b6;

    @ColorRes
    public static final int mtrl_filled_stroke_color = 0x7f0500b7;

    @ColorRes
    public static final int mtrl_indicator_text_color = 0x7f0500b8;

    @ColorRes
    public static final int mtrl_navigation_item_background_color = 0x7f0500b9;

    @ColorRes
    public static final int mtrl_navigation_item_icon_tint = 0x7f0500ba;

    @ColorRes
    public static final int mtrl_navigation_item_text_color = 0x7f0500bb;

    @ColorRes
    public static final int mtrl_on_primary_text_btn_text_color_selector = 0x7f0500bc;

    @ColorRes
    public static final int mtrl_outlined_icon_tint = 0x7f0500bd;

    @ColorRes
    public static final int mtrl_outlined_stroke_color = 0x7f0500be;

    @ColorRes
    public static final int mtrl_popupmenu_overlay_color = 0x7f0500bf;

    @ColorRes
    public static final int mtrl_scrim_color = 0x7f0500c0;

    @ColorRes
    public static final int mtrl_tabs_colored_ripple_color = 0x7f0500c1;

    @ColorRes
    public static final int mtrl_tabs_icon_color_selector = 0x7f0500c2;

    @ColorRes
    public static final int mtrl_tabs_icon_color_selector_colored = 0x7f0500c3;

    @ColorRes
    public static final int mtrl_tabs_legacy_text_color_selector = 0x7f0500c4;

    @ColorRes
    public static final int mtrl_tabs_ripple_color = 0x7f0500c5;

    @ColorRes
    public static final int mtrl_text_btn_text_color_selector = 0x7f0500c6;

    @ColorRes
    public static final int mtrl_textinput_default_box_stroke_color = 0x7f0500c7;

    @ColorRes
    public static final int mtrl_textinput_disabled_color = 0x7f0500c8;

    @ColorRes
    public static final int mtrl_textinput_filled_box_default_background_color = 0x7f0500c9;

    @ColorRes
    public static final int mtrl_textinput_focused_box_stroke_color = 0x7f0500ca;

    @ColorRes
    public static final int mtrl_textinput_hovered_box_stroke_color = 0x7f0500cb;

    @ColorRes
    public static final int notificationBackgroungDark = 0x7f0500cc;

    @ColorRes
    public static final int notification_action_color_filter = 0x7f0500cd;

    @ColorRes
    public static final int notification_icon_bg_color = 0x7f0500ce;

    @ColorRes
    public static final int notification_material_background_media_default_color = 0x7f0500cf;

    @ColorRes
    public static final int opt_round_card_view_dark_background = 0x7f0500d0;

    @ColorRes
    public static final int opt_round_card_view_light_background = 0x7f0500d1;

    @ColorRes
    public static final int opt_round_card_view_shadow_end_color = 0x7f0500d2;

    @ColorRes
    public static final int opt_round_card_view_shadow_start_color = 0x7f0500d3;

    @ColorRes
    public static final int pin_normal = 0x7f0500d4;

    @ColorRes
    public static final int primary_dark_material_dark = 0x7f0500d5;

    @ColorRes
    public static final int primary_dark_material_light = 0x7f0500d6;

    @ColorRes
    public static final int primary_material_dark = 0x7f0500d7;

    @ColorRes
    public static final int primary_material_light = 0x7f0500d8;

    @ColorRes
    public static final int primary_text_default_material_dark = 0x7f0500d9;

    @ColorRes
    public static final int primary_text_default_material_light = 0x7f0500da;

    @ColorRes
    public static final int primary_text_disabled_material_dark = 0x7f0500db;

    @ColorRes
    public static final int primary_text_disabled_material_light = 0x7f0500dc;

    @ColorRes
    public static final int radiobutton_themeable_attribute_color = 0x7f0500dd;

    @ColorRes
    public static final int ripple_material_dark = 0x7f0500de;

    @ColorRes
    public static final int ripple_material_light = 0x7f0500df;

    @ColorRes
    public static final int secondary_text_default_material_dark = 0x7f0500e0;

    @ColorRes
    public static final int secondary_text_default_material_light = 0x7f0500e1;

    @ColorRes
    public static final int secondary_text_disabled_material_dark = 0x7f0500e2;

    @ColorRes
    public static final int secondary_text_disabled_material_light = 0x7f0500e3;

    @ColorRes
    public static final int switch_thumb_disabled_material_dark = 0x7f0500e4;

    @ColorRes
    public static final int switch_thumb_disabled_material_light = 0x7f0500e5;

    @ColorRes
    public static final int switch_thumb_material_dark = 0x7f0500e6;

    @ColorRes
    public static final int switch_thumb_material_light = 0x7f0500e7;

    @ColorRes
    public static final int switch_thumb_normal_material_dark = 0x7f0500e8;

    @ColorRes
    public static final int switch_thumb_normal_material_light = 0x7f0500e9;

    @ColorRes
    public static final int test_mtrl_calendar_day = 0x7f0500ea;

    @ColorRes
    public static final int test_mtrl_calendar_day_selected = 0x7f0500eb;

    @ColorRes
    public static final int textColor = 0x7f0500ec;

    @ColorRes
    public static final int toolBarTextcolorWhite = 0x7f0500ed;

    @ColorRes
    public static final int tooltip_background_dark = 0x7f0500ee;

    @ColorRes
    public static final int tooltip_background_light = 0x7f0500ef;

    @ColorRes
    public static final int white = 0x7f0500f0;
  }

  public static final class dimen {
    @DimenRes
    public static final int abc_action_bar_content_inset_material = 0x7f060000;

    @DimenRes
    public static final int abc_action_bar_content_inset_with_nav = 0x7f060001;

    @DimenRes
    public static final int abc_action_bar_default_height_material = 0x7f060002;

    @DimenRes
    public static final int abc_action_bar_default_padding_end_material = 0x7f060003;

    @DimenRes
    public static final int abc_action_bar_default_padding_start_material = 0x7f060004;

    @DimenRes
    public static final int abc_action_bar_elevation_material = 0x7f060005;

    @DimenRes
    public static final int abc_action_bar_icon_vertical_padding_material = 0x7f060006;

    @DimenRes
    public static final int abc_action_bar_overflow_padding_end_material = 0x7f060007;

    @DimenRes
    public static final int abc_action_bar_overflow_padding_start_material = 0x7f060008;

    @DimenRes
    public static final int abc_action_bar_stacked_max_height = 0x7f060009;

    @DimenRes
    public static final int abc_action_bar_stacked_tab_max_width = 0x7f06000a;

    @DimenRes
    public static final int abc_action_bar_subtitle_bottom_margin_material = 0x7f06000b;

    @DimenRes
    public static final int abc_action_bar_subtitle_top_margin_material = 0x7f06000c;

    @DimenRes
    public static final int abc_action_button_min_height_material = 0x7f06000d;

    @DimenRes
    public static final int abc_action_button_min_width_material = 0x7f06000e;

    @DimenRes
    public static final int abc_action_button_min_width_overflow_material = 0x7f06000f;

    @DimenRes
    public static final int abc_alert_dialog_button_bar_height = 0x7f060010;

    @DimenRes
    public static final int abc_alert_dialog_button_dimen = 0x7f060011;

    @DimenRes
    public static final int abc_button_inset_horizontal_material = 0x7f060012;

    @DimenRes
    public static final int abc_button_inset_vertical_material = 0x7f060013;

    @DimenRes
    public static final int abc_button_padding_horizontal_material = 0x7f060014;

    @DimenRes
    public static final int abc_button_padding_vertical_material = 0x7f060015;

    @DimenRes
    public static final int abc_cascading_menus_min_smallest_width = 0x7f060016;

    @DimenRes
    public static final int abc_config_prefDialogWidth = 0x7f060017;

    @DimenRes
    public static final int abc_control_corner_material = 0x7f060018;

    @DimenRes
    public static final int abc_control_inset_material = 0x7f060019;

    @DimenRes
    public static final int abc_control_padding_material = 0x7f06001a;

    @DimenRes
    public static final int abc_dialog_corner_radius_material = 0x7f06001b;

    @DimenRes
    public static final int abc_dialog_fixed_height_major = 0x7f06001c;

    @DimenRes
    public static final int abc_dialog_fixed_height_minor = 0x7f06001d;

    @DimenRes
    public static final int abc_dialog_fixed_width_major = 0x7f06001e;

    @DimenRes
    public static final int abc_dialog_fixed_width_minor = 0x7f06001f;

    @DimenRes
    public static final int abc_dialog_list_padding_bottom_no_buttons = 0x7f060020;

    @DimenRes
    public static final int abc_dialog_list_padding_top_no_title = 0x7f060021;

    @DimenRes
    public static final int abc_dialog_min_width_major = 0x7f060022;

    @DimenRes
    public static final int abc_dialog_min_width_minor = 0x7f060023;

    @DimenRes
    public static final int abc_dialog_padding_material = 0x7f060024;

    @DimenRes
    public static final int abc_dialog_padding_top_material = 0x7f060025;

    @DimenRes
    public static final int abc_dialog_title_divider_material = 0x7f060026;

    @DimenRes
    public static final int abc_disabled_alpha_material_dark = 0x7f060027;

    @DimenRes
    public static final int abc_disabled_alpha_material_light = 0x7f060028;

    @DimenRes
    public static final int abc_dropdownitem_icon_width = 0x7f060029;

    @DimenRes
    public static final int abc_dropdownitem_text_padding_left = 0x7f06002a;

    @DimenRes
    public static final int abc_dropdownitem_text_padding_right = 0x7f06002b;

    @DimenRes
    public static final int abc_edit_text_inset_bottom_material = 0x7f06002c;

    @DimenRes
    public static final int abc_edit_text_inset_horizontal_material = 0x7f06002d;

    @DimenRes
    public static final int abc_edit_text_inset_top_material = 0x7f06002e;

    @DimenRes
    public static final int abc_floating_window_z = 0x7f06002f;

    @DimenRes
    public static final int abc_list_item_height_large_material = 0x7f060030;

    @DimenRes
    public static final int abc_list_item_height_material = 0x7f060031;

    @DimenRes
    public static final int abc_list_item_height_small_material = 0x7f060032;

    @DimenRes
    public static final int abc_list_item_padding_horizontal_material = 0x7f060033;

    @DimenRes
    public static final int abc_panel_menu_list_width = 0x7f060034;

    @DimenRes
    public static final int abc_progress_bar_height_material = 0x7f060035;

    @DimenRes
    public static final int abc_search_view_preferred_height = 0x7f060036;

    @DimenRes
    public static final int abc_search_view_preferred_width = 0x7f060037;

    @DimenRes
    public static final int abc_seekbar_track_background_height_material = 0x7f060038;

    @DimenRes
    public static final int abc_seekbar_track_progress_height_material = 0x7f060039;

    @DimenRes
    public static final int abc_select_dialog_padding_start_material = 0x7f06003a;

    @DimenRes
    public static final int abc_switch_padding = 0x7f06003b;

    @DimenRes
    public static final int abc_text_size_body_1_material = 0x7f06003c;

    @DimenRes
    public static final int abc_text_size_body_2_material = 0x7f06003d;

    @DimenRes
    public static final int abc_text_size_button_material = 0x7f06003e;

    @DimenRes
    public static final int abc_text_size_caption_material = 0x7f06003f;

    @DimenRes
    public static final int abc_text_size_display_1_material = 0x7f060040;

    @DimenRes
    public static final int abc_text_size_display_2_material = 0x7f060041;

    @DimenRes
    public static final int abc_text_size_display_3_material = 0x7f060042;

    @DimenRes
    public static final int abc_text_size_display_4_material = 0x7f060043;

    @DimenRes
    public static final int abc_text_size_headline_material = 0x7f060044;

    @DimenRes
    public static final int abc_text_size_large_material = 0x7f060045;

    @DimenRes
    public static final int abc_text_size_medium_material = 0x7f060046;

    @DimenRes
    public static final int abc_text_size_menu_header_material = 0x7f060047;

    @DimenRes
    public static final int abc_text_size_menu_material = 0x7f060048;

    @DimenRes
    public static final int abc_text_size_small_material = 0x7f060049;

    @DimenRes
    public static final int abc_text_size_subhead_material = 0x7f06004a;

    @DimenRes
    public static final int abc_text_size_subtitle_material_toolbar = 0x7f06004b;

    @DimenRes
    public static final int abc_text_size_title_material = 0x7f06004c;

    @DimenRes
    public static final int abc_text_size_title_material_toolbar = 0x7f06004d;

    @DimenRes
    public static final int action_bar_size = 0x7f06004e;

    @DimenRes
    public static final int activity_horizontal_margin = 0x7f06004f;

    @DimenRes
    public static final int activity_vertical_margin = 0x7f060050;

    @DimenRes
    public static final int appcompat_dialog_background_inset = 0x7f060051;

    @DimenRes
    public static final int borderSize1 = 0x7f060052;

    @DimenRes
    public static final int borderSize2 = 0x7f060053;

    @DimenRes
    public static final int bottom_ellipsis_height = 0x7f060054;

    @DimenRes
    public static final int bottom_text_size = 0x7f060055;

    @DimenRes
    public static final int cardview_compat_inset_shadow = 0x7f060056;

    @DimenRes
    public static final int cardview_default_elevation = 0x7f060057;

    @DimenRes
    public static final int cardview_default_radius = 0x7f060058;

    @DimenRes
    public static final int compat_button_inset_horizontal_material = 0x7f060059;

    @DimenRes
    public static final int compat_button_inset_vertical_material = 0x7f06005a;

    @DimenRes
    public static final int compat_button_padding_horizontal_material = 0x7f06005b;

    @DimenRes
    public static final int compat_button_padding_vertical_material = 0x7f06005c;

    @DimenRes
    public static final int compat_control_corner_material = 0x7f06005d;

    @DimenRes
    public static final int compat_notification_large_icon_max_height = 0x7f06005e;

    @DimenRes
    public static final int compat_notification_large_icon_max_width = 0x7f06005f;

    @DimenRes
    public static final int cover_height = 0x7f060060;

    @DimenRes
    public static final int cover_width = 0x7f060061;

    @DimenRes
    public static final int default_button_size = 0x7f060062;

    @DimenRes
    public static final int default_delete_button_size = 0x7f060063;

    @DimenRes
    public static final int default_dimension = 0x7f060064;

    @DimenRes
    public static final int default_dot_diameter = 0x7f060065;

    @DimenRes
    public static final int default_dot_spacing = 0x7f060066;

    @DimenRes
    public static final int default_horizontal_spacing = 0x7f060067;

    @DimenRes
    public static final int default_padding_bottom = 0x7f060068;

    @DimenRes
    public static final int default_padding_top = 0x7f060069;

    @DimenRes
    public static final int default_text_size = 0x7f06006a;

    @DimenRes
    public static final int default_vertical_spacing = 0x7f06006b;

    @DimenRes
    public static final int design_appbar_elevation = 0x7f06006c;

    @DimenRes
    public static final int design_bottom_navigation_active_item_max_width = 0x7f06006d;

    @DimenRes
    public static final int design_bottom_navigation_active_item_min_width = 0x7f06006e;

    @DimenRes
    public static final int design_bottom_navigation_active_text_size = 0x7f06006f;

    @DimenRes
    public static final int design_bottom_navigation_elevation = 0x7f060070;

    @DimenRes
    public static final int design_bottom_navigation_height = 0x7f060071;

    @DimenRes
    public static final int design_bottom_navigation_icon_size = 0x7f060072;

    @DimenRes
    public static final int design_bottom_navigation_item_max_width = 0x7f060073;

    @DimenRes
    public static final int design_bottom_navigation_item_min_width = 0x7f060074;

    @DimenRes
    public static final int design_bottom_navigation_margin = 0x7f060075;

    @DimenRes
    public static final int design_bottom_navigation_shadow_height = 0x7f060076;

    @DimenRes
    public static final int design_bottom_navigation_text_size = 0x7f060077;

    @DimenRes
    public static final int design_bottom_sheet_elevation = 0x7f060078;

    @DimenRes
    public static final int design_bottom_sheet_modal_elevation = 0x7f060079;

    @DimenRes
    public static final int design_bottom_sheet_peek_height_min = 0x7f06007a;

    @DimenRes
    public static final int design_fab_border_width = 0x7f06007b;

    @DimenRes
    public static final int design_fab_elevation = 0x7f06007c;

    @DimenRes
    public static final int design_fab_image_size = 0x7f06007d;

    @DimenRes
    public static final int design_fab_size_mini = 0x7f06007e;

    @DimenRes
    public static final int design_fab_size_normal = 0x7f06007f;

    @DimenRes
    public static final int design_fab_translation_z_hovered_focused = 0x7f060080;

    @DimenRes
    public static final int design_fab_translation_z_pressed = 0x7f060081;

    @DimenRes
    public static final int design_navigation_elevation = 0x7f060082;

    @DimenRes
    public static final int design_navigation_icon_padding = 0x7f060083;

    @DimenRes
    public static final int design_navigation_icon_size = 0x7f060084;

    @DimenRes
    public static final int design_navigation_item_horizontal_padding = 0x7f060085;

    @DimenRes
    public static final int design_navigation_item_icon_padding = 0x7f060086;

    @DimenRes
    public static final int design_navigation_max_width = 0x7f060087;

    @DimenRes
    public static final int design_navigation_padding_bottom = 0x7f060088;

    @DimenRes
    public static final int design_navigation_separator_vertical_padding = 0x7f060089;

    @DimenRes
    public static final int design_snackbar_action_inline_max_width = 0x7f06008a;

    @DimenRes
    public static final int design_snackbar_action_text_color_alpha = 0x7f06008b;

    @DimenRes
    public static final int design_snackbar_background_corner_radius = 0x7f06008c;

    @DimenRes
    public static final int design_snackbar_elevation = 0x7f06008d;

    @DimenRes
    public static final int design_snackbar_extra_spacing_horizontal = 0x7f06008e;

    @DimenRes
    public static final int design_snackbar_max_width = 0x7f06008f;

    @DimenRes
    public static final int design_snackbar_min_width = 0x7f060090;

    @DimenRes
    public static final int design_snackbar_padding_horizontal = 0x7f060091;

    @DimenRes
    public static final int design_snackbar_padding_vertical = 0x7f060092;

    @DimenRes
    public static final int design_snackbar_padding_vertical_2lines = 0x7f060093;

    @DimenRes
    public static final int design_snackbar_text_size = 0x7f060094;

    @DimenRes
    public static final int design_tab_max_width = 0x7f060095;

    @DimenRes
    public static final int design_tab_scrollable_min_width = 0x7f060096;

    @DimenRes
    public static final int design_tab_text_size = 0x7f060097;

    @DimenRes
    public static final int design_tab_text_size_2line = 0x7f060098;

    @DimenRes
    public static final int design_textinput_caption_translate_y = 0x7f060099;

    @DimenRes
    public static final int dimen_1 = 0x7f06009a;

    @DimenRes
    public static final int dimen_10 = 0x7f06009b;

    @DimenRes
    public static final int dimen_100 = 0x7f06009c;

    @DimenRes
    public static final int dimen_13 = 0x7f06009d;

    @DimenRes
    public static final int dimen_14 = 0x7f06009e;

    @DimenRes
    public static final int dimen_15 = 0x7f06009f;

    @DimenRes
    public static final int dimen_150 = 0x7f0600a0;

    @DimenRes
    public static final int dimen_180 = 0x7f0600a1;

    @DimenRes
    public static final int dimen_2 = 0x7f0600a2;

    @DimenRes
    public static final int dimen_20 = 0x7f0600a3;

    @DimenRes
    public static final int dimen_200 = 0x7f0600a4;

    @DimenRes
    public static final int dimen_220 = 0x7f0600a5;

    @DimenRes
    public static final int dimen_23 = 0x7f0600a6;

    @DimenRes
    public static final int dimen_25 = 0x7f0600a7;

    @DimenRes
    public static final int dimen_250 = 0x7f0600a8;

    @DimenRes
    public static final int dimen_3 = 0x7f0600a9;

    @DimenRes
    public static final int dimen_30 = 0x7f0600aa;

    @DimenRes
    public static final int dimen_300 = 0x7f0600ab;

    @DimenRes
    public static final int dimen_32 = 0x7f0600ac;

    @DimenRes
    public static final int dimen_320 = 0x7f0600ad;

    @DimenRes
    public static final int dimen_35 = 0x7f0600ae;

    @DimenRes
    public static final int dimen_40 = 0x7f0600af;

    @DimenRes
    public static final int dimen_400 = 0x7f0600b0;

    @DimenRes
    public static final int dimen_5 = 0x7f0600b1;

    @DimenRes
    public static final int dimen_50 = 0x7f0600b2;

    @DimenRes
    public static final int dimen_6 = 0x7f0600b3;

    @DimenRes
    public static final int dimen_7 = 0x7f0600b4;

    @DimenRes
    public static final int dimen_70 = 0x7f0600b5;

    @DimenRes
    public static final int dimen_8 = 0x7f0600b6;

    @DimenRes
    public static final int dimen_80 = 0x7f0600b7;

    @DimenRes
    public static final int disabled_alpha_material_dark = 0x7f0600b8;

    @DimenRes
    public static final int disabled_alpha_material_light = 0x7f0600b9;

    @DimenRes
    public static final int fab_margin = 0x7f0600ba;

    @DimenRes
    public static final int fastscroll_default_thickness = 0x7f0600bb;

    @DimenRes
    public static final int fastscroll_margin = 0x7f0600bc;

    @DimenRes
    public static final int fastscroll_minimum_range = 0x7f0600bd;

    @DimenRes
    public static final int floating_label_text_size = 0x7f0600be;

    @DimenRes
    public static final int highlight_alpha_material_colored = 0x7f0600bf;

    @DimenRes
    public static final int highlight_alpha_material_dark = 0x7f0600c0;

    @DimenRes
    public static final int highlight_alpha_material_light = 0x7f0600c1;

    @DimenRes
    public static final int hint_alpha_material_dark = 0x7f0600c2;

    @DimenRes
    public static final int hint_alpha_material_light = 0x7f0600c3;

    @DimenRes
    public static final int hint_pressed_alpha_material_dark = 0x7f0600c4;

    @DimenRes
    public static final int hint_pressed_alpha_material_light = 0x7f0600c5;

    @DimenRes
    public static final int home_btn_height = 0x7f0600c6;

    @DimenRes
    public static final int home_btn_width = 0x7f0600c7;

    @DimenRes
    public static final int inner_components_spacing = 0x7f0600c8;

    @DimenRes
    public static final int inner_padding_left = 0x7f0600c9;

    @DimenRes
    public static final int inner_padding_right = 0x7f0600ca;

    @DimenRes
    public static final int item_touch_helper_max_drag_scroll_per_frame = 0x7f0600cb;

    @DimenRes
    public static final int item_touch_helper_swipe_escape_max_velocity = 0x7f0600cc;

    @DimenRes
    public static final int item_touch_helper_swipe_escape_velocity = 0x7f0600cd;

    @DimenRes
    public static final int material_emphasis_disabled = 0x7f0600ce;

    @DimenRes
    public static final int material_emphasis_high_type = 0x7f0600cf;

    @DimenRes
    public static final int material_emphasis_medium = 0x7f0600d0;

    @DimenRes
    public static final int material_text_view_test_line_height = 0x7f0600d1;

    @DimenRes
    public static final int material_text_view_test_line_height_override = 0x7f0600d2;

    @DimenRes
    public static final int mtrl_alert_dialog_background_inset_bottom = 0x7f0600d3;

    @DimenRes
    public static final int mtrl_alert_dialog_background_inset_end = 0x7f0600d4;

    @DimenRes
    public static final int mtrl_alert_dialog_background_inset_start = 0x7f0600d5;

    @DimenRes
    public static final int mtrl_alert_dialog_background_inset_top = 0x7f0600d6;

    @DimenRes
    public static final int mtrl_alert_dialog_picker_background_inset = 0x7f0600d7;

    @DimenRes
    public static final int mtrl_badge_horizontal_edge_offset = 0x7f0600d8;

    @DimenRes
    public static final int mtrl_badge_long_text_horizontal_padding = 0x7f0600d9;

    @DimenRes
    public static final int mtrl_badge_radius = 0x7f0600da;

    @DimenRes
    public static final int mtrl_badge_text_horizontal_edge_offset = 0x7f0600db;

    @DimenRes
    public static final int mtrl_badge_text_size = 0x7f0600dc;

    @DimenRes
    public static final int mtrl_badge_with_text_radius = 0x7f0600dd;

    @DimenRes
    public static final int mtrl_bottomappbar_fabOffsetEndMode = 0x7f0600de;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_bottom_margin = 0x7f0600df;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_margin = 0x7f0600e0;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_rounded_corner_radius = 0x7f0600e1;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_vertical_offset = 0x7f0600e2;

    @DimenRes
    public static final int mtrl_bottomappbar_height = 0x7f0600e3;

    @DimenRes
    public static final int mtrl_btn_corner_radius = 0x7f0600e4;

    @DimenRes
    public static final int mtrl_btn_dialog_btn_min_width = 0x7f0600e5;

    @DimenRes
    public static final int mtrl_btn_disabled_elevation = 0x7f0600e6;

    @DimenRes
    public static final int mtrl_btn_disabled_z = 0x7f0600e7;

    @DimenRes
    public static final int mtrl_btn_elevation = 0x7f0600e8;

    @DimenRes
    public static final int mtrl_btn_focused_z = 0x7f0600e9;

    @DimenRes
    public static final int mtrl_btn_hovered_z = 0x7f0600ea;

    @DimenRes
    public static final int mtrl_btn_icon_btn_padding_left = 0x7f0600eb;

    @DimenRes
    public static final int mtrl_btn_icon_padding = 0x7f0600ec;

    @DimenRes
    public static final int mtrl_btn_inset = 0x7f0600ed;

    @DimenRes
    public static final int mtrl_btn_letter_spacing = 0x7f0600ee;

    @DimenRes
    public static final int mtrl_btn_padding_bottom = 0x7f0600ef;

    @DimenRes
    public static final int mtrl_btn_padding_left = 0x7f0600f0;

    @DimenRes
    public static final int mtrl_btn_padding_right = 0x7f0600f1;

    @DimenRes
    public static final int mtrl_btn_padding_top = 0x7f0600f2;

    @DimenRes
    public static final int mtrl_btn_pressed_z = 0x7f0600f3;

    @DimenRes
    public static final int mtrl_btn_stroke_size = 0x7f0600f4;

    @DimenRes
    public static final int mtrl_btn_text_btn_icon_padding = 0x7f0600f5;

    @DimenRes
    public static final int mtrl_btn_text_btn_padding_left = 0x7f0600f6;

    @DimenRes
    public static final int mtrl_btn_text_btn_padding_right = 0x7f0600f7;

    @DimenRes
    public static final int mtrl_btn_text_size = 0x7f0600f8;

    @DimenRes
    public static final int mtrl_btn_z = 0x7f0600f9;

    @DimenRes
    public static final int mtrl_calendar_action_height = 0x7f0600fa;

    @DimenRes
    public static final int mtrl_calendar_action_padding = 0x7f0600fb;

    @DimenRes
    public static final int mtrl_calendar_bottom_padding = 0x7f0600fc;

    @DimenRes
    public static final int mtrl_calendar_content_padding = 0x7f0600fd;

    @DimenRes
    public static final int mtrl_calendar_day_corner = 0x7f0600fe;

    @DimenRes
    public static final int mtrl_calendar_day_height = 0x7f0600ff;

    @DimenRes
    public static final int mtrl_calendar_day_horizontal_padding = 0x7f060100;

    @DimenRes
    public static final int mtrl_calendar_day_today_stroke = 0x7f060101;

    @DimenRes
    public static final int mtrl_calendar_day_vertical_padding = 0x7f060102;

    @DimenRes
    public static final int mtrl_calendar_day_width = 0x7f060103;

    @DimenRes
    public static final int mtrl_calendar_days_of_week_height = 0x7f060104;

    @DimenRes
    public static final int mtrl_calendar_dialog_background_inset = 0x7f060105;

    @DimenRes
    public static final int mtrl_calendar_header_content_padding = 0x7f060106;

    @DimenRes
    public static final int mtrl_calendar_header_content_padding_fullscreen = 0x7f060107;

    @DimenRes
    public static final int mtrl_calendar_header_divider_thickness = 0x7f060108;

    @DimenRes
    public static final int mtrl_calendar_header_height = 0x7f060109;

    @DimenRes
    public static final int mtrl_calendar_header_height_fullscreen = 0x7f06010a;

    @DimenRes
    public static final int mtrl_calendar_header_selection_line_height = 0x7f06010b;

    @DimenRes
    public static final int mtrl_calendar_header_text_padding = 0x7f06010c;

    @DimenRes
    public static final int mtrl_calendar_header_toggle_margin_bottom = 0x7f06010d;

    @DimenRes
    public static final int mtrl_calendar_header_toggle_margin_top = 0x7f06010e;

    @DimenRes
    public static final int mtrl_calendar_landscape_header_width = 0x7f06010f;

    @DimenRes
    public static final int mtrl_calendar_maximum_default_fullscreen_minor_axis = 0x7f060110;

    @DimenRes
    public static final int mtrl_calendar_month_horizontal_padding = 0x7f060111;

    @DimenRes
    public static final int mtrl_calendar_month_vertical_padding = 0x7f060112;

    @DimenRes
    public static final int mtrl_calendar_navigation_bottom_padding = 0x7f060113;

    @DimenRes
    public static final int mtrl_calendar_navigation_height = 0x7f060114;

    @DimenRes
    public static final int mtrl_calendar_navigation_top_padding = 0x7f060115;

    @DimenRes
    public static final int mtrl_calendar_pre_l_text_clip_padding = 0x7f060116;

    @DimenRes
    public static final int mtrl_calendar_selection_baseline_to_top_fullscreen = 0x7f060117;

    @DimenRes
    public static final int mtrl_calendar_selection_text_baseline_to_bottom = 0x7f060118;

    @DimenRes
    public static final int mtrl_calendar_selection_text_baseline_to_bottom_fullscreen = 0x7f060119;

    @DimenRes
    public static final int mtrl_calendar_selection_text_baseline_to_top = 0x7f06011a;

    @DimenRes
    public static final int mtrl_calendar_text_input_padding_top = 0x7f06011b;

    @DimenRes
    public static final int mtrl_calendar_title_baseline_to_top = 0x7f06011c;

    @DimenRes
    public static final int mtrl_calendar_title_baseline_to_top_fullscreen = 0x7f06011d;

    @DimenRes
    public static final int mtrl_calendar_year_corner = 0x7f06011e;

    @DimenRes
    public static final int mtrl_calendar_year_height = 0x7f06011f;

    @DimenRes
    public static final int mtrl_calendar_year_horizontal_padding = 0x7f060120;

    @DimenRes
    public static final int mtrl_calendar_year_vertical_padding = 0x7f060121;

    @DimenRes
    public static final int mtrl_calendar_year_width = 0x7f060122;

    @DimenRes
    public static final int mtrl_card_checked_icon_margin = 0x7f060123;

    @DimenRes
    public static final int mtrl_card_checked_icon_size = 0x7f060124;

    @DimenRes
    public static final int mtrl_card_corner_radius = 0x7f060125;

    @DimenRes
    public static final int mtrl_card_dragged_z = 0x7f060126;

    @DimenRes
    public static final int mtrl_card_elevation = 0x7f060127;

    @DimenRes
    public static final int mtrl_card_spacing = 0x7f060128;

    @DimenRes
    public static final int mtrl_chip_pressed_translation_z = 0x7f060129;

    @DimenRes
    public static final int mtrl_chip_text_size = 0x7f06012a;

    @DimenRes
    public static final int mtrl_edittext_rectangle_top_offset = 0x7f06012b;

    @DimenRes
    public static final int mtrl_exposed_dropdown_menu_popup_elevation = 0x7f06012c;

    @DimenRes
    public static final int mtrl_exposed_dropdown_menu_popup_vertical_offset = 0x7f06012d;

    @DimenRes
    public static final int mtrl_exposed_dropdown_menu_popup_vertical_padding = 0x7f06012e;

    @DimenRes
    public static final int mtrl_extended_fab_bottom_padding = 0x7f06012f;

    @DimenRes
    public static final int mtrl_extended_fab_corner_radius = 0x7f060130;

    @DimenRes
    public static final int mtrl_extended_fab_disabled_elevation = 0x7f060131;

    @DimenRes
    public static final int mtrl_extended_fab_disabled_translation_z = 0x7f060132;

    @DimenRes
    public static final int mtrl_extended_fab_elevation = 0x7f060133;

    @DimenRes
    public static final int mtrl_extended_fab_end_padding = 0x7f060134;

    @DimenRes
    public static final int mtrl_extended_fab_end_padding_icon = 0x7f060135;

    @DimenRes
    public static final int mtrl_extended_fab_icon_size = 0x7f060136;

    @DimenRes
    public static final int mtrl_extended_fab_icon_text_spacing = 0x7f060137;

    @DimenRes
    public static final int mtrl_extended_fab_min_height = 0x7f060138;

    @DimenRes
    public static final int mtrl_extended_fab_min_width = 0x7f060139;

    @DimenRes
    public static final int mtrl_extended_fab_start_padding = 0x7f06013a;

    @DimenRes
    public static final int mtrl_extended_fab_start_padding_icon = 0x7f06013b;

    @DimenRes
    public static final int mtrl_extended_fab_top_padding = 0x7f06013c;

    @DimenRes
    public static final int mtrl_extended_fab_translation_z_base = 0x7f06013d;

    @DimenRes
    public static final int mtrl_extended_fab_translation_z_hovered_focused = 0x7f06013e;

    @DimenRes
    public static final int mtrl_extended_fab_translation_z_pressed = 0x7f06013f;

    @DimenRes
    public static final int mtrl_fab_elevation = 0x7f060140;

    @DimenRes
    public static final int mtrl_fab_min_touch_target = 0x7f060141;

    @DimenRes
    public static final int mtrl_fab_translation_z_hovered_focused = 0x7f060142;

    @DimenRes
    public static final int mtrl_fab_translation_z_pressed = 0x7f060143;

    @DimenRes
    public static final int mtrl_high_ripple_default_alpha = 0x7f060144;

    @DimenRes
    public static final int mtrl_high_ripple_focused_alpha = 0x7f060145;

    @DimenRes
    public static final int mtrl_high_ripple_hovered_alpha = 0x7f060146;

    @DimenRes
    public static final int mtrl_high_ripple_pressed_alpha = 0x7f060147;

    @DimenRes
    public static final int mtrl_large_touch_target = 0x7f060148;

    @DimenRes
    public static final int mtrl_low_ripple_default_alpha = 0x7f060149;

    @DimenRes
    public static final int mtrl_low_ripple_focused_alpha = 0x7f06014a;

    @DimenRes
    public static final int mtrl_low_ripple_hovered_alpha = 0x7f06014b;

    @DimenRes
    public static final int mtrl_low_ripple_pressed_alpha = 0x7f06014c;

    @DimenRes
    public static final int mtrl_min_touch_target_size = 0x7f06014d;

    @DimenRes
    public static final int mtrl_navigation_elevation = 0x7f06014e;

    @DimenRes
    public static final int mtrl_navigation_item_horizontal_padding = 0x7f06014f;

    @DimenRes
    public static final int mtrl_navigation_item_icon_padding = 0x7f060150;

    @DimenRes
    public static final int mtrl_navigation_item_icon_size = 0x7f060151;

    @DimenRes
    public static final int mtrl_navigation_item_shape_horizontal_margin = 0x7f060152;

    @DimenRes
    public static final int mtrl_navigation_item_shape_vertical_margin = 0x7f060153;

    @DimenRes
    public static final int mtrl_shape_corner_size_large_component = 0x7f060154;

    @DimenRes
    public static final int mtrl_shape_corner_size_medium_component = 0x7f060155;

    @DimenRes
    public static final int mtrl_shape_corner_size_small_component = 0x7f060156;

    @DimenRes
    public static final int mtrl_slider_halo_radius = 0x7f060157;

    @DimenRes
    public static final int mtrl_slider_label_padding = 0x7f060158;

    @DimenRes
    public static final int mtrl_slider_label_radius = 0x7f060159;

    @DimenRes
    public static final int mtrl_slider_label_square_side = 0x7f06015a;

    @DimenRes
    public static final int mtrl_slider_thumb_elevation = 0x7f06015b;

    @DimenRes
    public static final int mtrl_slider_thumb_radius = 0x7f06015c;

    @DimenRes
    public static final int mtrl_slider_track_height = 0x7f06015d;

    @DimenRes
    public static final int mtrl_slider_track_side_padding = 0x7f06015e;

    @DimenRes
    public static final int mtrl_slider_track_top = 0x7f06015f;

    @DimenRes
    public static final int mtrl_slider_widget_height = 0x7f060160;

    @DimenRes
    public static final int mtrl_snackbar_action_text_color_alpha = 0x7f060161;

    @DimenRes
    public static final int mtrl_snackbar_background_corner_radius = 0x7f060162;

    @DimenRes
    public static final int mtrl_snackbar_background_overlay_color_alpha = 0x7f060163;

    @DimenRes
    public static final int mtrl_snackbar_margin = 0x7f060164;

    @DimenRes
    public static final int mtrl_switch_thumb_elevation = 0x7f060165;

    @DimenRes
    public static final int mtrl_textinput_box_corner_radius_medium = 0x7f060166;

    @DimenRes
    public static final int mtrl_textinput_box_corner_radius_small = 0x7f060167;

    @DimenRes
    public static final int mtrl_textinput_box_label_cutout_padding = 0x7f060168;

    @DimenRes
    public static final int mtrl_textinput_box_stroke_width_default = 0x7f060169;

    @DimenRes
    public static final int mtrl_textinput_box_stroke_width_focused = 0x7f06016a;

    @DimenRes
    public static final int mtrl_textinput_counter_margin_start = 0x7f06016b;

    @DimenRes
    public static final int mtrl_textinput_end_icon_margin_start = 0x7f06016c;

    @DimenRes
    public static final int mtrl_textinput_outline_box_expanded_padding = 0x7f06016d;

    @DimenRes
    public static final int mtrl_textinput_start_icon_margin_end = 0x7f06016e;

    @DimenRes
    public static final int mtrl_toolbar_default_height = 0x7f06016f;

    @DimenRes
    public static final int mtrl_tooltip_arrowSize = 0x7f060170;

    @DimenRes
    public static final int mtrl_tooltip_cornerSize = 0x7f060171;

    @DimenRes
    public static final int mtrl_tooltip_minHeight = 0x7f060172;

    @DimenRes
    public static final int mtrl_tooltip_minWidth = 0x7f060173;

    @DimenRes
    public static final int mtrl_tooltip_padding = 0x7f060174;

    @DimenRes
    public static final int mtrl_transition_shared_axis_slide_distance = 0x7f060175;

    @DimenRes
    public static final int nav_header_height = 0x7f060176;

    @DimenRes
    public static final int nav_header_vertical_spacing = 0x7f060177;

    @DimenRes
    public static final int notification_action_icon_size = 0x7f060178;

    @DimenRes
    public static final int notification_action_text_size = 0x7f060179;

    @DimenRes
    public static final int notification_big_circle_margin = 0x7f06017a;

    @DimenRes
    public static final int notification_content_margin_start = 0x7f06017b;

    @DimenRes
    public static final int notification_large_icon_height = 0x7f06017c;

    @DimenRes
    public static final int notification_large_icon_width = 0x7f06017d;

    @DimenRes
    public static final int notification_main_column_padding_top = 0x7f06017e;

    @DimenRes
    public static final int notification_media_narrow_margin = 0x7f06017f;

    @DimenRes
    public static final int notification_right_icon_size = 0x7f060180;

    @DimenRes
    public static final int notification_right_side_padding_top = 0x7f060181;

    @DimenRes
    public static final int notification_small_icon_background_padding = 0x7f060182;

    @DimenRes
    public static final int notification_small_icon_size_as_large = 0x7f060183;

    @DimenRes
    public static final int notification_subtext_size = 0x7f060184;

    @DimenRes
    public static final int notification_top_pad = 0x7f060185;

    @DimenRes
    public static final int notification_top_pad_large_text = 0x7f060186;

    @DimenRes
    public static final int opt_round_card_view_compat_inset_shadow = 0x7f060187;

    @DimenRes
    public static final int opt_round_card_view_default_elevation = 0x7f060188;

    @DimenRes
    public static final int opt_round_card_view_default_radius = 0x7f060189;

    @DimenRes
    public static final int space10dp = 0x7f06018a;

    @DimenRes
    public static final int space12dp = 0x7f06018b;

    @DimenRes
    public static final int space15dp = 0x7f06018c;

    @DimenRes
    public static final int space1dp = 0x7f06018d;

    @DimenRes
    public static final int space20dp = 0x7f06018e;

    @DimenRes
    public static final int space2dp = 0x7f06018f;

    @DimenRes
    public static final int space30dp = 0x7f060190;

    @DimenRes
    public static final int space3dp = 0x7f060191;

    @DimenRes
    public static final int space40dp = 0x7f060192;

    @DimenRes
    public static final int space4dp = 0x7f060193;

    @DimenRes
    public static final int space5dp = 0x7f060194;

    @DimenRes
    public static final int subtitle_corner_radius = 0x7f060195;

    @DimenRes
    public static final int subtitle_outline_width = 0x7f060196;

    @DimenRes
    public static final int subtitle_shadow_offset = 0x7f060197;

    @DimenRes
    public static final int subtitle_shadow_radius = 0x7f060198;

    @DimenRes
    public static final int test_mtrl_calendar_day_cornerSize = 0x7f060199;

    @DimenRes
    public static final int textSize10 = 0x7f06019a;

    @DimenRes
    public static final int textSize15 = 0x7f06019b;

    @DimenRes
    public static final int textSize16 = 0x7f06019c;

    @DimenRes
    public static final int textSize18 = 0x7f06019d;

    @DimenRes
    public static final int textSize20 = 0x7f06019e;

    @DimenRes
    public static final int textSize22 = 0x7f06019f;

    @DimenRes
    public static final int textSize25 = 0x7f0601a0;

    @DimenRes
    public static final int textSize30 = 0x7f0601a1;

    @DimenRes
    public static final int textSize4 = 0x7f0601a2;

    @DimenRes
    public static final int textSize7 = 0x7f0601a3;

    @DimenRes
    public static final int text_size_14SP = 0x7f0601a4;

    @DimenRes
    public static final int text_size_big = 0x7f0601a5;

    @DimenRes
    public static final int text_size_ex_large = 0x7f0601a6;

    @DimenRes
    public static final int text_size_large = 0x7f0601a7;

    @DimenRes
    public static final int text_size_medium = 0x7f0601a8;

    @DimenRes
    public static final int text_size_small = 0x7f0601a9;

    @DimenRes
    public static final int tooltip_corner_radius = 0x7f0601aa;

    @DimenRes
    public static final int tooltip_horizontal_padding = 0x7f0601ab;

    @DimenRes
    public static final int tooltip_margin = 0x7f0601ac;

    @DimenRes
    public static final int tooltip_precise_anchor_extra_offset = 0x7f0601ad;

    @DimenRes
    public static final int tooltip_precise_anchor_threshold = 0x7f0601ae;

    @DimenRes
    public static final int tooltip_vertical_padding = 0x7f0601af;

    @DimenRes
    public static final int tooltip_y_offset_non_touch = 0x7f0601b0;

    @DimenRes
    public static final int tooltip_y_offset_touch = 0x7f0601b1;
  }

  public static final class drawable {
    @DrawableRes
    public static final int aajeevika_ic = 0x7f070008;

    @DrawableRes
    public static final int abc_ab_share_pack_mtrl_alpha = 0x7f070009;

    @DrawableRes
    public static final int abc_action_bar_item_background_material = 0x7f07000a;

    @DrawableRes
    public static final int abc_btn_borderless_material = 0x7f07000b;

    @DrawableRes
    public static final int abc_btn_check_material = 0x7f07000c;

    @DrawableRes
    public static final int abc_btn_check_material_anim = 0x7f07000d;

    @DrawableRes
    public static final int abc_btn_check_to_on_mtrl_000 = 0x7f07000e;

    @DrawableRes
    public static final int abc_btn_check_to_on_mtrl_015 = 0x7f07000f;

    @DrawableRes
    public static final int abc_btn_colored_material = 0x7f070010;

    @DrawableRes
    public static final int abc_btn_default_mtrl_shape = 0x7f070011;

    @DrawableRes
    public static final int abc_btn_radio_material = 0x7f070012;

    @DrawableRes
    public static final int abc_btn_radio_material_anim = 0x7f070013;

    @DrawableRes
    public static final int abc_btn_radio_to_on_mtrl_000 = 0x7f070014;

    @DrawableRes
    public static final int abc_btn_radio_to_on_mtrl_015 = 0x7f070015;

    @DrawableRes
    public static final int abc_btn_switch_to_on_mtrl_00001 = 0x7f070016;

    @DrawableRes
    public static final int abc_btn_switch_to_on_mtrl_00012 = 0x7f070017;

    @DrawableRes
    public static final int abc_cab_background_internal_bg = 0x7f070018;

    @DrawableRes
    public static final int abc_cab_background_top_material = 0x7f070019;

    @DrawableRes
    public static final int abc_cab_background_top_mtrl_alpha = 0x7f07001a;

    @DrawableRes
    public static final int abc_control_background_material = 0x7f07001b;

    @DrawableRes
    public static final int abc_dialog_material_background = 0x7f07001c;

    @DrawableRes
    public static final int abc_edit_text_material = 0x7f07001d;

    @DrawableRes
    public static final int abc_ic_ab_back_material = 0x7f07001e;

    @DrawableRes
    public static final int abc_ic_arrow_drop_right_black_24dp = 0x7f07001f;

    @DrawableRes
    public static final int abc_ic_clear_material = 0x7f070020;

    @DrawableRes
    public static final int abc_ic_commit_search_api_mtrl_alpha = 0x7f070021;

    @DrawableRes
    public static final int abc_ic_go_search_api_material = 0x7f070022;

    @DrawableRes
    public static final int abc_ic_menu_copy_mtrl_am_alpha = 0x7f070023;

    @DrawableRes
    public static final int abc_ic_menu_cut_mtrl_alpha = 0x7f070024;

    @DrawableRes
    public static final int abc_ic_menu_overflow_material = 0x7f070025;

    @DrawableRes
    public static final int abc_ic_menu_paste_mtrl_am_alpha = 0x7f070026;

    @DrawableRes
    public static final int abc_ic_menu_selectall_mtrl_alpha = 0x7f070027;

    @DrawableRes
    public static final int abc_ic_menu_share_mtrl_alpha = 0x7f070028;

    @DrawableRes
    public static final int abc_ic_search_api_material = 0x7f070029;

    @DrawableRes
    public static final int abc_ic_star_black_16dp = 0x7f07002a;

    @DrawableRes
    public static final int abc_ic_star_black_36dp = 0x7f07002b;

    @DrawableRes
    public static final int abc_ic_star_black_48dp = 0x7f07002c;

    @DrawableRes
    public static final int abc_ic_star_half_black_16dp = 0x7f07002d;

    @DrawableRes
    public static final int abc_ic_star_half_black_36dp = 0x7f07002e;

    @DrawableRes
    public static final int abc_ic_star_half_black_48dp = 0x7f07002f;

    @DrawableRes
    public static final int abc_ic_voice_search_api_material = 0x7f070030;

    @DrawableRes
    public static final int abc_item_background_holo_dark = 0x7f070031;

    @DrawableRes
    public static final int abc_item_background_holo_light = 0x7f070032;

    @DrawableRes
    public static final int abc_list_divider_material = 0x7f070033;

    @DrawableRes
    public static final int abc_list_divider_mtrl_alpha = 0x7f070034;

    @DrawableRes
    public static final int abc_list_focused_holo = 0x7f070035;

    @DrawableRes
    public static final int abc_list_longpressed_holo = 0x7f070036;

    @DrawableRes
    public static final int abc_list_pressed_holo_dark = 0x7f070037;

    @DrawableRes
    public static final int abc_list_pressed_holo_light = 0x7f070038;

    @DrawableRes
    public static final int abc_list_selector_background_transition_holo_dark = 0x7f070039;

    @DrawableRes
    public static final int abc_list_selector_background_transition_holo_light = 0x7f07003a;

    @DrawableRes
    public static final int abc_list_selector_disabled_holo_dark = 0x7f07003b;

    @DrawableRes
    public static final int abc_list_selector_disabled_holo_light = 0x7f07003c;

    @DrawableRes
    public static final int abc_list_selector_holo_dark = 0x7f07003d;

    @DrawableRes
    public static final int abc_list_selector_holo_light = 0x7f07003e;

    @DrawableRes
    public static final int abc_menu_hardkey_panel_mtrl_mult = 0x7f07003f;

    @DrawableRes
    public static final int abc_popup_background_mtrl_mult = 0x7f070040;

    @DrawableRes
    public static final int abc_ratingbar_indicator_material = 0x7f070041;

    @DrawableRes
    public static final int abc_ratingbar_material = 0x7f070042;

    @DrawableRes
    public static final int abc_ratingbar_small_material = 0x7f070043;

    @DrawableRes
    public static final int abc_scrubber_control_off_mtrl_alpha = 0x7f070044;

    @DrawableRes
    public static final int abc_scrubber_control_to_pressed_mtrl_000 = 0x7f070045;

    @DrawableRes
    public static final int abc_scrubber_control_to_pressed_mtrl_005 = 0x7f070046;

    @DrawableRes
    public static final int abc_scrubber_primary_mtrl_alpha = 0x7f070047;

    @DrawableRes
    public static final int abc_scrubber_track_mtrl_alpha = 0x7f070048;

    @DrawableRes
    public static final int abc_seekbar_thumb_material = 0x7f070049;

    @DrawableRes
    public static final int abc_seekbar_tick_mark_material = 0x7f07004a;

    @DrawableRes
    public static final int abc_seekbar_track_material = 0x7f07004b;

    @DrawableRes
    public static final int abc_spinner_mtrl_am_alpha = 0x7f07004c;

    @DrawableRes
    public static final int abc_spinner_textfield_background_material = 0x7f07004d;

    @DrawableRes
    public static final int abc_switch_thumb_material = 0x7f07004e;

    @DrawableRes
    public static final int abc_switch_track_mtrl_alpha = 0x7f07004f;

    @DrawableRes
    public static final int abc_tab_indicator_material = 0x7f070050;

    @DrawableRes
    public static final int abc_tab_indicator_mtrl_alpha = 0x7f070051;

    @DrawableRes
    public static final int abc_text_cursor_material = 0x7f070052;

    @DrawableRes
    public static final int abc_text_select_handle_left_mtrl_dark = 0x7f070053;

    @DrawableRes
    public static final int abc_text_select_handle_left_mtrl_light = 0x7f070054;

    @DrawableRes
    public static final int abc_text_select_handle_middle_mtrl_dark = 0x7f070055;

    @DrawableRes
    public static final int abc_text_select_handle_middle_mtrl_light = 0x7f070056;

    @DrawableRes
    public static final int abc_text_select_handle_right_mtrl_dark = 0x7f070057;

    @DrawableRes
    public static final int abc_text_select_handle_right_mtrl_light = 0x7f070058;

    @DrawableRes
    public static final int abc_textfield_activated_mtrl_alpha = 0x7f070059;

    @DrawableRes
    public static final int abc_textfield_default_mtrl_alpha = 0x7f07005a;

    @DrawableRes
    public static final int abc_textfield_search_activated_mtrl_alpha = 0x7f07005b;

    @DrawableRes
    public static final int abc_textfield_search_default_mtrl_alpha = 0x7f07005c;

    @DrawableRes
    public static final int abc_textfield_search_material = 0x7f07005d;

    @DrawableRes
    public static final int abc_vector_test = 0x7f07005e;

    @DrawableRes
    public static final int arow_right = 0x7f07005f;

    @DrawableRes
    public static final int avd_hide_password = 0x7f070060;

    @DrawableRes
    public static final int avd_show_password = 0x7f070061;

    @DrawableRes
    public static final int badge_background = 0x7f070062;

    @DrawableRes
    public static final int baseline_img = 0x7f070063;

    @DrawableRes
    public static final int baseline_nev_icon = 0x7f070064;

    @DrawableRes
    public static final int bg_pin = 0x7f070065;

    @DrawableRes
    public static final int bg_pin_round = 0x7f070066;

    @DrawableRes
    public static final int border = 0x7f070067;

    @DrawableRes
    public static final int border_of_button = 0x7f070068;

    @DrawableRes
    public static final int borderbutton_colour_title = 0x7f070069;

    @DrawableRes
    public static final int btn_checkbox_checked_mtrl = 0x7f07006a;

    @DrawableRes
    public static final int btn_checkbox_checked_to_unchecked_mtrl_animation = 0x7f07006b;

    @DrawableRes
    public static final int btn_checkbox_unchecked_mtrl = 0x7f07006c;

    @DrawableRes
    public static final int btn_checkbox_unchecked_to_checked_mtrl_animation = 0x7f07006d;

    @DrawableRes
    public static final int btn_radio_off_mtrl = 0x7f07006e;

    @DrawableRes
    public static final int btn_radio_off_to_on_mtrl_animation = 0x7f07006f;

    @DrawableRes
    public static final int btn_radio_on_mtrl = 0x7f070070;

    @DrawableRes
    public static final int btn_radio_on_to_off_mtrl_animation = 0x7f070071;

    @DrawableRes
    public static final int btn_round_colour = 0x7f070072;

    @DrawableRes
    public static final int change_language_nev_icon = 0x7f070073;

    @DrawableRes
    public static final int dashboard_nev_icon = 0x7f070074;

    @DrawableRes
    public static final int demo = 0x7f070075;

    @DrawableRes
    public static final int demo_round = 0x7f070076;

    @DrawableRes
    public static final int design_bottom_navigation_item_background = 0x7f070077;

    @DrawableRes
    public static final int design_fab_background = 0x7f070078;

    @DrawableRes
    public static final int design_ic_visibility = 0x7f070079;

    @DrawableRes
    public static final int design_ic_visibility_off = 0x7f07007a;

    @DrawableRes
    public static final int design_password_eye = 0x7f07007b;

    @DrawableRes
    public static final int design_snackbar_background = 0x7f07007c;

    @DrawableRes
    public static final int dot_empty = 0x7f07007d;

    @DrawableRes
    public static final int dot_filled = 0x7f07007e;

    @DrawableRes
    public static final int drawer_background_layout = 0x7f07007f;

    @DrawableRes
    public static final int drawer_hadder_logo = 0x7f070080;

    @DrawableRes
    public static final int evaluation_nev_icon = 0x7f070081;

    @DrawableRes
    public static final int evalution_img = 0x7f070082;

    @DrawableRes
    public static final int forward_arrow = 0x7f070083;

    @DrawableRes
    public static final int ic_arrow_left_white_24dp = 0x7f070084;

    @DrawableRes
    public static final int ic_backspace = 0x7f070085;

    @DrawableRes
    public static final int ic_chevron_right_black_24dp = 0x7f070086;

    @DrawableRes
    public static final int ic_expand_more_black_18dp = 0x7f070087;

    @DrawableRes
    public static final int ic_launcher_background = 0x7f070088;

    @DrawableRes
    public static final int ic_launcher_foreground = 0x7f070089;

    @DrawableRes
    public static final int ic_menu_camera = 0x7f07008a;

    @DrawableRes
    public static final int ic_menu_gallery = 0x7f07008b;

    @DrawableRes
    public static final int ic_menu_manage = 0x7f07008c;

    @DrawableRes
    public static final int ic_menu_send = 0x7f07008d;

    @DrawableRes
    public static final int ic_menu_share = 0x7f07008e;

    @DrawableRes
    public static final int ic_menu_slideshow = 0x7f07008f;

    @DrawableRes
    public static final int ic_mtrl_checked_circle = 0x7f070090;

    @DrawableRes
    public static final int ic_mtrl_chip_checked_black = 0x7f070091;

    @DrawableRes
    public static final int ic_mtrl_chip_checked_circle = 0x7f070092;

    @DrawableRes
    public static final int ic_mtrl_chip_close_circle = 0x7f070093;

    @DrawableRes
    public static final int large = 0x7f070094;

    @DrawableRes
    public static final int light_border = 0x7f070095;

    @DrawableRes
    public static final int login_edittext_img = 0x7f070096;

    @DrawableRes
    public static final int logout_nev_icon = 0x7f070097;

    @DrawableRes
    public static final int material_ic_calendar_black_24dp = 0x7f070098;

    @DrawableRes
    public static final int material_ic_clear_black_24dp = 0x7f070099;

    @DrawableRes
    public static final int material_ic_edit_black_24dp = 0x7f07009a;

    @DrawableRes
    public static final int material_ic_keyboard_arrow_left_black_24dp = 0x7f07009b;

    @DrawableRes
    public static final int material_ic_keyboard_arrow_right_black_24dp = 0x7f07009c;

    @DrawableRes
    public static final int material_ic_menu_arrow_down_black_24dp = 0x7f07009d;

    @DrawableRes
    public static final int material_ic_menu_arrow_up_black_24dp = 0x7f07009e;

    @DrawableRes
    public static final int met_ic_clear = 0x7f07009f;

    @DrawableRes
    public static final int met_ic_close = 0x7f0700a0;

    @DrawableRes
    public static final int mtrl_dialog_background = 0x7f0700a1;

    @DrawableRes
    public static final int mtrl_dropdown_arrow = 0x7f0700a2;

    @DrawableRes
    public static final int mtrl_ic_arrow_drop_down = 0x7f0700a3;

    @DrawableRes
    public static final int mtrl_ic_arrow_drop_up = 0x7f0700a4;

    @DrawableRes
    public static final int mtrl_ic_cancel = 0x7f0700a5;

    @DrawableRes
    public static final int mtrl_ic_error = 0x7f0700a6;

    @DrawableRes
    public static final int mtrl_popupmenu_background = 0x7f0700a7;

    @DrawableRes
    public static final int mtrl_popupmenu_background_dark = 0x7f0700a8;

    @DrawableRes
    public static final int mtrl_tabs_default_indicator = 0x7f0700a9;

    @DrawableRes
    public static final int navigation_empty_icon = 0x7f0700aa;

    @DrawableRes
    public static final int navigation_gradiant = 0x7f0700ab;

    @DrawableRes
    public static final int nic = 0x7f0700ac;

    @DrawableRes
    public static final int nic1 = 0x7f0700ad;

    @DrawableRes
    public static final int nic_updated = 0x7f0700ae;

    @DrawableRes
    public static final int notification_action_background = 0x7f0700af;

    @DrawableRes
    public static final int notification_bg = 0x7f0700b0;

    @DrawableRes
    public static final int notification_bg_low = 0x7f0700b1;

    @DrawableRes
    public static final int notification_bg_low_normal = 0x7f0700b2;

    @DrawableRes
    public static final int notification_bg_low_pressed = 0x7f0700b3;

    @DrawableRes
    public static final int notification_bg_normal = 0x7f0700b4;

    @DrawableRes
    public static final int notification_bg_normal_pressed = 0x7f0700b5;

    @DrawableRes
    public static final int notification_icon_background = 0x7f0700b6;

    @DrawableRes
    public static final int notification_template_icon_bg = 0x7f0700b7;

    @DrawableRes
    public static final int notification_template_icon_low_bg = 0x7f0700b8;

    @DrawableRes
    public static final int notification_tile_bg = 0x7f0700b9;

    @DrawableRes
    public static final int notify_panel_notification_icon_bg = 0x7f0700ba;

    @DrawableRes
    public static final int nrlm = 0x7f0700bb;

    @DrawableRes
    public static final int ok = 0x7f0700bc;

    @DrawableRes
    public static final int password_edittext_img = 0x7f0700bd;

    @DrawableRes
    public static final int report_img = 0x7f0700be;

    @DrawableRes
    public static final int riple = 0x7f0700bf;

    @DrawableRes
    public static final int round_shape = 0x7f0700c0;

    @DrawableRes
    public static final int round_shape_dashboard = 0x7f0700c1;

    @DrawableRes
    public static final int rounded_shape_white_background = 0x7f0700c2;

    @DrawableRes
    public static final int saksham_gradient = 0x7f0700c3;

    @DrawableRes
    public static final int saksham_new_splash = 0x7f0700c4;

    @DrawableRes
    public static final int saksham_splash = 0x7f0700c5;

    @DrawableRes
    public static final int shaksham_splash_blue = 0x7f0700c6;

    @DrawableRes
    public static final int shape_dialog = 0x7f0700c7;

    @DrawableRes
    public static final int shape_rounded = 0x7f0700c8;

    @DrawableRes
    public static final int shape_rounded_for_mpin = 0x7f0700c9;

    @DrawableRes
    public static final int shape_single_rounded = 0x7f0700ca;

    @DrawableRes
    public static final int side_nav_bar = 0x7f0700cb;

    @DrawableRes
    public static final int step_bg = 0x7f0700cc;

    @DrawableRes
    public static final int test_custom_background = 0x7f0700cd;

    @DrawableRes
    public static final int tooltip_frame_dark = 0x7f0700ce;

    @DrawableRes
    public static final int tooltip_frame_light = 0x7f0700cf;

    @DrawableRes
    public static final int training_img = 0x7f0700d0;

    @DrawableRes
    public static final int training_nev_icon = 0x7f0700d1;

    @DrawableRes
    public static final int view_report_icon = 0x7f0700d2;
  }

  public static final class id {
    @IdRes
    public static final int ALT = 0x7f080000;

    @IdRes
    public static final int BOTTOM_END = 0x7f080001;

    @IdRes
    public static final int BOTTOM_START = 0x7f080002;

    @IdRes
    public static final int CTRL = 0x7f080003;

    @IdRes
    public static final int FUNCTION = 0x7f080004;

    @IdRes
    public static final int Local_LanguageTv = 0x7f080005;

    @IdRes
    public static final int META = 0x7f080006;

    @IdRes
    public static final int SHIFT = 0x7f080007;

    @IdRes
    public static final int SYM = 0x7f080008;

    @IdRes
    public static final int TOP_END = 0x7f080009;

    @IdRes
    public static final int TOP_START = 0x7f08000a;

    @IdRes
    public static final int accessibility_action_clickable_span = 0x7f08000b;

    @IdRes
    public static final int accessibility_custom_action_0 = 0x7f08000c;

    @IdRes
    public static final int accessibility_custom_action_1 = 0x7f08000d;

    @IdRes
    public static final int accessibility_custom_action_10 = 0x7f08000e;

    @IdRes
    public static final int accessibility_custom_action_11 = 0x7f08000f;

    @IdRes
    public static final int accessibility_custom_action_12 = 0x7f080010;

    @IdRes
    public static final int accessibility_custom_action_13 = 0x7f080011;

    @IdRes
    public static final int accessibility_custom_action_14 = 0x7f080012;

    @IdRes
    public static final int accessibility_custom_action_15 = 0x7f080013;

    @IdRes
    public static final int accessibility_custom_action_16 = 0x7f080014;

    @IdRes
    public static final int accessibility_custom_action_17 = 0x7f080015;

    @IdRes
    public static final int accessibility_custom_action_18 = 0x7f080016;

    @IdRes
    public static final int accessibility_custom_action_19 = 0x7f080017;

    @IdRes
    public static final int accessibility_custom_action_2 = 0x7f080018;

    @IdRes
    public static final int accessibility_custom_action_20 = 0x7f080019;

    @IdRes
    public static final int accessibility_custom_action_21 = 0x7f08001a;

    @IdRes
    public static final int accessibility_custom_action_22 = 0x7f08001b;

    @IdRes
    public static final int accessibility_custom_action_23 = 0x7f08001c;

    @IdRes
    public static final int accessibility_custom_action_24 = 0x7f08001d;

    @IdRes
    public static final int accessibility_custom_action_25 = 0x7f08001e;

    @IdRes
    public static final int accessibility_custom_action_26 = 0x7f08001f;

    @IdRes
    public static final int accessibility_custom_action_27 = 0x7f080020;

    @IdRes
    public static final int accessibility_custom_action_28 = 0x7f080021;

    @IdRes
    public static final int accessibility_custom_action_29 = 0x7f080022;

    @IdRes
    public static final int accessibility_custom_action_3 = 0x7f080023;

    @IdRes
    public static final int accessibility_custom_action_30 = 0x7f080024;

    @IdRes
    public static final int accessibility_custom_action_31 = 0x7f080025;

    @IdRes
    public static final int accessibility_custom_action_4 = 0x7f080026;

    @IdRes
    public static final int accessibility_custom_action_5 = 0x7f080027;

    @IdRes
    public static final int accessibility_custom_action_6 = 0x7f080028;

    @IdRes
    public static final int accessibility_custom_action_7 = 0x7f080029;

    @IdRes
    public static final int accessibility_custom_action_8 = 0x7f08002a;

    @IdRes
    public static final int accessibility_custom_action_9 = 0x7f08002b;

    @IdRes
    public static final int action0 = 0x7f08002c;

    @IdRes
    public static final int action_bar = 0x7f08002d;

    @IdRes
    public static final int action_bar_activity_content = 0x7f08002e;

    @IdRes
    public static final int action_bar_container = 0x7f08002f;

    @IdRes
    public static final int action_bar_root = 0x7f080030;

    @IdRes
    public static final int action_bar_spinner = 0x7f080031;

    @IdRes
    public static final int action_bar_subtitle = 0x7f080032;

    @IdRes
    public static final int action_bar_title = 0x7f080033;

    @IdRes
    public static final int action_container = 0x7f080034;

    @IdRes
    public static final int action_context_bar = 0x7f080035;

    @IdRes
    public static final int action_divider = 0x7f080036;

    @IdRes
    public static final int action_image = 0x7f080037;

    @IdRes
    public static final int action_menu_divider = 0x7f080038;

    @IdRes
    public static final int action_menu_presenter = 0x7f080039;

    @IdRes
    public static final int action_mode_bar = 0x7f08003a;

    @IdRes
    public static final int action_mode_bar_stub = 0x7f08003b;

    @IdRes
    public static final int action_mode_close_button = 0x7f08003c;

    @IdRes
    public static final int action_text = 0x7f08003d;

    @IdRes
    public static final int actions = 0x7f08003e;

    @IdRes
    public static final int activity_chooser_view_content = 0x7f08003f;

    @IdRes
    public static final int add = 0x7f080040;

    @IdRes
    public static final int add_Shg_rcv = 0x7f080041;

    @IdRes
    public static final int add_contact_status_back = 0x7f080042;

    @IdRes
    public static final int add_contact_status_next = 0x7f080043;

    @IdRes
    public static final int add_number_participant_back = 0x7f080044;

    @IdRes
    public static final int add_number_participent_next = 0x7f080045;

    @IdRes
    public static final int add_shg_back = 0x7f080046;

    @IdRes
    public static final int add_shg_next = 0x7f080047;

    @IdRes
    public static final int addlocfr_next_btn = 0x7f080048;

    @IdRes
    public static final int alertTitle = 0x7f080049;

    @IdRes
    public static final int all = 0x7f08004a;

    @IdRes
    public static final int always = 0x7f08004b;

    @IdRes
    public static final int appName = 0x7f08004c;

    @IdRes
    public static final int assesment_doneTV = 0x7f08004d;

    @IdRes
    public static final int async = 0x7f08004e;

    @IdRes
    public static final int auto = 0x7f08004f;

    @IdRes
    public static final int back_of_addlocation = 0x7f080050;

    @IdRes
    public static final int barrier = 0x7f080051;

    @IdRes
    public static final int baseline_doneShg = 0x7f080052;

    @IdRes
    public static final int baseline_doneShglinr = 0x7f080053;

    @IdRes
    public static final int baseline_doneTV = 0x7f080054;

    @IdRes
    public static final int baseline_shg = 0x7f080055;

    @IdRes
    public static final int baslineDone_tv = 0x7f080056;

    @IdRes
    public static final int baslineShgDetilsLinearLayout = 0x7f080057;

    @IdRes
    public static final int baslineShgErrorTv = 0x7f080058;

    @IdRes
    public static final int baslineStatusRelativeLayout = 0x7f080059;

    @IdRes
    public static final int basline_customShgMemberTv = 0x7f08005a;

    @IdRes
    public static final int basline_customShgNameTv = 0x7f08005b;

    @IdRes
    public static final int basline_doneShgTv = 0x7f08005c;

    @IdRes
    public static final int basline_pendingShgTV = 0x7f08005d;

    @IdRes
    public static final int basline_shgListRV = 0x7f08005e;

    @IdRes
    public static final int basline_totalShgTv = 0x7f08005f;

    @IdRes
    public static final int beginning = 0x7f080060;

    @IdRes
    public static final int blocking = 0x7f080061;

    @IdRes
    public static final int bottom = 0x7f080062;

    @IdRes
    public static final int bottomLLL = 0x7f080063;

    @IdRes
    public static final int bottomLayout = 0x7f080064;

    @IdRes
    public static final int btnAddTraining = 0x7f080065;

    @IdRes
    public static final int btnBaseline = 0x7f080066;

    @IdRes
    public static final int btnEvaluation = 0x7f080067;

    @IdRes
    public static final int btnMpinProceed = 0x7f080068;

    @IdRes
    public static final int btnReports = 0x7f080069;

    @IdRes
    public static final int button = 0x7f08006a;

    @IdRes
    public static final int buttonImage = 0x7f08006b;

    @IdRes
    public static final int buttonPanel = 0x7f08006c;

    @IdRes
    public static final int button_capture_gps = 0x7f08006d;

    @IdRes
    public static final int button_save = 0x7f08006e;

    @IdRes
    public static final int button_take_photo = 0x7f08006f;

    @IdRes
    public static final int button_upload_data = 0x7f080070;

    @IdRes
    public static final int cancel_action = 0x7f080071;

    @IdRes
    public static final int cancel_button = 0x7f080072;

    @IdRes
    public static final int cancleTV = 0x7f080073;

    @IdRes
    public static final int center = 0x7f080074;

    @IdRes
    public static final int center_horizontal = 0x7f080075;

    @IdRes
    public static final int center_vertical = 0x7f080076;

    @IdRes
    public static final int chains = 0x7f080077;

    @IdRes
    public static final int changLanguage_Rv = 0x7f080078;

    @IdRes
    public static final int checkbox = 0x7f080079;

    @IdRes
    public static final int checkbox_select_module = 0x7f08007a;

    @IdRes
    public static final int checkbox_select_shg_add = 0x7f08007b;

    @IdRes
    public static final int checked = 0x7f08007c;

    @IdRes
    public static final int childNoofDaysLeft = 0x7f08007d;

    @IdRes
    public static final int childQuestionRV = 0x7f08007e;

    @IdRes
    public static final int childRv = 0x7f08007f;

    @IdRes
    public static final int childShgTv = 0x7f080080;

    @IdRes
    public static final int childTV = 0x7f080081;

    @IdRes
    public static final int childevaluationTypeTv = 0x7f080082;

    @IdRes
    public static final int chip = 0x7f080083;

    @IdRes
    public static final int chip1 = 0x7f080084;

    @IdRes
    public static final int chip2 = 0x7f080085;

    @IdRes
    public static final int chip3 = 0x7f080086;

    @IdRes
    public static final int chip_group = 0x7f080087;

    @IdRes
    public static final int chklanguage = 0x7f080088;

    @IdRes
    public static final int chronometer = 0x7f080089;

    @IdRes
    public static final int clear_text = 0x7f08008a;

    @IdRes
    public static final int clip_horizontal = 0x7f08008b;

    @IdRes
    public static final int clip_vertical = 0x7f08008c;

    @IdRes
    public static final int collapseActionView = 0x7f08008d;

    @IdRes
    public static final int confirm_button = 0x7f08008e;

    @IdRes
    public static final int confirm_passwordTIET = 0x7f08008f;

    @IdRes
    public static final int container = 0x7f080090;

    @IdRes
    public static final int content = 0x7f080091;

    @IdRes
    public static final int contentPanel = 0x7f080092;

    @IdRes
    public static final int coordinator = 0x7f080093;

    @IdRes
    public static final int countdataLL = 0x7f080094;

    @IdRes
    public static final int custom = 0x7f080095;

    @IdRes
    public static final int customPanel = 0x7f080096;

    @IdRes
    public static final int cut = 0x7f080097;

    @IdRes
    public static final int dashBoadrd_evaluationBtn = 0x7f080098;

    @IdRes
    public static final int dashBoard_evaluationRV = 0x7f080099;

    @IdRes
    public static final int dash_pendingEvaluatingShgTv = 0x7f08009a;

    @IdRes
    public static final int dashboard_showDetailtv = 0x7f08009b;

    @IdRes
    public static final int dateLinearLayout = 0x7f08009c;

    @IdRes
    public static final int date_TextView = 0x7f08009d;

    @IdRes
    public static final int date_picker_actions = 0x7f08009e;

    @IdRes
    public static final int date_viewTV = 0x7f08009f;

    @IdRes
    public static final int decor_content_parent = 0x7f0800a0;

    @IdRes
    public static final int default_activity_button = 0x7f0800a1;

    @IdRes
    public static final int demo_image = 0x7f0800a2;

    @IdRes
    public static final int design_bottom_sheet = 0x7f0800a3;

    @IdRes
    public static final int design_menu_item_action_area = 0x7f0800a4;

    @IdRes
    public static final int design_menu_item_action_area_stub = 0x7f0800a5;

    @IdRes
    public static final int design_menu_item_text = 0x7f0800a6;

    @IdRes
    public static final int design_navigation_view = 0x7f0800a7;

    @IdRes
    public static final int dialog_CancelBtn = 0x7f0800a8;

    @IdRes
    public static final int dialog_button = 0x7f0800a9;

    @IdRes
    public static final int dialog_nextBtn = 0x7f0800aa;

    @IdRes
    public static final int dilog_enterShgEt = 0x7f0800ab;

    @IdRes
    public static final int dimensions = 0x7f0800ac;

    @IdRes
    public static final int direct = 0x7f0800ad;

    @IdRes
    public static final int disableHome = 0x7f0800ae;

    @IdRes
    public static final int drawer = 0x7f0800af;

    @IdRes
    public static final int dropdown_menu = 0x7f0800b0;

    @IdRes
    public static final int edit_no_of_shg = 0x7f0800b1;

    @IdRes
    public static final int edit_query = 0x7f0800b2;

    @IdRes
    public static final int edittext_other_participent = 0x7f0800b3;

    @IdRes
    public static final int end = 0x7f0800b4;

    @IdRes
    public static final int end_padder = 0x7f0800b5;

    @IdRes
    public static final int english_languageTv = 0x7f0800b6;

    @IdRes
    public static final int enterAlways = 0x7f0800b7;

    @IdRes
    public static final int enterAlwaysCollapsed = 0x7f0800b8;

    @IdRes
    public static final int enter_UserIdEt = 0x7f0800b9;

    @IdRes
    public static final int enter_otpPEET = 0x7f0800ba;

    @IdRes
    public static final int enter_passwordTIET = 0x7f0800bb;

    @IdRes
    public static final int evForm_ShgNameTv = 0x7f0800bc;

    @IdRes
    public static final int evForm_enteredMemberTv = 0x7f0800bd;

    @IdRes
    public static final int ev_ModuleTv = 0x7f0800be;

    @IdRes
    public static final int ev_selectShgTv = 0x7f0800bf;

    @IdRes
    public static final int ev_shgMemberLayout = 0x7f0800c0;

    @IdRes
    public static final int ev_shgModuleDoneLL = 0x7f0800c1;

    @IdRes
    public static final int ev_shgTotal_linearlayOut = 0x7f0800c2;

    @IdRes
    public static final int ev_shg_linearlayOut = 0x7f0800c3;

    @IdRes
    public static final int ev_shg_memberLL = 0x7f0800c4;

    @IdRes
    public static final int ev_topMostLayout = 0x7f0800c5;

    @IdRes
    public static final int ev_totalMemberTv = 0x7f0800c6;

    @IdRes
    public static final int evaluationDoneMessg_tv = 0x7f0800c7;

    @IdRes
    public static final int evaluationDone_tv = 0x7f0800c8;

    @IdRes
    public static final int evaluationStatusLayout = 0x7f0800c9;

    @IdRes
    public static final int evaluationTrainingRelativeLayout = 0x7f0800ca;

    @IdRes
    public static final int evaluation_ChildEt = 0x7f0800cb;

    @IdRes
    public static final int evaluation_ChildQuestionTV = 0x7f0800cc;

    @IdRes
    public static final int evaluation_NoOfDaysLeftTv = 0x7f0800cd;

    @IdRes
    public static final int evaluation_QuestionRv = 0x7f0800ce;

    @IdRes
    public static final int evaluation_SelectVillageSpinner = 0x7f0800cf;

    @IdRes
    public static final int evaluation_ShgNametv = 0x7f0800d0;

    @IdRes
    public static final int evaluation_TypeTv = 0x7f0800d1;

    @IdRes
    public static final int evaluation_dateSelectedTv = 0x7f0800d2;

    @IdRes
    public static final int evaluation_formTitleTv = 0x7f0800d3;

    @IdRes
    public static final int evaluation_member_backBtn = 0x7f0800d4;

    @IdRes
    public static final int evaluation_member_nextBtn = 0x7f0800d5;

    @IdRes
    public static final int evaluation_selectBlockSpinner = 0x7f0800d6;

    @IdRes
    public static final int evaluation_selectGpSpinner = 0x7f0800d7;

    @IdRes
    public static final int evaluation_trainingListLL = 0x7f0800d8;

    @IdRes
    public static final int evealuation_StartDateTv = 0x7f0800d9;

    @IdRes
    public static final int evealuation_enterdMemberRv = 0x7f0800da;

    @IdRes
    public static final int evealuation_shgSelectRecyclerView = 0x7f0800db;

    @IdRes
    public static final int evform_totalMemberTv = 0x7f0800dc;

    @IdRes
    public static final int exitUntilCollapsed = 0x7f0800dd;

    @IdRes
    public static final int expand_activities_button = 0x7f0800de;

    @IdRes
    public static final int expanded_menu = 0x7f0800df;

    @IdRes
    public static final int fade = 0x7f0800e0;

    @IdRes
    public static final int fgt_pin = 0x7f0800e1;

    @IdRes
    public static final int fill = 0x7f0800e2;

    @IdRes
    public static final int fill_horizontal = 0x7f0800e3;

    @IdRes
    public static final int fill_vertical = 0x7f0800e4;

    @IdRes
    public static final int filled = 0x7f0800e5;

    @IdRes
    public static final int firstDate_Tv = 0x7f0800e6;

    @IdRes
    public static final int firstEvaluationLayout = 0x7f0800e7;

    @IdRes
    public static final int fitToContents = 0x7f0800e8;

    @IdRes
    public static final int fixed = 0x7f0800e9;

    @IdRes
    public static final int floating = 0x7f0800ea;

    @IdRes
    public static final int footer = 0x7f0800eb;

    @IdRes
    public static final int forever = 0x7f0800ec;

    @IdRes
    public static final int forgotLL = 0x7f0800ed;

    @IdRes
    public static final int forgotPasswordTV = 0x7f0800ee;

    @IdRes
    public static final int forgot_pin = 0x7f0800ef;

    @IdRes
    public static final int fourthDate_Tv = 0x7f0800f0;

    @IdRes
    public static final int fourthEvaluationLayout = 0x7f0800f1;

    @IdRes
    public static final int fp = 0x7f0800f2;

    @IdRes
    public static final int fragmentContainer = 0x7f0800f3;

    @IdRes
    public static final int fromBottom = 0x7f0800f4;

    @IdRes
    public static final int ghost_view = 0x7f0800f5;

    @IdRes
    public static final int ghost_view_holder = 0x7f0800f6;

    @IdRes
    public static final int gone = 0x7f0800f7;

    @IdRes
    public static final int goto_homeBTN = 0x7f0800f8;

    @IdRes
    public static final int goto_homeLL = 0x7f0800f9;

    @IdRes
    public static final int group1 = 0x7f0800fa;

    @IdRes
    public static final int group_divider = 0x7f0800fb;

    @IdRes
    public static final int groups = 0x7f0800fc;

    @IdRes
    public static final int header2_linearlayout = 0x7f0800fd;

    @IdRes
    public static final int header_relativeLayout = 0x7f0800fe;

    @IdRes
    public static final int hideable = 0x7f0800ff;

    @IdRes
    public static final int highlight = 0x7f080100;

    @IdRes
    public static final int home = 0x7f080101;

    @IdRes
    public static final int homeAsUp = 0x7f080102;

    @IdRes
    public static final int icon = 0x7f080103;

    @IdRes
    public static final int icon_group = 0x7f080104;

    @IdRes
    public static final int id_fgd = 0x7f080105;

    @IdRes
    public static final int ifRoom = 0x7f080106;

    @IdRes
    public static final int image = 0x7f080107;

    @IdRes
    public static final int image_on_take_photo = 0x7f080108;

    @IdRes
    public static final int imgBack = 0x7f080109;

    @IdRes
    public static final int indicator_dots = 0x7f08010a;

    @IdRes
    public static final int info = 0x7f08010b;

    @IdRes
    public static final int invisible = 0x7f08010c;

    @IdRes
    public static final int italic = 0x7f08010d;

    @IdRes
    public static final int item_touch_helper_previous_elevation = 0x7f08010e;

    @IdRes
    public static final int ivHeaderBackground = 0x7f08010f;

    @IdRes
    public static final int labeled = 0x7f080110;

    @IdRes
    public static final int largeLabel = 0x7f080111;

    @IdRes
    public static final int layoutRv = 0x7f080112;

    @IdRes
    public static final int left = 0x7f080113;

    @IdRes
    public static final int line1 = 0x7f080114;

    @IdRes
    public static final int line3 = 0x7f080115;

    @IdRes
    public static final int linerlayout_add_shg = 0x7f080116;

    @IdRes
    public static final int listMode = 0x7f080117;

    @IdRes
    public static final int listTrainingTv = 0x7f080118;

    @IdRes
    public static final int list_item = 0x7f080119;

    @IdRes
    public static final int loc_of_training_viewTV = 0x7f08011a;

    @IdRes
    public static final int locationLinearLayout = 0x7f08011b;

    @IdRes
    public static final int location_ShgDetailRv = 0x7f08011c;

    @IdRes
    public static final int loginBTN = 0x7f08011d;

    @IdRes
    public static final int login_attempts_remainTV = 0x7f08011e;

    @IdRes
    public static final int masked = 0x7f08011f;

    @IdRes
    public static final int media_actions = 0x7f080120;

    @IdRes
    public static final int memberLinearLayout = 0x7f080121;

    @IdRes
    public static final int menu_Logout = 0x7f080122;

    @IdRes
    public static final int menu_addtraining = 0x7f080123;

    @IdRes
    public static final int menu_baseline = 0x7f080124;

    @IdRes
    public static final int menu_changLanguage = 0x7f080125;

    @IdRes
    public static final int menu_dashbord = 0x7f080126;

    @IdRes
    public static final int menu_evaluation = 0x7f080127;

    @IdRes
    public static final int menu_report = 0x7f080128;

    @IdRes
    public static final int message = 0x7f080129;

    @IdRes
    public static final int middle = 0x7f08012a;

    @IdRes
    public static final int middleLayout = 0x7f08012b;

    @IdRes
    public static final int mini = 0x7f08012c;

    @IdRes
    public static final int mobileNoET = 0x7f08012d;

    @IdRes
    public static final int moduleLinearLayout = 0x7f08012e;

    @IdRes
    public static final int modules_viewTV = 0x7f08012f;

    @IdRes
    public static final int month_grid = 0x7f080130;

    @IdRes
    public static final int month_navigation_bar = 0x7f080131;

    @IdRes
    public static final int month_navigation_fragment_toggle = 0x7f080132;

    @IdRes
    public static final int month_navigation_next = 0x7f080133;

    @IdRes
    public static final int month_navigation_previous = 0x7f080134;

    @IdRes
    public static final int month_title = 0x7f080135;

    @IdRes
    public static final int mtrl_calendar_day_selector_frame = 0x7f080136;

    @IdRes
    public static final int mtrl_calendar_days_of_week = 0x7f080137;

    @IdRes
    public static final int mtrl_calendar_frame = 0x7f080138;

    @IdRes
    public static final int mtrl_calendar_main_pane = 0x7f080139;

    @IdRes
    public static final int mtrl_calendar_months = 0x7f08013a;

    @IdRes
    public static final int mtrl_calendar_selection_frame = 0x7f08013b;

    @IdRes
    public static final int mtrl_calendar_text_input_frame = 0x7f08013c;

    @IdRes
    public static final int mtrl_calendar_year_selector_frame = 0x7f08013d;

    @IdRes
    public static final int mtrl_card_checked_layer_id = 0x7f08013e;

    @IdRes
    public static final int mtrl_child_content_container = 0x7f08013f;

    @IdRes
    public static final int mtrl_internal_children_alpha_tag = 0x7f080140;

    @IdRes
    public static final int mtrl_motion_snapshot_view = 0x7f080141;

    @IdRes
    public static final int mtrl_picker_fullscreen = 0x7f080142;

    @IdRes
    public static final int mtrl_picker_header = 0x7f080143;

    @IdRes
    public static final int mtrl_picker_header_selection_text = 0x7f080144;

    @IdRes
    public static final int mtrl_picker_header_title_and_selection = 0x7f080145;

    @IdRes
    public static final int mtrl_picker_header_toggle = 0x7f080146;

    @IdRes
    public static final int mtrl_picker_text_input_date = 0x7f080147;

    @IdRes
    public static final int mtrl_picker_text_input_range_end = 0x7f080148;

    @IdRes
    public static final int mtrl_picker_text_input_range_start = 0x7f080149;

    @IdRes
    public static final int mtrl_picker_title_text = 0x7f08014a;

    @IdRes
    public static final int multiply = 0x7f08014b;

    @IdRes
    public static final int nav_controller_view_tag = 0x7f08014c;

    @IdRes
    public static final int navigateLL = 0x7f08014d;

    @IdRes
    public static final int navigationLL = 0x7f08014e;

    @IdRes
    public static final int navigationView = 0x7f08014f;

    @IdRes
    public static final int navigation_header_container = 0x7f080150;

    @IdRes
    public static final int never = 0x7f080151;

    @IdRes
    public static final int new_passwordBtn = 0x7f080152;

    @IdRes
    public static final int noDataFoundLL = 0x7f080153;

    @IdRes
    public static final int noDataLL = 0x7f080154;

    @IdRes
    public static final int noScroll = 0x7f080155;

    @IdRes
    public static final int no_internet_dialoge = 0x7f080156;

    @IdRes
    public static final int no_of_daysLL = 0x7f080157;

    @IdRes
    public static final int no_of_daysTV = 0x7f080158;

    @IdRes
    public static final int no_of_shg_member_participatedTV = 0x7f080159;

    @IdRes
    public static final int no_of_shg_member_participated_viewTV = 0x7f08015a;

    @IdRes
    public static final int no_of_shg_participatedTV = 0x7f08015b;

    @IdRes
    public static final int no_of_shgsLL = 0x7f08015c;

    @IdRes
    public static final int no_of_shgsTV = 0x7f08015d;

    @IdRes
    public static final int nodata_found = 0x7f08015e;

    @IdRes
    public static final int none = 0x7f08015f;

    @IdRes
    public static final int normal = 0x7f080160;

    @IdRes
    public static final int noshgTV = 0x7f080161;

    @IdRes
    public static final int notificationLL = 0x7f080162;

    @IdRes
    public static final int notificationShgName_tv = 0x7f080163;

    @IdRes
    public static final int notification_background = 0x7f080164;

    @IdRes
    public static final int notification_main_column = 0x7f080165;

    @IdRes
    public static final int notification_main_column_container = 0x7f080166;

    @IdRes
    public static final int notification_rv = 0x7f080167;

    @IdRes
    public static final int off = 0x7f080168;

    @IdRes
    public static final int on = 0x7f080169;

    @IdRes
    public static final int otherLinearLayout = 0x7f08016a;

    @IdRes
    public static final int other_member_participatedTV = 0x7f08016b;

    @IdRes
    public static final int other_member_participated_viewTV = 0x7f08016c;

    @IdRes
    public static final int otp_errorTV = 0x7f08016d;

    @IdRes
    public static final int outline = 0x7f08016e;

    @IdRes
    public static final int packed = 0x7f08016f;

    @IdRes
    public static final int parallax = 0x7f080170;

    @IdRes
    public static final int parent = 0x7f080171;

    @IdRes
    public static final int parentPanel = 0x7f080172;

    @IdRes
    public static final int parentRL = 0x7f080173;

    @IdRes
    public static final int parent_matrix = 0x7f080174;

    @IdRes
    public static final int passwd_fgd = 0x7f080175;

    @IdRes
    public static final int passwordTIET = 0x7f080176;

    @IdRes
    public static final int password_toggle = 0x7f080177;

    @IdRes
    public static final int peekHeight = 0x7f080178;

    @IdRes
    public static final int percent = 0x7f080179;

    @IdRes
    public static final int pin = 0x7f08017a;

    @IdRes
    public static final int pin_lock_view = 0x7f08017b;

    @IdRes
    public static final int popIn = 0x7f08017c;

    @IdRes
    public static final int profile_image = 0x7f08017d;

    @IdRes
    public static final int profile_name = 0x7f08017e;

    @IdRes
    public static final int progress_circular = 0x7f08017f;

    @IdRes
    public static final int progress_horizontal = 0x7f080180;

    @IdRes
    public static final int radio = 0x7f080181;

    @IdRes
    public static final int radioButton = 0x7f080182;

    @IdRes
    public static final int radioButton2 = 0x7f080183;

    @IdRes
    public static final int radioGroup = 0x7f080184;

    @IdRes
    public static final int recyclerview_selected_shg = 0x7f080185;

    @IdRes
    public static final int report_listRV = 0x7f080186;

    @IdRes
    public static final int reset_PassBTN = 0x7f080187;

    @IdRes
    public static final int reset_passwordLL = 0x7f080188;

    @IdRes
    public static final int right = 0x7f080189;

    @IdRes
    public static final int right_icon = 0x7f08018a;

    @IdRes
    public static final int right_side = 0x7f08018b;

    @IdRes
    public static final int root_view = 0x7f08018c;

    @IdRes
    public static final int rounded = 0x7f08018d;

    @IdRes
    public static final int row_index_key = 0x7f08018e;

    @IdRes
    public static final int save_non_transition_alpha = 0x7f08018f;

    @IdRes
    public static final int save_overlay_view = 0x7f080190;

    @IdRes
    public static final int scale = 0x7f080191;

    @IdRes
    public static final int screen = 0x7f080192;

    @IdRes
    public static final int scroll = 0x7f080193;

    @IdRes
    public static final int scrollIndicatorDown = 0x7f080194;

    @IdRes
    public static final int scrollIndicatorUp = 0x7f080195;

    @IdRes
    public static final int scrollView = 0x7f080196;

    @IdRes
    public static final int scrollable = 0x7f080197;

    @IdRes
    public static final int search_badge = 0x7f080198;

    @IdRes
    public static final int search_bar = 0x7f080199;

    @IdRes
    public static final int search_button = 0x7f08019a;

    @IdRes
    public static final int search_close_btn = 0x7f08019b;

    @IdRes
    public static final int search_edit_frame = 0x7f08019c;

    @IdRes
    public static final int search_go_btn = 0x7f08019d;

    @IdRes
    public static final int search_mag_icon = 0x7f08019e;

    @IdRes
    public static final int search_plate = 0x7f08019f;

    @IdRes
    public static final int search_src_text = 0x7f0801a0;

    @IdRes
    public static final int search_voice_btn = 0x7f0801a1;

    @IdRes
    public static final int secodEvaluationLayout = 0x7f0801a2;

    @IdRes
    public static final int secondDate_Tv = 0x7f0801a3;

    @IdRes
    public static final int select_dialog_listview = 0x7f0801a4;

    @IdRes
    public static final int select_enterShgMemberEt = 0x7f0801a5;

    @IdRes
    public static final int select_languageRV = 0x7f0801a6;

    @IdRes
    public static final int select_languageTV = 0x7f0801a7;

    @IdRes
    public static final int select_language_item_viewTV = 0x7f0801a8;

    @IdRes
    public static final int select_module_rcv = 0x7f0801a9;

    @IdRes
    public static final int select_monthMBS = 0x7f0801aa;

    @IdRes
    public static final int select_shgNameMemberTv = 0x7f0801ab;

    @IdRes
    public static final int selected = 0x7f0801ac;

    @IdRes
    public static final int selectedLocationTv = 0x7f0801ad;

    @IdRes
    public static final int selected_languageTV = 0x7f0801ae;

    @IdRes
    public static final int selected_language_item_viewCB = 0x7f0801af;

    @IdRes
    public static final int selectedmonth_view = 0x7f0801b0;

    @IdRes
    public static final int selectmd_back = 0x7f0801b1;

    @IdRes
    public static final int selectmd_next_btn = 0x7f0801b2;

    @IdRes
    public static final int send_otpBTN = 0x7f0801b3;

    @IdRes
    public static final int server_error_dialoge = 0x7f0801b4;

    @IdRes
    public static final int shgEvLocationTv = 0x7f0801b5;

    @IdRes
    public static final int shgEvaluation_nextBtn = 0x7f0801b6;

    @IdRes
    public static final int shgLinearLayout = 0x7f0801b7;

    @IdRes
    public static final int shgLocationTv = 0x7f0801b8;

    @IdRes
    public static final int shgLocation_Tv = 0x7f0801b9;

    @IdRes
    public static final int shgParticipated_viewTV = 0x7f0801ba;

    @IdRes
    public static final int shg_detailRV = 0x7f0801bb;

    @IdRes
    public static final int shortcut = 0x7f0801bc;

    @IdRes
    public static final int showCustom = 0x7f0801bd;

    @IdRes
    public static final int showHome = 0x7f0801be;

    @IdRes
    public static final int showPendingEvaluationDataLL = 0x7f0801bf;

    @IdRes
    public static final int showPendingEvaluationDataNum = 0x7f0801c0;

    @IdRes
    public static final int showTitle = 0x7f0801c1;

    @IdRes
    public static final int skipCollapsed = 0x7f0801c2;

    @IdRes
    public static final int slide = 0x7f0801c3;

    @IdRes
    public static final int smallLabel = 0x7f0801c4;

    @IdRes
    public static final int snackbar_action = 0x7f0801c5;

    @IdRes
    public static final int snackbar_text = 0x7f0801c6;

    @IdRes
    public static final int snap = 0x7f0801c7;

    @IdRes
    public static final int snapMargins = 0x7f0801c8;

    @IdRes
    public static final int spacer = 0x7f0801c9;

    @IdRes
    public static final int spi = 0x7f0801ca;

    @IdRes
    public static final int spinner_select_gp = 0x7f0801cb;

    @IdRes
    public static final int spinner_select_village = 0x7f0801cc;

    @IdRes
    public static final int spinner_selectblock = 0x7f0801cd;

    @IdRes
    public static final int split_action_bar = 0x7f0801ce;

    @IdRes
    public static final int spread = 0x7f0801cf;

    @IdRes
    public static final int spread_inside = 0x7f0801d0;

    @IdRes
    public static final int src_atop = 0x7f0801d1;

    @IdRes
    public static final int src_in = 0x7f0801d2;

    @IdRes
    public static final int src_over = 0x7f0801d3;

    @IdRes
    public static final int standard = 0x7f0801d4;

    @IdRes
    public static final int start = 0x7f0801d5;

    @IdRes
    public static final int status_bar_latest_event_content = 0x7f0801d6;

    @IdRes
    public static final int stretch = 0x7f0801d7;

    @IdRes
    public static final int submenuarrow = 0x7f0801d8;

    @IdRes
    public static final int submit_area = 0x7f0801d9;

    @IdRes
    public static final int submit_fgd = 0x7f0801da;

    @IdRes
    public static final int syncTest = 0x7f0801db;

    @IdRes
    public static final int sync_dataTV = 0x7f0801dc;

    @IdRes
    public static final int tabMode = 0x7f0801dd;

    @IdRes
    public static final int tag_accessibility_actions = 0x7f0801de;

    @IdRes
    public static final int tag_accessibility_clickable_spans = 0x7f0801df;

    @IdRes
    public static final int tag_accessibility_heading = 0x7f0801e0;

    @IdRes
    public static final int tag_accessibility_pane_title = 0x7f0801e1;

    @IdRes
    public static final int tag_screen_reader_focusable = 0x7f0801e2;

    @IdRes
    public static final int tag_transition_group = 0x7f0801e3;

    @IdRes
    public static final int tag_unhandled_key_event_manager = 0x7f0801e4;

    @IdRes
    public static final int tag_unhandled_key_listeners = 0x7f0801e5;

    @IdRes
    public static final int tbTitle = 0x7f0801e6;

    @IdRes
    public static final int test_checkbox_android_button_tint = 0x7f0801e7;

    @IdRes
    public static final int test_checkbox_app_button_tint = 0x7f0801e8;

    @IdRes
    public static final int test_radiobutton_android_button_tint = 0x7f0801e9;

    @IdRes
    public static final int test_radiobutton_app_button_tint = 0x7f0801ea;

    @IdRes
    public static final int text = 0x7f0801eb;

    @IdRes
    public static final int text2 = 0x7f0801ec;

    @IdRes
    public static final int textEnd = 0x7f0801ed;

    @IdRes
    public static final int textSpacerNoButtons = 0x7f0801ee;

    @IdRes
    public static final int textSpacerNoTitle = 0x7f0801ef;

    @IdRes
    public static final int textStart = 0x7f0801f0;

    @IdRes
    public static final int text_add_shg = 0x7f0801f1;

    @IdRes
    public static final int text_block = 0x7f0801f2;

    @IdRes
    public static final int text_content_module = 0x7f0801f3;

    @IdRes
    public static final int text_date_of_training = 0x7f0801f4;

    @IdRes
    public static final int text_date_of_training_addlocation = 0x7f0801f5;

    @IdRes
    public static final int text_gp = 0x7f0801f6;

    @IdRes
    public static final int text_input_end_icon = 0x7f0801f7;

    @IdRes
    public static final int text_input_start_icon = 0x7f0801f8;

    @IdRes
    public static final int text_module_name = 0x7f0801f9;

    @IdRes
    public static final int text_other_member_participating = 0x7f0801fa;

    @IdRes
    public static final int text_selected_shg_name = 0x7f0801fb;

    @IdRes
    public static final int text_shg_member_participating = 0x7f0801fc;

    @IdRes
    public static final int text_shg_participating = 0x7f0801fd;

    @IdRes
    public static final int text_sub_total = 0x7f0801fe;

    @IdRes
    public static final int text_total_participant = 0x7f0801ff;

    @IdRes
    public static final int text_village = 0x7f080200;

    @IdRes
    public static final int textinput_counter = 0x7f080201;

    @IdRes
    public static final int textinput_error = 0x7f080202;

    @IdRes
    public static final int textinput_helper_text = 0x7f080203;

    @IdRes
    public static final int textinput_placeholder = 0x7f080204;

    @IdRes
    public static final int textinput_prefix_text = 0x7f080205;

    @IdRes
    public static final int textinput_suffix_text = 0x7f080206;

    @IdRes
    public static final int thirdDate_Tv = 0x7f080207;

    @IdRes
    public static final int thirdEvaluationLayout = 0x7f080208;

    @IdRes
    public static final int time = 0x7f080209;

    @IdRes
    public static final int titalLL = 0x7f08020a;

    @IdRes
    public static final int title = 0x7f08020b;

    @IdRes
    public static final int titleDividerNoCustom = 0x7f08020c;

    @IdRes
    public static final int titleLayout = 0x7f08020d;

    @IdRes
    public static final int titleTV = 0x7f08020e;

    @IdRes
    public static final int title_template = 0x7f08020f;

    @IdRes
    public static final int toolbar = 0x7f080210;

    @IdRes
    public static final int top = 0x7f080211;

    @IdRes
    public static final int topLL = 0x7f080212;

    @IdRes
    public static final int topLLL = 0x7f080213;

    @IdRes
    public static final int topLinerL = 0x7f080214;

    @IdRes
    public static final int topPanel = 0x7f080215;

    @IdRes
    public static final int topRL = 0x7f080216;

    @IdRes
    public static final int topRelativLayout = 0x7f080217;

    @IdRes
    public static final int topRelativeLayout = 0x7f080218;

    @IdRes
    public static final int topSetLinearLayout = 0x7f080219;

    @IdRes
    public static final int totalLL = 0x7f08021a;

    @IdRes
    public static final int touch_outside = 0x7f08021b;

    @IdRes
    public static final int trainingDate_Tv = 0x7f08021c;

    @IdRes
    public static final int trainingDoneTv = 0x7f08021d;

    @IdRes
    public static final int trainingPendingLayout = 0x7f08021e;

    @IdRes
    public static final int transition_current_scene = 0x7f08021f;

    @IdRes
    public static final int transition_layout_save = 0x7f080220;

    @IdRes
    public static final int transition_position = 0x7f080221;

    @IdRes
    public static final int transition_scene_layoutid_cache = 0x7f080222;

    @IdRes
    public static final int transition_transform = 0x7f080223;

    @IdRes
    public static final int tvAppVersion = 0x7f080224;

    @IdRes
    public static final int tvContent = 0x7f080225;

    @IdRes
    public static final int tvEnterMpin = 0x7f080226;

    @IdRes
    public static final int tvUserMobileNumber = 0x7f080227;

    @IdRes
    public static final int tvUserName = 0x7f080228;

    @IdRes
    public static final int txt_pin_entry = 0x7f080229;

    @IdRes
    public static final int txt_pin_entry_confirm = 0x7f08022a;

    @IdRes
    public static final int unchecked = 0x7f08022b;

    @IdRes
    public static final int uniform = 0x7f08022c;

    @IdRes
    public static final int unlabeled = 0x7f08022d;

    @IdRes
    public static final int unsynced_baselineTV = 0x7f08022e;

    @IdRes
    public static final int unsynced_baselineTVW = 0x7f08022f;

    @IdRes
    public static final int unsynced_evaluationTV = 0x7f080230;

    @IdRes
    public static final int unsynced_evaluationTVW = 0x7f080231;

    @IdRes
    public static final int unsynced_trainingTV = 0x7f080232;

    @IdRes
    public static final int unsynced_trainingTVW = 0x7f080233;

    @IdRes
    public static final int up = 0x7f080234;

    @IdRes
    public static final int useLogo = 0x7f080235;

    @IdRes
    public static final int user_IdTIET = 0x7f080236;

    @IdRes
    public static final int verifieLinerLayout = 0x7f080237;

    @IdRes
    public static final int view = 0x7f080238;

    @IdRes
    public static final int view_offset_helper = 0x7f080239;

    @IdRes
    public static final int view_seperator = 0x7f08023a;

    @IdRes
    public static final int view_seperator1 = 0x7f08023b;

    @IdRes
    public static final int view_seperator2 = 0x7f08023c;

    @IdRes
    public static final int view_seperator3 = 0x7f08023d;

    @IdRes
    public static final int view_seperator4 = 0x7f08023e;

    @IdRes
    public static final int view_seperator5 = 0x7f08023f;

    @IdRes
    public static final int villageModifiedSync_BTN = 0x7f080240;

    @IdRes
    public static final int visible = 0x7f080241;

    @IdRes
    public static final int withText = 0x7f080242;

    @IdRes
    public static final int withinBounds = 0x7f080243;

    @IdRes
    public static final int wrap = 0x7f080244;

    @IdRes
    public static final int wrap_content = 0x7f080245;

    @IdRes
    public static final int zero_corner_chip = 0x7f080246;
  }

  public static final class integer {
    @IntegerRes
    public static final int abc_config_activityDefaultDur = 0x7f090000;

    @IntegerRes
    public static final int abc_config_activityShortDur = 0x7f090001;

    @IntegerRes
    public static final int app_bar_elevation_anim_duration = 0x7f090002;

    @IntegerRes
    public static final int bottom_sheet_slide_duration = 0x7f090003;

    @IntegerRes
    public static final int cancel_button_image_alpha = 0x7f090004;

    @IntegerRes
    public static final int card_flip_time_full = 0x7f090005;

    @IntegerRes
    public static final int card_flip_time_half = 0x7f090006;

    @IntegerRes
    public static final int config_navAnimTime = 0x7f090007;

    @IntegerRes
    public static final int config_tooltipAnimTime = 0x7f090008;

    @IntegerRes
    public static final int design_snackbar_text_max_lines = 0x7f090009;

    @IntegerRes
    public static final int design_tab_indicator_anim_duration_ms = 0x7f09000a;

    @IntegerRes
    public static final int half_slide_up_down_duration = 0x7f09000b;

    @IntegerRes
    public static final int hide_password_duration = 0x7f09000c;

    @IntegerRes
    public static final int mtrl_badge_max_character_count = 0x7f09000d;

    @IntegerRes
    public static final int mtrl_btn_anim_delay_ms = 0x7f09000e;

    @IntegerRes
    public static final int mtrl_btn_anim_duration_ms = 0x7f09000f;

    @IntegerRes
    public static final int mtrl_calendar_header_orientation = 0x7f090010;

    @IntegerRes
    public static final int mtrl_calendar_selection_text_lines = 0x7f090011;

    @IntegerRes
    public static final int mtrl_calendar_year_selector_span = 0x7f090012;

    @IntegerRes
    public static final int mtrl_card_anim_delay_ms = 0x7f090013;

    @IntegerRes
    public static final int mtrl_card_anim_duration_ms = 0x7f090014;

    @IntegerRes
    public static final int mtrl_chip_anim_duration = 0x7f090015;

    @IntegerRes
    public static final int mtrl_tab_indicator_anim_duration_ms = 0x7f090016;

    @IntegerRes
    public static final int show_password_duration = 0x7f090017;

    @IntegerRes
    public static final int slide_up_down_duration = 0x7f090018;

    @IntegerRes
    public static final int slide_up_down_final_value = 0x7f090019;

    @IntegerRes
    public static final int status_bar_notification_info_maxnum = 0x7f09001a;
  }

  public static final class layout {
    @LayoutRes
    public static final int abc_action_bar_title_item = 0x7f0b0000;

    @LayoutRes
    public static final int abc_action_bar_up_container = 0x7f0b0001;

    @LayoutRes
    public static final int abc_action_menu_item_layout = 0x7f0b0002;

    @LayoutRes
    public static final int abc_action_menu_layout = 0x7f0b0003;

    @LayoutRes
    public static final int abc_action_mode_bar = 0x7f0b0004;

    @LayoutRes
    public static final int abc_action_mode_close_item_material = 0x7f0b0005;

    @LayoutRes
    public static final int abc_activity_chooser_view = 0x7f0b0006;

    @LayoutRes
    public static final int abc_activity_chooser_view_list_item = 0x7f0b0007;

    @LayoutRes
    public static final int abc_alert_dialog_button_bar_material = 0x7f0b0008;

    @LayoutRes
    public static final int abc_alert_dialog_material = 0x7f0b0009;

    @LayoutRes
    public static final int abc_alert_dialog_title_material = 0x7f0b000a;

    @LayoutRes
    public static final int abc_cascading_menu_item_layout = 0x7f0b000b;

    @LayoutRes
    public static final int abc_dialog_title_material = 0x7f0b000c;

    @LayoutRes
    public static final int abc_expanded_menu_layout = 0x7f0b000d;

    @LayoutRes
    public static final int abc_list_menu_item_checkbox = 0x7f0b000e;

    @LayoutRes
    public static final int abc_list_menu_item_icon = 0x7f0b000f;

    @LayoutRes
    public static final int abc_list_menu_item_layout = 0x7f0b0010;

    @LayoutRes
    public static final int abc_list_menu_item_radio = 0x7f0b0011;

    @LayoutRes
    public static final int abc_popup_menu_header_item_layout = 0x7f0b0012;

    @LayoutRes
    public static final int abc_popup_menu_item_layout = 0x7f0b0013;

    @LayoutRes
    public static final int abc_screen_content_include = 0x7f0b0014;

    @LayoutRes
    public static final int abc_screen_simple = 0x7f0b0015;

    @LayoutRes
    public static final int abc_screen_simple_overlay_action_mode = 0x7f0b0016;

    @LayoutRes
    public static final int abc_screen_toolbar = 0x7f0b0017;

    @LayoutRes
    public static final int abc_search_dropdown_item_icons_2line = 0x7f0b0018;

    @LayoutRes
    public static final int abc_search_view = 0x7f0b0019;

    @LayoutRes
    public static final int abc_select_dialog_material = 0x7f0b001a;

    @LayoutRes
    public static final int abc_tooltip = 0x7f0b001b;

    @LayoutRes
    public static final int activity_change_mpin = 0x7f0b001c;

    @LayoutRes
    public static final int activity_home = 0x7f0b001d;

    @LayoutRes
    public static final int activity_login = 0x7f0b001e;

    @LayoutRes
    public static final int activity_mpin = 0x7f0b001f;

    @LayoutRes
    public static final int activity_otpverification = 0x7f0b0020;

    @LayoutRes
    public static final int activity_splash = 0x7f0b0021;

    @LayoutRes
    public static final int activity_verify_mpin = 0x7f0b0022;

    @LayoutRes
    public static final int activity_village_modified = 0x7f0b0023;

    @LayoutRes
    public static final int add_content_and_status = 0x7f0b0024;

    @LayoutRes
    public static final int add_number_participants = 0x7f0b0025;

    @LayoutRes
    public static final int add_shg = 0x7f0b0026;

    @LayoutRes
    public static final int add_shg_custom_layout = 0x7f0b0027;

    @LayoutRes
    public static final int addlocation = 0x7f0b0028;

    @LayoutRes
    public static final int base_line_done_layout = 0x7f0b0029;

    @LayoutRes
    public static final int baseline_done_shg_custom_layout = 0x7f0b002a;

    @LayoutRes
    public static final int basline_select_shg_custom_layout = 0x7f0b002b;

    @LayoutRes
    public static final int basline_select_shg_fragment = 0x7f0b002c;

    @LayoutRes
    public static final int child = 0x7f0b002d;

    @LayoutRes
    public static final int common_layout_for_shg_description = 0x7f0b002e;

    @LayoutRes
    public static final int confirm_details = 0x7f0b002f;

    @LayoutRes
    public static final int custom_dialog = 0x7f0b0030;

    @LayoutRes
    public static final int dashboard_evaluation_custom_layout = 0x7f0b0031;

    @LayoutRes
    public static final int dashboard_evaluation_detail_dialog = 0x7f0b0032;

    @LayoutRes
    public static final int dashboard_fragment_layout = 0x7f0b0033;

    @LayoutRes
    public static final int design_bottom_navigation_item = 0x7f0b0034;

    @LayoutRes
    public static final int design_bottom_sheet_dialog = 0x7f0b0035;

    @LayoutRes
    public static final int design_layout_snackbar = 0x7f0b0036;

    @LayoutRes
    public static final int design_layout_snackbar_include = 0x7f0b0037;

    @LayoutRes
    public static final int design_layout_tab_icon = 0x7f0b0038;

    @LayoutRes
    public static final int design_layout_tab_text = 0x7f0b0039;

    @LayoutRes
    public static final int design_menu_item_action_area = 0x7f0b003a;

    @LayoutRes
    public static final int design_navigation_item = 0x7f0b003b;

    @LayoutRes
    public static final int design_navigation_item_header = 0x7f0b003c;

    @LayoutRes
    public static final int design_navigation_item_separator = 0x7f0b003d;

    @LayoutRes
    public static final int design_navigation_item_subheader = 0x7f0b003e;

    @LayoutRes
    public static final int design_navigation_menu = 0x7f0b003f;

    @LayoutRes
    public static final int design_navigation_menu_item = 0x7f0b0040;

    @LayoutRes
    public static final int design_text_input_end_icon = 0x7f0b0041;

    @LayoutRes
    public static final int design_text_input_start_icon = 0x7f0b0042;

    @LayoutRes
    public static final int enter_shg_member_custom_dialog = 0x7f0b0043;

    @LayoutRes
    public static final int evaluation_child_custom_layout = 0x7f0b0044;

    @LayoutRes
    public static final int evaluation_child_shg_detail_layout = 0x7f0b0045;

    @LayoutRes
    public static final int evaluation_form = 0x7f0b0046;

    @LayoutRes
    public static final int evaluation_member = 0x7f0b0047;

    @LayoutRes
    public static final int evaluation_question_custom_layout = 0x7f0b0048;

    @LayoutRes
    public static final int evaluation_select_shg_custom_layout = 0x7f0b0049;

    @LayoutRes
    public static final int evaluation_select_training_custom_lauout = 0x7f0b004a;

    @LayoutRes
    public static final int evaluation_shg_detail_layout = 0x7f0b004b;

    @LayoutRes
    public static final int evaluation_shg_list_fragment = 0x7f0b004c;

    @LayoutRes
    public static final int evaluation_shg_member_custom_layout = 0x7f0b004d;

    @LayoutRes
    public static final int footer_layout = 0x7f0b004e;

    @LayoutRes
    public static final int forgot_passdialog = 0x7f0b004f;

    @LayoutRes
    public static final int fragment_view_report = 0x7f0b0050;

    @LayoutRes
    public static final int language_selection_custom_layout = 0x7f0b0051;

    @LayoutRes
    public static final int language_selection_fragment = 0x7f0b0052;

    @LayoutRes
    public static final int layout_bottom_button = 0x7f0b0053;

    @LayoutRes
    public static final int layout_delete_item = 0x7f0b0054;

    @LayoutRes
    public static final int layout_navigation_header = 0x7f0b0055;

    @LayoutRes
    public static final int layout_number_item = 0x7f0b0056;

    @LayoutRes
    public static final int monthlist_spinnerview = 0x7f0b0057;

    @LayoutRes
    public static final int mtrl_alert_dialog = 0x7f0b0058;

    @LayoutRes
    public static final int mtrl_alert_dialog_actions = 0x7f0b0059;

    @LayoutRes
    public static final int mtrl_alert_dialog_title = 0x7f0b005a;

    @LayoutRes
    public static final int mtrl_alert_select_dialog_item = 0x7f0b005b;

    @LayoutRes
    public static final int mtrl_alert_select_dialog_multichoice = 0x7f0b005c;

    @LayoutRes
    public static final int mtrl_alert_select_dialog_singlechoice = 0x7f0b005d;

    @LayoutRes
    public static final int mtrl_calendar_day = 0x7f0b005e;

    @LayoutRes
    public static final int mtrl_calendar_day_of_week = 0x7f0b005f;

    @LayoutRes
    public static final int mtrl_calendar_days_of_week = 0x7f0b0060;

    @LayoutRes
    public static final int mtrl_calendar_horizontal = 0x7f0b0061;

    @LayoutRes
    public static final int mtrl_calendar_month = 0x7f0b0062;

    @LayoutRes
    public static final int mtrl_calendar_month_labeled = 0x7f0b0063;

    @LayoutRes
    public static final int mtrl_calendar_month_navigation = 0x7f0b0064;

    @LayoutRes
    public static final int mtrl_calendar_months = 0x7f0b0065;

    @LayoutRes
    public static final int mtrl_calendar_vertical = 0x7f0b0066;

    @LayoutRes
    public static final int mtrl_calendar_year = 0x7f0b0067;

    @LayoutRes
    public static final int mtrl_layout_snackbar = 0x7f0b0068;

    @LayoutRes
    public static final int mtrl_layout_snackbar_include = 0x7f0b0069;

    @LayoutRes
    public static final int mtrl_picker_actions = 0x7f0b006a;

    @LayoutRes
    public static final int mtrl_picker_dialog = 0x7f0b006b;

    @LayoutRes
    public static final int mtrl_picker_fullscreen = 0x7f0b006c;

    @LayoutRes
    public static final int mtrl_picker_header_dialog = 0x7f0b006d;

    @LayoutRes
    public static final int mtrl_picker_header_fullscreen = 0x7f0b006e;

    @LayoutRes
    public static final int mtrl_picker_header_selection_text = 0x7f0b006f;

    @LayoutRes
    public static final int mtrl_picker_header_title_text = 0x7f0b0070;

    @LayoutRes
    public static final int mtrl_picker_header_toggle = 0x7f0b0071;

    @LayoutRes
    public static final int mtrl_picker_text_input_date = 0x7f0b0072;

    @LayoutRes
    public static final int mtrl_picker_text_input_date_range = 0x7f0b0073;

    @LayoutRes
    public static final int my_reportlist_view = 0x7f0b0074;

    @LayoutRes
    public static final int no_internet_dialog = 0x7f0b0075;

    @LayoutRes
    public static final int notification_action = 0x7f0b0076;

    @LayoutRes
    public static final int notification_action_tombstone = 0x7f0b0077;

    @LayoutRes
    public static final int notification_media_action = 0x7f0b0078;

    @LayoutRes
    public static final int notification_media_cancel_action = 0x7f0b0079;

    @LayoutRes
    public static final int notification_template_big_media = 0x7f0b007a;

    @LayoutRes
    public static final int notification_template_big_media_custom = 0x7f0b007b;

    @LayoutRes
    public static final int notification_template_big_media_narrow = 0x7f0b007c;

    @LayoutRes
    public static final int notification_template_big_media_narrow_custom = 0x7f0b007d;

    @LayoutRes
    public static final int notification_template_custom_big = 0x7f0b007e;

    @LayoutRes
    public static final int notification_template_icon_group = 0x7f0b007f;

    @LayoutRes
    public static final int notification_template_lines_media = 0x7f0b0080;

    @LayoutRes
    public static final int notification_template_media = 0x7f0b0081;

    @LayoutRes
    public static final int notification_template_media_custom = 0x7f0b0082;

    @LayoutRes
    public static final int notification_template_part_chronometer = 0x7f0b0083;

    @LayoutRes
    public static final int notification_template_part_time = 0x7f0b0084;

    @LayoutRes
    public static final int photo_gps = 0x7f0b0085;

    @LayoutRes
    public static final int select_dialog_item_material = 0x7f0b0086;

    @LayoutRes
    public static final int select_dialog_multichoice_material = 0x7f0b0087;

    @LayoutRes
    public static final int select_dialog_singlechoice_material = 0x7f0b0088;

    @LayoutRes
    public static final int select_language_dialog = 0x7f0b0089;

    @LayoutRes
    public static final int select_language_view_item = 0x7f0b008a;

    @LayoutRes
    public static final int select_module_custom_layout = 0x7f0b008b;

    @LayoutRes
    public static final int select_module_layout = 0x7f0b008c;

    @LayoutRes
    public static final int selected_shg_custom_layout = 0x7f0b008d;

    @LayoutRes
    public static final int server_error_dialog = 0x7f0b008e;

    @LayoutRes
    public static final int shape_dialog = 0x7f0b008f;

    @LayoutRes
    public static final int spinner_textview = 0x7f0b0090;

    @LayoutRes
    public static final int support_simple_spinner_dropdown_item = 0x7f0b0091;

    @LayoutRes
    public static final int test_action_chip = 0x7f0b0092;

    @LayoutRes
    public static final int test_chip_zero_corner_radius = 0x7f0b0093;

    @LayoutRes
    public static final int test_design_checkbox = 0x7f0b0094;

    @LayoutRes
    public static final int test_design_radiobutton = 0x7f0b0095;

    @LayoutRes
    public static final int test_reflow_chipgroup = 0x7f0b0096;

    @LayoutRes
    public static final int test_toolbar = 0x7f0b0097;

    @LayoutRes
    public static final int test_toolbar_custom_background = 0x7f0b0098;

    @LayoutRes
    public static final int test_toolbar_elevation = 0x7f0b0099;

    @LayoutRes
    public static final int test_toolbar_surface = 0x7f0b009a;

    @LayoutRes
    public static final int text_view_with_line_height_from_appearance = 0x7f0b009b;

    @LayoutRes
    public static final int text_view_with_line_height_from_layout = 0x7f0b009c;

    @LayoutRes
    public static final int text_view_with_line_height_from_style = 0x7f0b009d;

    @LayoutRes
    public static final int text_view_with_theme_line_height = 0x7f0b009e;

    @LayoutRes
    public static final int text_view_without_line_height = 0x7f0b009f;

    @LayoutRes
    public static final int toolbar = 0x7f0b00a0;
  }

  public static final class menu {
    @MenuRes
    public static final int menu_navigation_view = 0x7f0c0000;
  }

  public static final class plurals {
    @PluralsRes
    public static final int mtrl_badge_content_description = 0x7f0e0000;
  }

  public static final class string {
    @StringRes
    public static final int GPSAlertDialogMessage = 0x7f0f0000;

    @StringRes
    public static final int GPSAlertDialogTitle = 0x7f0f0001;

    @StringRes
    public static final int INTERNET_BAD = 0x7f0f0002;

    @StringRes
    public static final int INTERNET_MESSAGE = 0x7f0f0003;

    @StringRes
    public static final int NOT_AVAILABLE = 0x7f0f0004;

    @StringRes
    public static final int NO_INTERNET_TITLE = 0x7f0f0005;

    @StringRes
    public static final int Proceed = 0x7f0f0006;

    @StringRes
    public static final int SERVER_ERROR_MESSAGE = 0x7f0f0007;

    @StringRes
    public static final int SERVER_ERROR_TITLE = 0x7f0f0008;

    @StringRes
    public static final int Toast_toast_take_gps = 0x7f0f0009;

    @StringRes
    public static final int abc_action_bar_home_description = 0x7f0f000a;

    @StringRes
    public static final int abc_action_bar_up_description = 0x7f0f000b;

    @StringRes
    public static final int abc_action_menu_overflow_description = 0x7f0f000c;

    @StringRes
    public static final int abc_action_mode_done = 0x7f0f000d;

    @StringRes
    public static final int abc_activity_chooser_view_see_all = 0x7f0f000e;

    @StringRes
    public static final int abc_activitychooserview_choose_application = 0x7f0f000f;

    @StringRes
    public static final int abc_capital_off = 0x7f0f0010;

    @StringRes
    public static final int abc_capital_on = 0x7f0f0011;

    @StringRes
    public static final int abc_menu_alt_shortcut_label = 0x7f0f0012;

    @StringRes
    public static final int abc_menu_ctrl_shortcut_label = 0x7f0f0013;

    @StringRes
    public static final int abc_menu_delete_shortcut_label = 0x7f0f0014;

    @StringRes
    public static final int abc_menu_enter_shortcut_label = 0x7f0f0015;

    @StringRes
    public static final int abc_menu_function_shortcut_label = 0x7f0f0016;

    @StringRes
    public static final int abc_menu_meta_shortcut_label = 0x7f0f0017;

    @StringRes
    public static final int abc_menu_shift_shortcut_label = 0x7f0f0018;

    @StringRes
    public static final int abc_menu_space_shortcut_label = 0x7f0f0019;

    @StringRes
    public static final int abc_menu_sym_shortcut_label = 0x7f0f001a;

    @StringRes
    public static final int abc_prepend_shortcut_label = 0x7f0f001b;

    @StringRes
    public static final int abc_search_hint = 0x7f0f001c;

    @StringRes
    public static final int abc_searchview_description_clear = 0x7f0f001d;

    @StringRes
    public static final int abc_searchview_description_query = 0x7f0f001e;

    @StringRes
    public static final int abc_searchview_description_search = 0x7f0f001f;

    @StringRes
    public static final int abc_searchview_description_submit = 0x7f0f0020;

    @StringRes
    public static final int abc_searchview_description_voice = 0x7f0f0021;

    @StringRes
    public static final int abc_shareactionprovider_share_with = 0x7f0f0022;

    @StringRes
    public static final int abc_shareactionprovider_share_with_application = 0x7f0f0023;

    @StringRes
    public static final int abc_toolbar_collapse_description = 0x7f0f0024;

    @StringRes
    public static final int action_settings = 0x7f0f0025;

    @StringRes
    public static final int add_location = 0x7f0f0026;

    @StringRes
    public static final int add_participants = 0x7f0f0027;

    @StringRes
    public static final int add_shg = 0x7f0f0028;

    @StringRes
    public static final int app_exit_dialog = 0x7f0f0029;

    @StringRes
    public static final int app_name = 0x7f0f002a;

    @StringRes
    public static final int app_version = 0x7f0f002b;

    @StringRes
    public static final int appbar_scrolling_view_behavior = 0x7f0f002c;

    @StringRes
    public static final int are_sure_exit = 0x7f0f002d;

    @StringRes
    public static final int atlest_one_shg_should_selected = 0x7f0f002e;

    @StringRes
    public static final int auto_time_not_enabled = 0x7f0f002f;

    @StringRes
    public static final int back = 0x7f0f0030;

    @StringRes
    public static final int baseline = 0x7f0f0031;

    @StringRes
    public static final int baseline_completed = 0x7f0f0032;

    @StringRes
    public static final int baseline_done = 0x7f0f0033;

    @StringRes
    public static final int baseline_form_title = 0x7f0f0034;

    @StringRes
    public static final int baseline_pending = 0x7f0f0035;

    @StringRes
    public static final int baslineDate = 0x7f0f0036;

    @StringRes
    public static final int baslineDate_completed = 0x7f0f0037;

    @StringRes
    public static final int baslineDate_pending = 0x7f0f0038;

    @StringRes
    public static final int baslineDate_selected_location_msg = 0x7f0f0039;

    @StringRes
    public static final int baslineDate_total_shg = 0x7f0f003a;

    @StringRes
    public static final int basline_entred_member = 0x7f0f003b;

    @StringRes
    public static final int basline_shg_name = 0x7f0f003c;

    @StringRes
    public static final int basline_totalMember = 0x7f0f003d;

    @StringRes
    public static final int block = 0x7f0f003e;

    @StringRes
    public static final int block_grampanchayt_village = 0x7f0f003f;

    @StringRes
    public static final int bottom_sheet_behavior = 0x7f0f0040;

    @StringRes
    public static final int bottom_text = 0x7f0f0041;

    @StringRes
    public static final int cancel = 0x7f0f0042;

    @StringRes
    public static final int cant_select_more_than3 = 0x7f0f0043;

    @StringRes
    public static final int capture_gps = 0x7f0f0044;

    @StringRes
    public static final int character_counter_content_description = 0x7f0f0045;

    @StringRes
    public static final int character_counter_overflowed_content_description = 0x7f0f0046;

    @StringRes
    public static final int character_counter_pattern = 0x7f0f0047;

    @StringRes
    public static final int chip_text = 0x7f0f0048;

    @StringRes
    public static final int clear_text_end_icon_content_description = 0x7f0f0049;

    @StringRes
    public static final int closeDrawer = 0x7f0f004a;

    @StringRes
    public static final int confirm_details = 0x7f0f004b;

    @StringRes
    public static final int confirm_mpin = 0x7f0f004c;

    @StringRes
    public static final int confirm_pass = 0x7f0f004d;

    @StringRes
    public static final int content_module = 0x7f0f004e;

    @StringRes
    public static final int dashboard_shg_left = 0x7f0f004f;

    @StringRes
    public static final int dashboard_shg_msg = 0x7f0f0050;

    @StringRes
    public static final int dashboard_show_detail = 0x7f0f0051;

    @StringRes
    public static final int data_info = 0x7f0f0052;

    @StringRes
    public static final int data_infoB = 0x7f0f0053;

    @StringRes
    public static final int data_infoE = 0x7f0f0054;

    @StringRes
    public static final int data_infoT = 0x7f0f0055;

    @StringRes
    public static final int data_not_found_tost = 0x7f0f0056;

    @StringRes
    public static final int data_save_succesfully = 0x7f0f0057;

    @StringRes
    public static final int date = 0x7f0f0058;

    @StringRes
    public static final int date_of_evaluation = 0x7f0f0059;

    @StringRes
    public static final int date_of_training = 0x7f0f005a;

    @StringRes
    public static final int device = 0x7f0f005b;

    @StringRes
    public static final int dialog_evaluation_chang_answer = 0x7f0f005c;

    @StringRes
    public static final int dialog_save_data = 0x7f0f005d;

    @StringRes
    public static final int dialog_save_data_for_evaluation = 0x7f0f005e;

    @StringRes
    public static final int dialog_web_request_failes = 0x7f0f005f;

    @StringRes
    public static final int do_you_want_to_save_detail = 0x7f0f0060;

    @StringRes
    public static final int enter_id = 0x7f0f0061;

    @StringRes
    public static final int enter_mobile = 0x7f0f0062;

    @StringRes
    public static final int enter_mpin = 0x7f0f0063;

    @StringRes
    public static final int enter_number_of_participants = 0x7f0f0064;

    @StringRes
    public static final int enter_number_of_participants_baseline = 0x7f0f0065;

    @StringRes
    public static final int enter_otp = 0x7f0f0066;

    @StringRes
    public static final int enter_participant = 0x7f0f0067;

    @StringRes
    public static final int enter_pass = 0x7f0f0068;

    @StringRes
    public static final int enter_your_password = 0x7f0f0069;

    @StringRes
    public static final int error_confirm_password = 0x7f0f006a;

    @StringRes
    public static final int error_confirm_password_not_matched = 0x7f0f006b;

    @StringRes
    public static final int error_icon_content_description = 0x7f0f006c;

    @StringRes
    public static final int error_invalid_password = 0x7f0f006d;

    @StringRes
    public static final int error_mobileno = 0x7f0f006e;

    @StringRes
    public static final int error_mobileno_notmatched = 0x7f0f006f;

    @StringRes
    public static final int error_mobileno_notregistered = 0x7f0f0070;

    @StringRes
    public static final int error_otp = 0x7f0f0071;

    @StringRes
    public static final int error_password = 0x7f0f0072;

    @StringRes
    public static final int error_userId = 0x7f0f0073;

    @StringRes
    public static final int evaluation_Question1 = 0x7f0f0074;

    @StringRes
    public static final int evaluation_Question10 = 0x7f0f0075;

    @StringRes
    public static final int evaluation_Question2 = 0x7f0f0076;

    @StringRes
    public static final int evaluation_Question3 = 0x7f0f0077;

    @StringRes
    public static final int evaluation_Question4 = 0x7f0f0078;

    @StringRes
    public static final int evaluation_Question5 = 0x7f0f0079;

    @StringRes
    public static final int evaluation_Question6 = 0x7f0f007a;

    @StringRes
    public static final int evaluation_Question7 = 0x7f0f007b;

    @StringRes
    public static final int evaluation_Question8 = 0x7f0f007c;

    @StringRes
    public static final int evaluation_Question9 = 0x7f0f007d;

    @StringRes
    public static final int evaluation_Question_Title1 = 0x7f0f007e;

    @StringRes
    public static final int evaluation_Question_Title2 = 0x7f0f007f;

    @StringRes
    public static final int evaluation_Question_Title3 = 0x7f0f0080;

    @StringRes
    public static final int evaluation_Question_Title4 = 0x7f0f0081;

    @StringRes
    public static final int evaluation_date_msg = 0x7f0f0082;

    @StringRes
    public static final int evaluation_day_msg = 0x7f0f0083;

    @StringRes
    public static final int evaluation_days_left = 0x7f0f0084;

    @StringRes
    public static final int evaluation_days_msg = 0x7f0f0085;

    @StringRes
    public static final int evaluation_done_msg = 0x7f0f0086;

    @StringRes
    public static final int evaluation_no_days_left = 0x7f0f0087;

    @StringRes
    public static final int evaluation_number_of_training = 0x7f0f0088;

    @StringRes
    public static final int evaluation_ro_evaluation_msg = 0x7f0f0089;

    @StringRes
    public static final int evaluation_ro_evaluation_msg_first = 0x7f0f008a;

    @StringRes
    public static final int evaluation_shg_list = 0x7f0f008b;

    @StringRes
    public static final int evaluation_shg_not_found_msg = 0x7f0f008c;

    @StringRes
    public static final int evaluation_start_msg = 0x7f0f008d;

    @StringRes
    public static final int evaluation_training_date_msg = 0x7f0f008e;

    @StringRes
    public static final int exposed_dropdown_menu_content_description = 0x7f0f008f;

    @StringRes
    public static final int fab_transformation_scrim_behavior = 0x7f0f0090;

    @StringRes
    public static final int fab_transformation_sheet_behavior = 0x7f0f0091;

    @StringRes
    public static final int faild_code = 0x7f0f0092;

    @StringRes
    public static final int forgot_mpin = 0x7f0f0093;

    @StringRes
    public static final int forgot_password = 0x7f0f0094;

    @StringRes
    public static final int forgotpin = 0x7f0f0095;

    @StringRes
    public static final int gotoHome = 0x7f0f0096;

    @StringRes
    public static final int gp = 0x7f0f0097;

    @StringRes
    public static final int gps_not_enabled = 0x7f0f0098;

    @StringRes
    public static final int hello_blank_fragment = 0x7f0f0099;

    @StringRes
    public static final int hide_bottom_view_on_scroll_behavior = 0x7f0f009a;

    @StringRes
    public static final int icon_content_description = 0x7f0f009b;

    @StringRes
    public static final int info = 0x7f0f009c;

    @StringRes
    public static final int info_confirmation = 0x7f0f009d;

    @StringRes
    public static final int item_view_role_description = 0x7f0f009e;

    @StringRes
    public static final int languge_change = 0x7f0f009f;

    @StringRes
    public static final int list_of_shg = 0x7f0f00a0;

    @StringRes
    public static final int list_of_shg_for_evaluation = 0x7f0f00a1;

    @StringRes
    public static final int list_of_shg_modules = 0x7f0f00a2;

    @StringRes
    public static final int list_of_shg_name = 0x7f0f00a3;

    @StringRes
    public static final int list_of_shg_selection_evaluation = 0x7f0f00a4;

    @StringRes
    public static final int list_of_shg_training_date = 0x7f0f00a5;

    @StringRes
    public static final int location = 0x7f0f00a6;

    @StringRes
    public static final int locationOfTraining = 0x7f0f00a7;

    @StringRes
    public static final int login = 0x7f0f00a8;

    @StringRes
    public static final int login_attempt_failed = 0x7f0f00a9;

    @StringRes
    public static final int login_attempts_msgE = 0x7f0f00aa;

    @StringRes
    public static final int login_attempts_msgS = 0x7f0f00ab;

    @StringRes
    public static final int login_massege = 0x7f0f00ac;

    @StringRes
    public static final int material_slider_range_end = 0x7f0f00ad;

    @StringRes
    public static final int material_slider_range_start = 0x7f0f00ae;

    @StringRes
    public static final int menu_change_Language = 0x7f0f00af;

    @StringRes
    public static final int menu_gallery = 0x7f0f00b0;

    @StringRes
    public static final int menu_home = 0x7f0f00b1;

    @StringRes
    public static final int menu_logout = 0x7f0f00b2;

    @StringRes
    public static final int menu_send = 0x7f0f00b3;

    @StringRes
    public static final int menu_share = 0x7f0f00b4;

    @StringRes
    public static final int menu_slideshow = 0x7f0f00b5;

    @StringRes
    public static final int menu_tools = 0x7f0f00b6;

    @StringRes
    public static final int minimum_half_members_required_for_baseline = 0x7f0f00b7;

    @StringRes
    public static final int module_Evaluation = 0x7f0f00b8;

    @StringRes
    public static final int module_Evaluation3 = 0x7f0f00b9;

    @StringRes
    public static final int module_Report = 0x7f0f00ba;

    @StringRes
    public static final int module_add_Training = 0x7f0f00bb;

    @StringRes
    public static final int modules = 0x7f0f00bc;

    @StringRes
    public static final int msg_logout = 0x7f0f00bd;

    @StringRes
    public static final int mtrl_badge_numberless_content_description = 0x7f0f00be;

    @StringRes
    public static final int mtrl_chip_close_icon_content_description = 0x7f0f00bf;

    @StringRes
    public static final int mtrl_exceed_max_badge_number_content_description = 0x7f0f00c0;

    @StringRes
    public static final int mtrl_exceed_max_badge_number_suffix = 0x7f0f00c1;

    @StringRes
    public static final int mtrl_picker_a11y_next_month = 0x7f0f00c2;

    @StringRes
    public static final int mtrl_picker_a11y_prev_month = 0x7f0f00c3;

    @StringRes
    public static final int mtrl_picker_announce_current_selection = 0x7f0f00c4;

    @StringRes
    public static final int mtrl_picker_cancel = 0x7f0f00c5;

    @StringRes
    public static final int mtrl_picker_confirm = 0x7f0f00c6;

    @StringRes
    public static final int mtrl_picker_date_header_selected = 0x7f0f00c7;

    @StringRes
    public static final int mtrl_picker_date_header_title = 0x7f0f00c8;

    @StringRes
    public static final int mtrl_picker_date_header_unselected = 0x7f0f00c9;

    @StringRes
    public static final int mtrl_picker_day_of_week_column_header = 0x7f0f00ca;

    @StringRes
    public static final int mtrl_picker_invalid_format = 0x7f0f00cb;

    @StringRes
    public static final int mtrl_picker_invalid_format_example = 0x7f0f00cc;

    @StringRes
    public static final int mtrl_picker_invalid_format_use = 0x7f0f00cd;

    @StringRes
    public static final int mtrl_picker_invalid_range = 0x7f0f00ce;

    @StringRes
    public static final int mtrl_picker_navigate_to_year_description = 0x7f0f00cf;

    @StringRes
    public static final int mtrl_picker_out_of_range = 0x7f0f00d0;

    @StringRes
    public static final int mtrl_picker_range_header_only_end_selected = 0x7f0f00d1;

    @StringRes
    public static final int mtrl_picker_range_header_only_start_selected = 0x7f0f00d2;

    @StringRes
    public static final int mtrl_picker_range_header_selected = 0x7f0f00d3;

    @StringRes
    public static final int mtrl_picker_range_header_title = 0x7f0f00d4;

    @StringRes
    public static final int mtrl_picker_range_header_unselected = 0x7f0f00d5;

    @StringRes
    public static final int mtrl_picker_save = 0x7f0f00d6;

    @StringRes
    public static final int mtrl_picker_text_input_date_hint = 0x7f0f00d7;

    @StringRes
    public static final int mtrl_picker_text_input_date_range_end_hint = 0x7f0f00d8;

    @StringRes
    public static final int mtrl_picker_text_input_date_range_start_hint = 0x7f0f00d9;

    @StringRes
    public static final int mtrl_picker_text_input_day_abbr = 0x7f0f00da;

    @StringRes
    public static final int mtrl_picker_text_input_month_abbr = 0x7f0f00db;

    @StringRes
    public static final int mtrl_picker_text_input_year_abbr = 0x7f0f00dc;

    @StringRes
    public static final int mtrl_picker_toggle_to_calendar_input_mode = 0x7f0f00dd;

    @StringRes
    public static final int mtrl_picker_toggle_to_day_selection = 0x7f0f00de;

    @StringRes
    public static final int mtrl_picker_toggle_to_text_input_mode = 0x7f0f00df;

    @StringRes
    public static final int mtrl_picker_toggle_to_year_selection = 0x7f0f00e0;

    @StringRes
    public static final int nav_app_bar_navigate_up_description = 0x7f0f00e1;

    @StringRes
    public static final int nav_app_bar_open_drawer_description = 0x7f0f00e2;

    @StringRes
    public static final int nav_header_desc = 0x7f0f00e3;

    @StringRes
    public static final int nav_header_subtitle = 0x7f0f00e4;

    @StringRes
    public static final int nav_header_title = 0x7f0f00e5;

    @StringRes
    public static final int navigation_drawer_close = 0x7f0f00e6;

    @StringRes
    public static final int navigation_drawer_open = 0x7f0f00e7;

    @StringRes
    public static final int next = 0x7f0f00e8;

    @StringRes
    public static final int no_internet_dialog = 0x7f0f00e9;

    @StringRes
    public static final int no_pending_shgB = 0x7f0f00ea;

    @StringRes
    public static final int nodataFound = 0x7f0f00eb;

    @StringRes
    public static final int noofDays = 0x7f0f00ec;

    @StringRes
    public static final int noofShgs = 0x7f0f00ed;

    @StringRes
    public static final int noofshgsassesmentdone = 0x7f0f00ee;

    @StringRes
    public static final int noofshgsbaselinedone = 0x7f0f00ef;

    @StringRes
    public static final int not_eligible_for_evolution = 0x7f0f00f0;

    @StringRes
    public static final int nubmber_of_shg_member_participating = 0x7f0f00f1;

    @StringRes
    public static final int number_of_selected_shg_for_evaluation = 0x7f0f00f2;

    @StringRes
    public static final int number_of_shg_member = 0x7f0f00f3;

    @StringRes
    public static final int number_of_shg_participating = 0x7f0f00f4;

    @StringRes
    public static final int number_of_shg_present_in_meeting = 0x7f0f00f5;

    @StringRes
    public static final int openDrawer = 0x7f0f00f6;

    @StringRes
    public static final int opt_round_card_view_name = 0x7f0f00f7;

    @StringRes
    public static final int otherMemberParticipated = 0x7f0f00f8;

    @StringRes
    public static final int other_member_participating = 0x7f0f00f9;

    @StringRes
    public static final int other_particepant = 0x7f0f00fa;

    @StringRes
    public static final int otp_creation = 0x7f0f00fb;

    @StringRes
    public static final int otp_greeting = 0x7f0f00fc;

    @StringRes
    public static final int otp_massage = 0x7f0f00fd;

    @StringRes
    public static final int password_toggle_content_description = 0x7f0f00fe;

    @StringRes
    public static final int path_password_eye = 0x7f0f00ff;

    @StringRes
    public static final int path_password_eye_mask_strike_through = 0x7f0f0100;

    @StringRes
    public static final int path_password_eye_mask_visible = 0x7f0f0101;

    @StringRes
    public static final int path_password_strike_through = 0x7f0f0102;

    @StringRes
    public static final int pending_evaluation = 0x7f0f0103;

    @StringRes
    public static final int pending_evaluation1 = 0x7f0f0104;

    @StringRes
    public static final int pending_shgs = 0x7f0f0105;

    @StringRes
    public static final int photo_gps = 0x7f0f0106;

    @StringRes
    public static final int please_enter_other_participant = 0x7f0f0107;

    @StringRes
    public static final int please_enter_valid_pin_details = 0x7f0f0108;

    @StringRes
    public static final int please_enter_your_id = 0x7f0f0109;

    @StringRes
    public static final int please_enter_your_password = 0x7f0f010a;

    @StringRes
    public static final int please_fill_proper_details = 0x7f0f010b;

    @StringRes
    public static final int please_select_atleast_half_member = 0x7f0f010c;

    @StringRes
    public static final int please_select_atleast_one_member = 0x7f0f010d;

    @StringRes
    public static final int please_select_atleast_one_module = 0x7f0f010e;

    @StringRes
    public static final int please_select_atleast_one_shg = 0x7f0f010f;

    @StringRes
    public static final int please_select_atlest_one_mod = 0x7f0f0110;

    @StringRes
    public static final int reset_pass_TV = 0x7f0f0111;

    @StringRes
    public static final int save = 0x7f0f0112;

    @StringRes
    public static final int search_menu_title = 0x7f0f0113;

    @StringRes
    public static final int selectMonth = 0x7f0f0114;

    @StringRes
    public static final int selectShg = 0x7f0f0115;

    @StringRes
    public static final int select_block = 0x7f0f0116;

    @StringRes
    public static final int select_content = 0x7f0f0117;

    @StringRes
    public static final int select_date = 0x7f0f0118;

    @StringRes
    public static final int select_gp = 0x7f0f0119;

    @StringRes
    public static final int select_language = 0x7f0f011a;

    @StringRes
    public static final int select_location = 0x7f0f011b;

    @StringRes
    public static final int select_location_de = 0x7f0f011c;

    @StringRes
    public static final int select_module = 0x7f0f011d;

    @StringRes
    public static final int select_month = 0x7f0f011e;

    @StringRes
    public static final int select_village = 0x7f0f011f;

    @StringRes
    public static final int send_otp = 0x7f0f0120;

    @StringRes
    public static final int server_error_device_info = 0x7f0f0121;

    @StringRes
    public static final int server_error_device_info_massege = 0x7f0f0122;

    @StringRes
    public static final int server_error_dialog = 0x7f0f0123;

    @StringRes
    public static final int server_error_invalid_date = 0x7f0f0124;

    @StringRes
    public static final int server_error_invalid_date_massege = 0x7f0f0125;

    @StringRes
    public static final int server_error_invalid_password = 0x7f0f0126;

    @StringRes
    public static final int server_error_invalid_password_massege = 0x7f0f0127;

    @StringRes
    public static final int server_error_invalid_userId = 0x7f0f0128;

    @StringRes
    public static final int server_error_invalid_userId_massege = 0x7f0f0129;

    @StringRes
    public static final int server_error_invalid_version = 0x7f0f012a;

    @StringRes
    public static final int server_error_invalid_version_massege = 0x7f0f012b;

    @StringRes
    public static final int shgMemberParticipated = 0x7f0f012c;

    @StringRes
    public static final int shgParticipated = 0x7f0f012d;

    @StringRes
    public static final int shg_for_baseline = 0x7f0f012e;

    @StringRes
    public static final int shg_list_baseline = 0x7f0f012f;

    @StringRes
    public static final int shg_not_found = 0x7f0f0130;

    @StringRes
    public static final int status = 0x7f0f0131;

    @StringRes
    public static final int status_bar_notification_info_overflow = 0x7f0f0132;

    @StringRes
    public static final int str_no = 0x7f0f0133;

    @StringRes
    public static final int str_yes = 0x7f0f0134;

    @StringRes
    public static final int sub_total_shg_member = 0x7f0f0135;

    @StringRes
    public static final int submit = 0x7f0f0136;

    @StringRes
    public static final int suggestion = 0x7f0f0137;

    @StringRes
    public static final int sync_data = 0x7f0f0138;

    @StringRes
    public static final int take_photo = 0x7f0f0139;

    @StringRes
    public static final int title_activity_home = 0x7f0f013a;

    @StringRes
    public static final int title_activity_village_modified = 0x7f0f013b;

    @StringRes
    public static final int title_addtraining = 0x7f0f013c;

    @StringRes
    public static final int title_baseline = 0x7f0f013d;

    @StringRes
    public static final int title_dashbord = 0x7f0f013e;

    @StringRes
    public static final int title_viewreport = 0x7f0f013f;

    @StringRes
    public static final int toast_FAIL = 0x7f0f0140;

    @StringRes
    public static final int toast_basline_entervalid_number = 0x7f0f0141;

    @StringRes
    public static final int toast_basline_shg_not_found = 0x7f0f0142;

    @StringRes
    public static final int toast_data_saved = 0x7f0f0143;

    @StringRes
    public static final int toast_enter_member_msg = 0x7f0f0144;

    @StringRes
    public static final int toast_enter_question = 0x7f0f0145;

    @StringRes
    public static final int toast_error = 0x7f0f0146;

    @StringRes
    public static final int toast_erros_shg_not_found = 0x7f0f0147;

    @StringRes
    public static final int toast_evaluation_form_fill_msg = 0x7f0f0148;

    @StringRes
    public static final int toast_evaluation_morethen_zero = 0x7f0f0149;

    @StringRes
    public static final int toast_evaluation_shgMember = 0x7f0f014a;

    @StringRes
    public static final int toast_fill_details = 0x7f0f014b;

    @StringRes
    public static final int toast_language_chang = 0x7f0f014c;

    @StringRes
    public static final int toast_language_max_selection = 0x7f0f014d;

    @StringRes
    public static final int toast_language_min_selection = 0x7f0f014e;

    @StringRes
    public static final int toast_location_msg = 0x7f0f014f;

    @StringRes
    public static final int toast_nodata_found = 0x7f0f0150;

    @StringRes
    public static final int toast_nologinid = 0x7f0f0151;

    @StringRes
    public static final int toast_nonot_matched_fromlocal = 0x7f0f0152;

    @StringRes
    public static final int toast_not_elegible = 0x7f0f0153;

    @StringRes
    public static final int toast_otp_wrong = 0x7f0f0154;

    @StringRes
    public static final int toast_registered_mobile = 0x7f0f0155;

    @StringRes
    public static final int toast_select_village_msg = 0x7f0f0156;

    @StringRes
    public static final int toast_shg_not_found_evaluation = 0x7f0f0157;

    @StringRes
    public static final int toast_take_picture = 0x7f0f0158;

    @StringRes
    public static final int total = 0x7f0f0159;

    @StringRes
    public static final int total_number_shg = 0x7f0f015a;

    @StringRes
    public static final int total_participants = 0x7f0f015b;

    @StringRes
    public static final int unlock_first = 0x7f0f015c;

    @StringRes
    public static final int unsync_baseline = 0x7f0f015d;

    @StringRes
    public static final int unsync_details = 0x7f0f015e;

    @StringRes
    public static final int unsync_evaluation = 0x7f0f015f;

    @StringRes
    public static final int unsync_training = 0x7f0f0160;

    @StringRes
    public static final int unsynced_Bshgs = 0x7f0f0161;

    @StringRes
    public static final int unsynced_Eshgs = 0x7f0f0162;

    @StringRes
    public static final int unsynced_training = 0x7f0f0163;

    @StringRes
    public static final int upload_data = 0x7f0f0164;

    @StringRes
    public static final int user_id = 0x7f0f0165;

    @StringRes
    public static final int user_password = 0x7f0f0166;

    @StringRes
    public static final int value_from_backend = 0x7f0f0167;

    @StringRes
    public static final int viewReportLabel = 0x7f0f0168;

    @StringRes
    public static final int village = 0x7f0f0169;
  }

  public static final class style {
    @StyleRes
    public static final int AlertDialog_AppCompat = 0x7f100000;

    @StyleRes
    public static final int AlertDialog_AppCompat_Light = 0x7f100001;

    @StyleRes
    public static final int AndroidThemeColorAccentYellow = 0x7f100002;

    @StyleRes
    public static final int Animation_AppCompat_Dialog = 0x7f100003;

    @StyleRes
    public static final int Animation_AppCompat_DropDownUp = 0x7f100004;

    @StyleRes
    public static final int Animation_AppCompat_Tooltip = 0x7f100005;

    @StyleRes
    public static final int Animation_Design_BottomSheetDialog = 0x7f100006;

    @StyleRes
    public static final int Animation_MaterialComponents_BottomSheetDialog = 0x7f100007;

    @StyleRes
    public static final int AppTheme = 0x7f100008;

    @StyleRes
    public static final int AppTheme_AppBarOverlay = 0x7f100009;

    @StyleRes
    public static final int AppTheme_NoActionBar = 0x7f10000a;

    @StyleRes
    public static final int AppTheme_PopupOverlay = 0x7f10000b;

    @StyleRes
    public static final int AppTheme_notActionBar = 0x7f10000c;

    @StyleRes
    public static final int Base_AlertDialog_AppCompat = 0x7f10000d;

    @StyleRes
    public static final int Base_AlertDialog_AppCompat_Light = 0x7f10000e;

    @StyleRes
    public static final int Base_Animation_AppCompat_Dialog = 0x7f10000f;

    @StyleRes
    public static final int Base_Animation_AppCompat_DropDownUp = 0x7f100010;

    @StyleRes
    public static final int Base_Animation_AppCompat_Tooltip = 0x7f100011;

    @StyleRes
    public static final int Base_CardView = 0x7f100012;

    @StyleRes
    public static final int Base_DialogWindowTitle_AppCompat = 0x7f100013;

    @StyleRes
    public static final int Base_DialogWindowTitleBackground_AppCompat = 0x7f100014;

    @StyleRes
    public static final int Base_MaterialAlertDialog_MaterialComponents_Title_Icon = 0x7f100015;

    @StyleRes
    public static final int Base_MaterialAlertDialog_MaterialComponents_Title_Panel = 0x7f100016;

    @StyleRes
    public static final int Base_MaterialAlertDialog_MaterialComponents_Title_Text = 0x7f100017;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat = 0x7f100018;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Body1 = 0x7f100019;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Body2 = 0x7f10001a;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Button = 0x7f10001b;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Caption = 0x7f10001c;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display1 = 0x7f10001d;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display2 = 0x7f10001e;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display3 = 0x7f10001f;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display4 = 0x7f100020;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Headline = 0x7f100021;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Inverse = 0x7f100022;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Large = 0x7f100023;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Large_Inverse = 0x7f100024;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large = 0x7f100025;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small = 0x7f100026;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Medium = 0x7f100027;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Medium_Inverse = 0x7f100028;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Menu = 0x7f100029;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_SearchResult = 0x7f10002a;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_SearchResult_Subtitle = 0x7f10002b;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_SearchResult_Title = 0x7f10002c;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Small = 0x7f10002d;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Small_Inverse = 0x7f10002e;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Subhead = 0x7f10002f;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Subhead_Inverse = 0x7f100030;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Title = 0x7f100031;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Title_Inverse = 0x7f100032;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Tooltip = 0x7f100033;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Menu = 0x7f100034;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle = 0x7f100035;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse = 0x7f100036;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Title = 0x7f100037;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse = 0x7f100038;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle = 0x7f100039;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionMode_Title = 0x7f10003a;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button = 0x7f10003b;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored = 0x7f10003c;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button_Colored = 0x7f10003d;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button_Inverse = 0x7f10003e;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_DropDownItem = 0x7f10003f;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_PopupMenu_Header = 0x7f100040;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_PopupMenu_Large = 0x7f100041;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_PopupMenu_Small = 0x7f100042;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Switch = 0x7f100043;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem = 0x7f100044;

    @StyleRes
    public static final int Base_TextAppearance_MaterialComponents_Badge = 0x7f100045;

    @StyleRes
    public static final int Base_TextAppearance_MaterialComponents_Button = 0x7f100046;

    @StyleRes
    public static final int Base_TextAppearance_MaterialComponents_Headline6 = 0x7f100047;

    @StyleRes
    public static final int Base_TextAppearance_MaterialComponents_Subtitle2 = 0x7f100048;

    @StyleRes
    public static final int Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item = 0x7f100049;

    @StyleRes
    public static final int Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle = 0x7f10004a;

    @StyleRes
    public static final int Base_TextAppearance_Widget_AppCompat_Toolbar_Title = 0x7f10004b;

    @StyleRes
    public static final int Base_Theme_AppCompat = 0x7f10004c;

    @StyleRes
    public static final int Base_Theme_AppCompat_CompactMenu = 0x7f10004d;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog = 0x7f10004e;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog_Alert = 0x7f10004f;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog_FixedSize = 0x7f100050;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog_MinWidth = 0x7f100051;

    @StyleRes
    public static final int Base_Theme_AppCompat_DialogWhenLarge = 0x7f100052;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light = 0x7f100053;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_DarkActionBar = 0x7f100054;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog = 0x7f100055;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog_Alert = 0x7f100056;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog_FixedSize = 0x7f100057;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog_MinWidth = 0x7f100058;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_DialogWhenLarge = 0x7f100059;

    @StyleRes
    public static final int Base_Theme_MaterialComponents = 0x7f10005a;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Bridge = 0x7f10005b;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_CompactMenu = 0x7f10005c;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog = 0x7f10005d;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog_Alert = 0x7f10005e;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog_Bridge = 0x7f10005f;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog_FixedSize = 0x7f100060;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog_MinWidth = 0x7f100061;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_DialogWhenLarge = 0x7f100062;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light = 0x7f100063;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Bridge = 0x7f100064;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_DarkActionBar = 0x7f100065;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_DarkActionBar_Bridge = 0x7f100066;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog = 0x7f100067;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog_Alert = 0x7f100068;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog_Bridge = 0x7f100069;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog_FixedSize = 0x7f10006a;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog_MinWidth = 0x7f10006b;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_DialogWhenLarge = 0x7f10006c;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat = 0x7f10006d;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_ActionBar = 0x7f10006e;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dark = 0x7f10006f;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dark_ActionBar = 0x7f100070;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dialog = 0x7f100071;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dialog_Alert = 0x7f100072;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Light = 0x7f100073;

    @StyleRes
    public static final int Base_ThemeOverlay_MaterialComponents_Dialog = 0x7f100074;

    @StyleRes
    public static final int Base_ThemeOverlay_MaterialComponents_Dialog_Alert = 0x7f100075;

    @StyleRes
    public static final int Base_ThemeOverlay_MaterialComponents_Dialog_Alert_Framework = 0x7f100076;

    @StyleRes
    public static final int Base_ThemeOverlay_MaterialComponents_Light_Dialog_Alert_Framework = 0x7f100077;

    @StyleRes
    public static final int Base_ThemeOverlay_MaterialComponents_MaterialAlertDialog = 0x7f100078;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents = 0x7f100079;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Bridge = 0x7f10007a;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Dialog = 0x7f10007b;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Dialog_Bridge = 0x7f10007c;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light = 0x7f10007d;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light_Bridge = 0x7f10007e;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light_DarkActionBar_Bridge = 0x7f10007f;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light_Dialog = 0x7f100080;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light_Dialog_Bridge = 0x7f100081;

    @StyleRes
    public static final int Base_V14_ThemeOverlay_MaterialComponents_Dialog = 0x7f100082;

    @StyleRes
    public static final int Base_V14_ThemeOverlay_MaterialComponents_Dialog_Alert = 0x7f100083;

    @StyleRes
    public static final int Base_V14_ThemeOverlay_MaterialComponents_MaterialAlertDialog = 0x7f100084;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat = 0x7f100085;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat_Dialog = 0x7f100086;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat_Light = 0x7f100087;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat_Light_Dialog = 0x7f100088;

    @StyleRes
    public static final int Base_V21_Theme_MaterialComponents = 0x7f100089;

    @StyleRes
    public static final int Base_V21_Theme_MaterialComponents_Dialog = 0x7f10008a;

    @StyleRes
    public static final int Base_V21_Theme_MaterialComponents_Light = 0x7f10008b;

    @StyleRes
    public static final int Base_V21_Theme_MaterialComponents_Light_Dialog = 0x7f10008c;

    @StyleRes
    public static final int Base_V21_ThemeOverlay_AppCompat_Dialog = 0x7f10008d;

    @StyleRes
    public static final int Base_V22_Theme_AppCompat = 0x7f10008e;

    @StyleRes
    public static final int Base_V22_Theme_AppCompat_Light = 0x7f10008f;

    @StyleRes
    public static final int Base_V23_Theme_AppCompat = 0x7f100090;

    @StyleRes
    public static final int Base_V23_Theme_AppCompat_Light = 0x7f100091;

    @StyleRes
    public static final int Base_V26_Theme_AppCompat = 0x7f100092;

    @StyleRes
    public static final int Base_V26_Theme_AppCompat_Light = 0x7f100093;

    @StyleRes
    public static final int Base_V26_Widget_AppCompat_Toolbar = 0x7f100094;

    @StyleRes
    public static final int Base_V28_Theme_AppCompat = 0x7f100095;

    @StyleRes
    public static final int Base_V28_Theme_AppCompat_Light = 0x7f100096;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat = 0x7f100097;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat_Dialog = 0x7f100098;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat_Light = 0x7f100099;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat_Light_Dialog = 0x7f10009a;

    @StyleRes
    public static final int Base_V7_ThemeOverlay_AppCompat_Dialog = 0x7f10009b;

    @StyleRes
    public static final int Base_V7_Widget_AppCompat_AutoCompleteTextView = 0x7f10009c;

    @StyleRes
    public static final int Base_V7_Widget_AppCompat_EditText = 0x7f10009d;

    @StyleRes
    public static final int Base_V7_Widget_AppCompat_Toolbar = 0x7f10009e;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar = 0x7f10009f;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_Solid = 0x7f1000a0;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_TabBar = 0x7f1000a1;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_TabText = 0x7f1000a2;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_TabView = 0x7f1000a3;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionButton = 0x7f1000a4;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionButton_CloseMode = 0x7f1000a5;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionButton_Overflow = 0x7f1000a6;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionMode = 0x7f1000a7;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActivityChooserView = 0x7f1000a8;

    @StyleRes
    public static final int Base_Widget_AppCompat_AutoCompleteTextView = 0x7f1000a9;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button = 0x7f1000aa;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Borderless = 0x7f1000ab;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Borderless_Colored = 0x7f1000ac;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_ButtonBar_AlertDialog = 0x7f1000ad;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Colored = 0x7f1000ae;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Small = 0x7f1000af;

    @StyleRes
    public static final int Base_Widget_AppCompat_ButtonBar = 0x7f1000b0;

    @StyleRes
    public static final int Base_Widget_AppCompat_ButtonBar_AlertDialog = 0x7f1000b1;

    @StyleRes
    public static final int Base_Widget_AppCompat_CompoundButton_CheckBox = 0x7f1000b2;

    @StyleRes
    public static final int Base_Widget_AppCompat_CompoundButton_RadioButton = 0x7f1000b3;

    @StyleRes
    public static final int Base_Widget_AppCompat_CompoundButton_Switch = 0x7f1000b4;

    @StyleRes
    public static final int Base_Widget_AppCompat_DrawerArrowToggle = 0x7f1000b5;

    @StyleRes
    public static final int Base_Widget_AppCompat_DrawerArrowToggle_Common = 0x7f1000b6;

    @StyleRes
    public static final int Base_Widget_AppCompat_DropDownItem_Spinner = 0x7f1000b7;

    @StyleRes
    public static final int Base_Widget_AppCompat_EditText = 0x7f1000b8;

    @StyleRes
    public static final int Base_Widget_AppCompat_ImageButton = 0x7f1000b9;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar = 0x7f1000ba;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_Solid = 0x7f1000bb;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabBar = 0x7f1000bc;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabText = 0x7f1000bd;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse = 0x7f1000be;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabView = 0x7f1000bf;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_PopupMenu = 0x7f1000c0;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_PopupMenu_Overflow = 0x7f1000c1;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListMenuView = 0x7f1000c2;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListPopupWindow = 0x7f1000c3;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListView = 0x7f1000c4;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListView_DropDown = 0x7f1000c5;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListView_Menu = 0x7f1000c6;

    @StyleRes
    public static final int Base_Widget_AppCompat_PopupMenu = 0x7f1000c7;

    @StyleRes
    public static final int Base_Widget_AppCompat_PopupMenu_Overflow = 0x7f1000c8;

    @StyleRes
    public static final int Base_Widget_AppCompat_PopupWindow = 0x7f1000c9;

    @StyleRes
    public static final int Base_Widget_AppCompat_ProgressBar = 0x7f1000ca;

    @StyleRes
    public static final int Base_Widget_AppCompat_ProgressBar_Horizontal = 0x7f1000cb;

    @StyleRes
    public static final int Base_Widget_AppCompat_RatingBar = 0x7f1000cc;

    @StyleRes
    public static final int Base_Widget_AppCompat_RatingBar_Indicator = 0x7f1000cd;

    @StyleRes
    public static final int Base_Widget_AppCompat_RatingBar_Small = 0x7f1000ce;

    @StyleRes
    public static final int Base_Widget_AppCompat_SearchView = 0x7f1000cf;

    @StyleRes
    public static final int Base_Widget_AppCompat_SearchView_ActionBar = 0x7f1000d0;

    @StyleRes
    public static final int Base_Widget_AppCompat_SeekBar = 0x7f1000d1;

    @StyleRes
    public static final int Base_Widget_AppCompat_SeekBar_Discrete = 0x7f1000d2;

    @StyleRes
    public static final int Base_Widget_AppCompat_Spinner = 0x7f1000d3;

    @StyleRes
    public static final int Base_Widget_AppCompat_Spinner_Underlined = 0x7f1000d4;

    @StyleRes
    public static final int Base_Widget_AppCompat_TextView = 0x7f1000d5;

    @StyleRes
    public static final int Base_Widget_AppCompat_TextView_SpinnerItem = 0x7f1000d6;

    @StyleRes
    public static final int Base_Widget_AppCompat_Toolbar = 0x7f1000d7;

    @StyleRes
    public static final int Base_Widget_AppCompat_Toolbar_Button_Navigation = 0x7f1000d8;

    @StyleRes
    public static final int Base_Widget_Design_TabLayout = 0x7f1000d9;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_AutoCompleteTextView = 0x7f1000da;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_CheckedTextView = 0x7f1000db;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_Chip = 0x7f1000dc;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_PopupMenu = 0x7f1000dd;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_PopupMenu_ContextMenu = 0x7f1000de;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_PopupMenu_ListPopupWindow = 0x7f1000df;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_PopupMenu_Overflow = 0x7f1000e0;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_Slider = 0x7f1000e1;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_TextInputEditText = 0x7f1000e2;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_TextInputLayout = 0x7f1000e3;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_TextView = 0x7f1000e4;

    @StyleRes
    public static final int ButtonTheam = 0x7f1000e5;

    @StyleRes
    public static final int CardView = 0x7f1000e6;

    @StyleRes
    public static final int CardView_Dark = 0x7f1000e7;

    @StyleRes
    public static final int CardView_Light = 0x7f1000e8;

    @StyleRes
    public static final int DialogTheme = 0x7f1000e9;

    @StyleRes
    public static final int EmptyTheme = 0x7f1000ea;

    @StyleRes
    public static final int IconTextButton = 0x7f1000eb;

    @StyleRes
    public static final int IconTextContainedButton = 0x7f1000ec;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents = 0x7f1000ed;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Body_Text = 0x7f1000ee;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Picker_Date_Calendar = 0x7f1000ef;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Picker_Date_Spinner = 0x7f1000f0;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Title_Icon = 0x7f1000f1;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Title_Icon_CenterStacked = 0x7f1000f2;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Title_Panel = 0x7f1000f3;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Title_Panel_CenterStacked = 0x7f1000f4;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Title_Text = 0x7f1000f5;

    @StyleRes
    public static final int MaterialAlertDialog_MaterialComponents_Title_Text_CenterStacked = 0x7f1000f6;

    @StyleRes
    public static final int MyActionBar = 0x7f1000f7;

    @StyleRes
    public static final int NormalTextView = 0x7f1000f8;

    @StyleRes
    public static final int OptRoundCardView = 0x7f1000f9;

    @StyleRes
    public static final int OptRoundCardView_Dark = 0x7f1000fa;

    @StyleRes
    public static final int OptRoundCardView_Light = 0x7f1000fb;

    @StyleRes
    public static final int OutlineIconTextButton = 0x7f1000fc;

    @StyleRes
    public static final int OutlineTextButton = 0x7f1000fd;

    @StyleRes
    public static final int Platform_AppCompat = 0x7f1000fe;

    @StyleRes
    public static final int Platform_AppCompat_Light = 0x7f1000ff;

    @StyleRes
    public static final int Platform_MaterialComponents = 0x7f100100;

    @StyleRes
    public static final int Platform_MaterialComponents_Dialog = 0x7f100101;

    @StyleRes
    public static final int Platform_MaterialComponents_Light = 0x7f100102;

    @StyleRes
    public static final int Platform_MaterialComponents_Light_Dialog = 0x7f100103;

    @StyleRes
    public static final int Platform_ThemeOverlay_AppCompat = 0x7f100104;

    @StyleRes
    public static final int Platform_ThemeOverlay_AppCompat_Dark = 0x7f100105;

    @StyleRes
    public static final int Platform_ThemeOverlay_AppCompat_Light = 0x7f100106;

    @StyleRes
    public static final int Platform_V21_AppCompat = 0x7f100107;

    @StyleRes
    public static final int Platform_V21_AppCompat_Light = 0x7f100108;

    @StyleRes
    public static final int Platform_V25_AppCompat = 0x7f100109;

    @StyleRes
    public static final int Platform_V25_AppCompat_Light = 0x7f10010a;

    @StyleRes
    public static final int Platform_Widget_AppCompat_Spinner = 0x7f10010b;

    @StyleRes
    public static final int RtlOverlay_DialogWindowTitle_AppCompat = 0x7f10010c;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_ActionBar_TitleItem = 0x7f10010d;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_DialogTitle_Icon = 0x7f10010e;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem = 0x7f10010f;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup = 0x7f100110;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut = 0x7f100111;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow = 0x7f100112;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_Text = 0x7f100113;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_Title = 0x7f100114;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown = 0x7f100115;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1 = 0x7f100116;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2 = 0x7f100117;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Query = 0x7f100118;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Text = 0x7f100119;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_SearchView_MagIcon = 0x7f10011a;

    @StyleRes
    public static final int RtlUnderlay_Widget_AppCompat_ActionButton = 0x7f10011b;

    @StyleRes
    public static final int RtlUnderlay_Widget_AppCompat_ActionButton_Overflow = 0x7f10011c;

    @StyleRes
    public static final int ShapeAppearance_MaterialComponents = 0x7f10011d;

    @StyleRes
    public static final int ShapeAppearance_MaterialComponents_LargeComponent = 0x7f10011e;

    @StyleRes
    public static final int ShapeAppearance_MaterialComponents_MediumComponent = 0x7f10011f;

    @StyleRes
    public static final int ShapeAppearance_MaterialComponents_SmallComponent = 0x7f100120;

    @StyleRes
    public static final int ShapeAppearance_MaterialComponents_Test = 0x7f100121;

    @StyleRes
    public static final int ShapeAppearance_MaterialComponents_Tooltip = 0x7f100122;

    @StyleRes
    public static final int ShapeAppearanceOverlay = 0x7f100123;

    @StyleRes
    public static final int ShapeAppearanceOverlay_BottomLeftDifferentCornerSize = 0x7f100124;

    @StyleRes
    public static final int ShapeAppearanceOverlay_BottomRightCut = 0x7f100125;

    @StyleRes
    public static final int ShapeAppearanceOverlay_Cut = 0x7f100126;

    @StyleRes
    public static final int ShapeAppearanceOverlay_DifferentCornerSize = 0x7f100127;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_BottomSheet = 0x7f100128;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_Chip = 0x7f100129;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_ExtendedFloatingActionButton = 0x7f10012a;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_FloatingActionButton = 0x7f10012b;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day = 0x7f10012c;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Window_Fullscreen = 0x7f10012d;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Year = 0x7f10012e;

    @StyleRes
    public static final int ShapeAppearanceOverlay_MaterialComponents_TextInputLayout_FilledBox = 0x7f10012f;

    @StyleRes
    public static final int ShapeAppearanceOverlay_TopLeftCut = 0x7f100130;

    @StyleRes
    public static final int ShapeAppearanceOverlay_TopRightDifferentCornerSize = 0x7f100131;

    @StyleRes
    public static final int Test_ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day = 0x7f100132;

    @StyleRes
    public static final int Test_Theme_MaterialComponents_MaterialCalendar = 0x7f100133;

    @StyleRes
    public static final int Test_Widget_MaterialComponents_MaterialCalendar = 0x7f100134;

    @StyleRes
    public static final int Test_Widget_MaterialComponents_MaterialCalendar_Day = 0x7f100135;

    @StyleRes
    public static final int Test_Widget_MaterialComponents_MaterialCalendar_Day_Selected = 0x7f100136;

    @StyleRes
    public static final int TestStyleWithLineHeight = 0x7f100137;

    @StyleRes
    public static final int TestStyleWithLineHeightAppearance = 0x7f100138;

    @StyleRes
    public static final int TestStyleWithThemeLineHeightAttribute = 0x7f100139;

    @StyleRes
    public static final int TestStyleWithoutLineHeight = 0x7f10013a;

    @StyleRes
    public static final int TestThemeWithLineHeight = 0x7f10013b;

    @StyleRes
    public static final int TestThemeWithLineHeightDisabled = 0x7f10013c;

    @StyleRes
    public static final int TextAppearance_AppCompat = 0x7f10013d;

    @StyleRes
    public static final int TextAppearance_AppCompat_Body1 = 0x7f10013e;

    @StyleRes
    public static final int TextAppearance_AppCompat_Body2 = 0x7f10013f;

    @StyleRes
    public static final int TextAppearance_AppCompat_Button = 0x7f100140;

    @StyleRes
    public static final int TextAppearance_AppCompat_Caption = 0x7f100141;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display1 = 0x7f100142;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display2 = 0x7f100143;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display3 = 0x7f100144;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display4 = 0x7f100145;

    @StyleRes
    public static final int TextAppearance_AppCompat_Headline = 0x7f100146;

    @StyleRes
    public static final int TextAppearance_AppCompat_Inverse = 0x7f100147;

    @StyleRes
    public static final int TextAppearance_AppCompat_Large = 0x7f100148;

    @StyleRes
    public static final int TextAppearance_AppCompat_Large_Inverse = 0x7f100149;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_SearchResult_Subtitle = 0x7f10014a;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_SearchResult_Title = 0x7f10014b;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_Widget_PopupMenu_Large = 0x7f10014c;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_Widget_PopupMenu_Small = 0x7f10014d;

    @StyleRes
    public static final int TextAppearance_AppCompat_Medium = 0x7f10014e;

    @StyleRes
    public static final int TextAppearance_AppCompat_Medium_Inverse = 0x7f10014f;

    @StyleRes
    public static final int TextAppearance_AppCompat_Menu = 0x7f100150;

    @StyleRes
    public static final int TextAppearance_AppCompat_SearchResult_Subtitle = 0x7f100151;

    @StyleRes
    public static final int TextAppearance_AppCompat_SearchResult_Title = 0x7f100152;

    @StyleRes
    public static final int TextAppearance_AppCompat_Small = 0x7f100153;

    @StyleRes
    public static final int TextAppearance_AppCompat_Small_Inverse = 0x7f100154;

    @StyleRes
    public static final int TextAppearance_AppCompat_Subhead = 0x7f100155;

    @StyleRes
    public static final int TextAppearance_AppCompat_Subhead_Inverse = 0x7f100156;

    @StyleRes
    public static final int TextAppearance_AppCompat_Title = 0x7f100157;

    @StyleRes
    public static final int TextAppearance_AppCompat_Title_Inverse = 0x7f100158;

    @StyleRes
    public static final int TextAppearance_AppCompat_Tooltip = 0x7f100159;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Menu = 0x7f10015a;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Subtitle = 0x7f10015b;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse = 0x7f10015c;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Title = 0x7f10015d;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse = 0x7f10015e;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Subtitle = 0x7f10015f;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse = 0x7f100160;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Title = 0x7f100161;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse = 0x7f100162;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button = 0x7f100163;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button_Borderless_Colored = 0x7f100164;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button_Colored = 0x7f100165;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button_Inverse = 0x7f100166;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_DropDownItem = 0x7f100167;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_PopupMenu_Header = 0x7f100168;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_PopupMenu_Large = 0x7f100169;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_PopupMenu_Small = 0x7f10016a;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Switch = 0x7f10016b;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_TextView_SpinnerItem = 0x7f10016c;

    @StyleRes
    public static final int TextAppearance_Compat_Notification = 0x7f10016d;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Info = 0x7f10016e;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Info_Media = 0x7f10016f;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Line2 = 0x7f100170;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Line2_Media = 0x7f100171;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Media = 0x7f100172;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Time = 0x7f100173;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Time_Media = 0x7f100174;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Title = 0x7f100175;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Title_Media = 0x7f100176;

    @StyleRes
    public static final int TextAppearance_Design_CollapsingToolbar_Expanded = 0x7f100177;

    @StyleRes
    public static final int TextAppearance_Design_Counter = 0x7f100178;

    @StyleRes
    public static final int TextAppearance_Design_Counter_Overflow = 0x7f100179;

    @StyleRes
    public static final int TextAppearance_Design_Error = 0x7f10017a;

    @StyleRes
    public static final int TextAppearance_Design_HelperText = 0x7f10017b;

    @StyleRes
    public static final int TextAppearance_Design_Hint = 0x7f10017c;

    @StyleRes
    public static final int TextAppearance_Design_Placeholder = 0x7f10017d;

    @StyleRes
    public static final int TextAppearance_Design_Prefix = 0x7f10017e;

    @StyleRes
    public static final int TextAppearance_Design_Snackbar_Message = 0x7f10017f;

    @StyleRes
    public static final int TextAppearance_Design_Suffix = 0x7f100180;

    @StyleRes
    public static final int TextAppearance_Design_Tab = 0x7f100181;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Badge = 0x7f100182;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Body1 = 0x7f100183;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Body2 = 0x7f100184;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Button = 0x7f100185;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Caption = 0x7f100186;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Chip = 0x7f100187;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline1 = 0x7f100188;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline2 = 0x7f100189;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline3 = 0x7f10018a;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline4 = 0x7f10018b;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline5 = 0x7f10018c;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline6 = 0x7f10018d;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Overline = 0x7f10018e;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Subtitle1 = 0x7f10018f;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Subtitle2 = 0x7f100190;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Tooltip = 0x7f100191;

    @StyleRes
    public static final int TextAppearance_Widget_AppCompat_ExpandedMenu_Item = 0x7f100192;

    @StyleRes
    public static final int TextAppearance_Widget_AppCompat_Toolbar_Subtitle = 0x7f100193;

    @StyleRes
    public static final int TextAppearance_Widget_AppCompat_Toolbar_Title = 0x7f100194;

    @StyleRes
    public static final int TextButtonButton = 0x7f100195;

    @StyleRes
    public static final int TextViewCommonTheam = 0x7f100196;

    @StyleRes
    public static final int Theme_AppCompat = 0x7f100197;

    @StyleRes
    public static final int Theme_AppCompat_CompactMenu = 0x7f100198;

    @StyleRes
    public static final int Theme_AppCompat_DayNight = 0x7f100199;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_DarkActionBar = 0x7f10019a;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_Dialog = 0x7f10019b;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_Dialog_Alert = 0x7f10019c;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_Dialog_MinWidth = 0x7f10019d;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_DialogWhenLarge = 0x7f10019e;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_NoActionBar = 0x7f10019f;

    @StyleRes
    public static final int Theme_AppCompat_Dialog = 0x7f1001a0;

    @StyleRes
    public static final int Theme_AppCompat_Dialog_Alert = 0x7f1001a1;

    @StyleRes
    public static final int Theme_AppCompat_Dialog_MinWidth = 0x7f1001a2;

    @StyleRes
    public static final int Theme_AppCompat_DialogWhenLarge = 0x7f1001a3;

    @StyleRes
    public static final int Theme_AppCompat_Light = 0x7f1001a4;

    @StyleRes
    public static final int Theme_AppCompat_Light_DarkActionBar = 0x7f1001a5;

    @StyleRes
    public static final int Theme_AppCompat_Light_Dialog = 0x7f1001a6;

    @StyleRes
    public static final int Theme_AppCompat_Light_Dialog_Alert = 0x7f1001a7;

    @StyleRes
    public static final int Theme_AppCompat_Light_Dialog_MinWidth = 0x7f1001a8;

    @StyleRes
    public static final int Theme_AppCompat_Light_DialogWhenLarge = 0x7f1001a9;

    @StyleRes
    public static final int Theme_AppCompat_Light_NoActionBar = 0x7f1001aa;

    @StyleRes
    public static final int Theme_AppCompat_NoActionBar = 0x7f1001ab;

    @StyleRes
    public static final int Theme_Design = 0x7f1001ac;

    @StyleRes
    public static final int Theme_Design_BottomSheetDialog = 0x7f1001ad;

    @StyleRes
    public static final int Theme_Design_Light = 0x7f1001ae;

    @StyleRes
    public static final int Theme_Design_Light_BottomSheetDialog = 0x7f1001af;

    @StyleRes
    public static final int Theme_Design_Light_NoActionBar = 0x7f1001b0;

    @StyleRes
    public static final int Theme_Design_NoActionBar = 0x7f1001b1;

    @StyleRes
    public static final int Theme_MaterialComponents = 0x7f1001b2;

    @StyleRes
    public static final int Theme_MaterialComponents_BottomSheetDialog = 0x7f1001b3;

    @StyleRes
    public static final int Theme_MaterialComponents_Bridge = 0x7f1001b4;

    @StyleRes
    public static final int Theme_MaterialComponents_CompactMenu = 0x7f1001b5;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight = 0x7f1001b6;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_BottomSheetDialog = 0x7f1001b7;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Bridge = 0x7f1001b8;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_DarkActionBar = 0x7f1001b9;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_DarkActionBar_Bridge = 0x7f1001ba;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog = 0x7f1001bb;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog_Alert = 0x7f1001bc;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog_Alert_Bridge = 0x7f1001bd;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog_Bridge = 0x7f1001be;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog_FixedSize = 0x7f1001bf;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog_FixedSize_Bridge = 0x7f1001c0;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog_MinWidth = 0x7f1001c1;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_Dialog_MinWidth_Bridge = 0x7f1001c2;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_DialogWhenLarge = 0x7f1001c3;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_NoActionBar = 0x7f1001c4;

    @StyleRes
    public static final int Theme_MaterialComponents_DayNight_NoActionBar_Bridge = 0x7f1001c5;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog = 0x7f1001c6;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_Alert = 0x7f1001c7;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_Alert_Bridge = 0x7f1001c8;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_Bridge = 0x7f1001c9;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_FixedSize = 0x7f1001ca;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_FixedSize_Bridge = 0x7f1001cb;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_MinWidth = 0x7f1001cc;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_MinWidth_Bridge = 0x7f1001cd;

    @StyleRes
    public static final int Theme_MaterialComponents_DialogWhenLarge = 0x7f1001ce;

    @StyleRes
    public static final int Theme_MaterialComponents_Light = 0x7f1001cf;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_BarSize = 0x7f1001d0;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_BottomSheetDialog = 0x7f1001d1;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Bridge = 0x7f1001d2;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_DarkActionBar = 0x7f1001d3;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_DarkActionBar_Bridge = 0x7f1001d4;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog = 0x7f1001d5;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_Alert = 0x7f1001d6;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_Alert_Bridge = 0x7f1001d7;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_Bridge = 0x7f1001d8;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_FixedSize = 0x7f1001d9;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_FixedSize_Bridge = 0x7f1001da;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_MinWidth = 0x7f1001db;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_MinWidth_Bridge = 0x7f1001dc;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_DialogWhenLarge = 0x7f1001dd;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_LargeTouch = 0x7f1001de;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_NoActionBar = 0x7f1001df;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_NoActionBar_Bridge = 0x7f1001e0;

    @StyleRes
    public static final int Theme_MaterialComponents_NoActionBar = 0x7f1001e1;

    @StyleRes
    public static final int Theme_MaterialComponents_NoActionBar_Bridge = 0x7f1001e2;

    @StyleRes
    public static final int ThemeOverlay_AppCompat = 0x7f1001e3;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_ActionBar = 0x7f1001e4;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dark = 0x7f1001e5;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dark_ActionBar = 0x7f1001e6;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_DayNight = 0x7f1001e7;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_DayNight_ActionBar = 0x7f1001e8;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dialog = 0x7f1001e9;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dialog_Alert = 0x7f1001ea;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Light = 0x7f1001eb;

    @StyleRes
    public static final int ThemeOverlay_Design_TextInputEditText = 0x7f1001ec;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents = 0x7f1001ed;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_ActionBar = 0x7f1001ee;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_ActionBar_Primary = 0x7f1001ef;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_ActionBar_Surface = 0x7f1001f0;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_AutoCompleteTextView = 0x7f1001f1;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox = 0x7f1001f2;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox_Dense = 0x7f1001f3;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox = 0x7f1001f4;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense = 0x7f1001f5;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_BottomAppBar_Primary = 0x7f1001f6;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_BottomAppBar_Surface = 0x7f1001f7;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_BottomSheetDialog = 0x7f1001f8;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dark = 0x7f1001f9;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dark_ActionBar = 0x7f1001fa;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_DayNight_BottomSheetDialog = 0x7f1001fb;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dialog = 0x7f1001fc;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dialog_Alert = 0x7f1001fd;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dialog_Alert_Framework = 0x7f1001fe;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Light = 0x7f1001ff;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Light_BottomSheetDialog = 0x7f100200;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Light_Dialog_Alert_Framework = 0x7f100201;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialAlertDialog = 0x7f100202;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered = 0x7f100203;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date = 0x7f100204;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Calendar = 0x7f100205;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text = 0x7f100206;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text_Day = 0x7f100207;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Spinner = 0x7f100208;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialCalendar = 0x7f100209;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen = 0x7f10020a;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText = 0x7f10020b;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox = 0x7f10020c;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox_Dense = 0x7f10020d;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox = 0x7f10020e;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox_Dense = 0x7f10020f;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Toolbar_Primary = 0x7f100210;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Toolbar_Surface = 0x7f100211;

    @StyleRes
    public static final int ThemeOverlayColorAccentRed = 0x7f100212;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar = 0x7f100213;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_Solid = 0x7f100214;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_TabBar = 0x7f100215;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_TabText = 0x7f100216;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_TabView = 0x7f100217;

    @StyleRes
    public static final int Widget_AppCompat_ActionButton = 0x7f100218;

    @StyleRes
    public static final int Widget_AppCompat_ActionButton_CloseMode = 0x7f100219;

    @StyleRes
    public static final int Widget_AppCompat_ActionButton_Overflow = 0x7f10021a;

    @StyleRes
    public static final int Widget_AppCompat_ActionMode = 0x7f10021b;

    @StyleRes
    public static final int Widget_AppCompat_ActivityChooserView = 0x7f10021c;

    @StyleRes
    public static final int Widget_AppCompat_AutoCompleteTextView = 0x7f10021d;

    @StyleRes
    public static final int Widget_AppCompat_Button = 0x7f10021e;

    @StyleRes
    public static final int Widget_AppCompat_Button_Borderless = 0x7f10021f;

    @StyleRes
    public static final int Widget_AppCompat_Button_Borderless_Colored = 0x7f100220;

    @StyleRes
    public static final int Widget_AppCompat_Button_ButtonBar_AlertDialog = 0x7f100221;

    @StyleRes
    public static final int Widget_AppCompat_Button_Colored = 0x7f100222;

    @StyleRes
    public static final int Widget_AppCompat_Button_Small = 0x7f100223;

    @StyleRes
    public static final int Widget_AppCompat_ButtonBar = 0x7f100224;

    @StyleRes
    public static final int Widget_AppCompat_ButtonBar_AlertDialog = 0x7f100225;

    @StyleRes
    public static final int Widget_AppCompat_CompoundButton_CheckBox = 0x7f100226;

    @StyleRes
    public static final int Widget_AppCompat_CompoundButton_RadioButton = 0x7f100227;

    @StyleRes
    public static final int Widget_AppCompat_CompoundButton_Switch = 0x7f100228;

    @StyleRes
    public static final int Widget_AppCompat_DrawerArrowToggle = 0x7f100229;

    @StyleRes
    public static final int Widget_AppCompat_DropDownItem_Spinner = 0x7f10022a;

    @StyleRes
    public static final int Widget_AppCompat_EditText = 0x7f10022b;

    @StyleRes
    public static final int Widget_AppCompat_ImageButton = 0x7f10022c;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar = 0x7f10022d;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_Solid = 0x7f10022e;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_Solid_Inverse = 0x7f10022f;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabBar = 0x7f100230;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabBar_Inverse = 0x7f100231;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabText = 0x7f100232;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabText_Inverse = 0x7f100233;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabView = 0x7f100234;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabView_Inverse = 0x7f100235;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionButton = 0x7f100236;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionButton_CloseMode = 0x7f100237;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionButton_Overflow = 0x7f100238;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionMode_Inverse = 0x7f100239;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActivityChooserView = 0x7f10023a;

    @StyleRes
    public static final int Widget_AppCompat_Light_AutoCompleteTextView = 0x7f10023b;

    @StyleRes
    public static final int Widget_AppCompat_Light_DropDownItem_Spinner = 0x7f10023c;

    @StyleRes
    public static final int Widget_AppCompat_Light_ListPopupWindow = 0x7f10023d;

    @StyleRes
    public static final int Widget_AppCompat_Light_ListView_DropDown = 0x7f10023e;

    @StyleRes
    public static final int Widget_AppCompat_Light_PopupMenu = 0x7f10023f;

    @StyleRes
    public static final int Widget_AppCompat_Light_PopupMenu_Overflow = 0x7f100240;

    @StyleRes
    public static final int Widget_AppCompat_Light_SearchView = 0x7f100241;

    @StyleRes
    public static final int Widget_AppCompat_Light_Spinner_DropDown_ActionBar = 0x7f100242;

    @StyleRes
    public static final int Widget_AppCompat_ListMenuView = 0x7f100243;

    @StyleRes
    public static final int Widget_AppCompat_ListPopupWindow = 0x7f100244;

    @StyleRes
    public static final int Widget_AppCompat_ListView = 0x7f100245;

    @StyleRes
    public static final int Widget_AppCompat_ListView_DropDown = 0x7f100246;

    @StyleRes
    public static final int Widget_AppCompat_ListView_Menu = 0x7f100247;

    @StyleRes
    public static final int Widget_AppCompat_PopupMenu = 0x7f100248;

    @StyleRes
    public static final int Widget_AppCompat_PopupMenu_Overflow = 0x7f100249;

    @StyleRes
    public static final int Widget_AppCompat_PopupWindow = 0x7f10024a;

    @StyleRes
    public static final int Widget_AppCompat_ProgressBar = 0x7f10024b;

    @StyleRes
    public static final int Widget_AppCompat_ProgressBar_Horizontal = 0x7f10024c;

    @StyleRes
    public static final int Widget_AppCompat_RatingBar = 0x7f10024d;

    @StyleRes
    public static final int Widget_AppCompat_RatingBar_Indicator = 0x7f10024e;

    @StyleRes
    public static final int Widget_AppCompat_RatingBar_Small = 0x7f10024f;

    @StyleRes
    public static final int Widget_AppCompat_SearchView = 0x7f100250;

    @StyleRes
    public static final int Widget_AppCompat_SearchView_ActionBar = 0x7f100251;

    @StyleRes
    public static final int Widget_AppCompat_SeekBar = 0x7f100252;

    @StyleRes
    public static final int Widget_AppCompat_SeekBar_Discrete = 0x7f100253;

    @StyleRes
    public static final int Widget_AppCompat_Spinner = 0x7f100254;

    @StyleRes
    public static final int Widget_AppCompat_Spinner_DropDown = 0x7f100255;

    @StyleRes
    public static final int Widget_AppCompat_Spinner_DropDown_ActionBar = 0x7f100256;

    @StyleRes
    public static final int Widget_AppCompat_Spinner_Underlined = 0x7f100257;

    @StyleRes
    public static final int Widget_AppCompat_TextView = 0x7f100258;

    @StyleRes
    public static final int Widget_AppCompat_TextView_SpinnerItem = 0x7f100259;

    @StyleRes
    public static final int Widget_AppCompat_Toolbar = 0x7f10025a;

    @StyleRes
    public static final int Widget_AppCompat_Toolbar_Button_Navigation = 0x7f10025b;

    @StyleRes
    public static final int Widget_Compat_NotificationActionContainer = 0x7f10025c;

    @StyleRes
    public static final int Widget_Compat_NotificationActionText = 0x7f10025d;

    @StyleRes
    public static final int Widget_Design_AppBarLayout = 0x7f10025e;

    @StyleRes
    public static final int Widget_Design_BottomNavigationView = 0x7f10025f;

    @StyleRes
    public static final int Widget_Design_BottomSheet_Modal = 0x7f100260;

    @StyleRes
    public static final int Widget_Design_CollapsingToolbar = 0x7f100261;

    @StyleRes
    public static final int Widget_Design_FloatingActionButton = 0x7f100262;

    @StyleRes
    public static final int Widget_Design_NavigationView = 0x7f100263;

    @StyleRes
    public static final int Widget_Design_ScrimInsetsFrameLayout = 0x7f100264;

    @StyleRes
    public static final int Widget_Design_Snackbar = 0x7f100265;

    @StyleRes
    public static final int Widget_Design_TabLayout = 0x7f100266;

    @StyleRes
    public static final int Widget_Design_TextInputEditText = 0x7f100267;

    @StyleRes
    public static final int Widget_Design_TextInputLayout = 0x7f100268;

    @StyleRes
    public static final int Widget_MaterialComponents_ActionBar_Primary = 0x7f100269;

    @StyleRes
    public static final int Widget_MaterialComponents_ActionBar_PrimarySurface = 0x7f10026a;

    @StyleRes
    public static final int Widget_MaterialComponents_ActionBar_Solid = 0x7f10026b;

    @StyleRes
    public static final int Widget_MaterialComponents_ActionBar_Surface = 0x7f10026c;

    @StyleRes
    public static final int Widget_MaterialComponents_AppBarLayout_Primary = 0x7f10026d;

    @StyleRes
    public static final int Widget_MaterialComponents_AppBarLayout_PrimarySurface = 0x7f10026e;

    @StyleRes
    public static final int Widget_MaterialComponents_AppBarLayout_Surface = 0x7f10026f;

    @StyleRes
    public static final int Widget_MaterialComponents_AutoCompleteTextView_FilledBox = 0x7f100270;

    @StyleRes
    public static final int Widget_MaterialComponents_AutoCompleteTextView_FilledBox_Dense = 0x7f100271;

    @StyleRes
    public static final int Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox = 0x7f100272;

    @StyleRes
    public static final int Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense = 0x7f100273;

    @StyleRes
    public static final int Widget_MaterialComponents_Badge = 0x7f100274;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomAppBar = 0x7f100275;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomAppBar_Colored = 0x7f100276;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomAppBar_PrimarySurface = 0x7f100277;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomNavigationView = 0x7f100278;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomNavigationView_Colored = 0x7f100279;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomNavigationView_PrimarySurface = 0x7f10027a;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomSheet = 0x7f10027b;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomSheet_Modal = 0x7f10027c;

    @StyleRes
    public static final int Widget_MaterialComponents_Button = 0x7f10027d;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_Icon = 0x7f10027e;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_OutlinedButton = 0x7f10027f;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_OutlinedButton_Icon = 0x7f100280;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton = 0x7f100281;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Dialog = 0x7f100282;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Dialog_Flush = 0x7f100283;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Dialog_Icon = 0x7f100284;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Icon = 0x7f100285;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Snackbar = 0x7f100286;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_UnelevatedButton = 0x7f100287;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_UnelevatedButton_Icon = 0x7f100288;

    @StyleRes
    public static final int Widget_MaterialComponents_CardView = 0x7f100289;

    @StyleRes
    public static final int Widget_MaterialComponents_CheckedTextView = 0x7f10028a;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Action = 0x7f10028b;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Choice = 0x7f10028c;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Entry = 0x7f10028d;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Filter = 0x7f10028e;

    @StyleRes
    public static final int Widget_MaterialComponents_ChipGroup = 0x7f10028f;

    @StyleRes
    public static final int Widget_MaterialComponents_CompoundButton_CheckBox = 0x7f100290;

    @StyleRes
    public static final int Widget_MaterialComponents_CompoundButton_RadioButton = 0x7f100291;

    @StyleRes
    public static final int Widget_MaterialComponents_CompoundButton_Switch = 0x7f100292;

    @StyleRes
    public static final int Widget_MaterialComponents_ExtendedFloatingActionButton = 0x7f100293;

    @StyleRes
    public static final int Widget_MaterialComponents_ExtendedFloatingActionButton_Icon = 0x7f100294;

    @StyleRes
    public static final int Widget_MaterialComponents_FloatingActionButton = 0x7f100295;

    @StyleRes
    public static final int Widget_MaterialComponents_Light_ActionBar_Solid = 0x7f100296;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialButtonToggleGroup = 0x7f100297;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar = 0x7f100298;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Day = 0x7f100299;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Day_Invalid = 0x7f10029a;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Day_Selected = 0x7f10029b;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Day_Today = 0x7f10029c;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_DayTextView = 0x7f10029d;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Fullscreen = 0x7f10029e;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_HeaderConfirmButton = 0x7f10029f;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_HeaderDivider = 0x7f1002a0;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_HeaderLayout = 0x7f1002a1;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_HeaderSelection = 0x7f1002a2;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_HeaderSelection_Fullscreen = 0x7f1002a3;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_HeaderTitle = 0x7f1002a4;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_HeaderToggleButton = 0x7f1002a5;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Item = 0x7f1002a6;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Year = 0x7f1002a7;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Year_Selected = 0x7f1002a8;

    @StyleRes
    public static final int Widget_MaterialComponents_MaterialCalendar_Year_Today = 0x7f1002a9;

    @StyleRes
    public static final int Widget_MaterialComponents_NavigationView = 0x7f1002aa;

    @StyleRes
    public static final int Widget_MaterialComponents_PopupMenu = 0x7f1002ab;

    @StyleRes
    public static final int Widget_MaterialComponents_PopupMenu_ContextMenu = 0x7f1002ac;

    @StyleRes
    public static final int Widget_MaterialComponents_PopupMenu_ListPopupWindow = 0x7f1002ad;

    @StyleRes
    public static final int Widget_MaterialComponents_PopupMenu_Overflow = 0x7f1002ae;

    @StyleRes
    public static final int Widget_MaterialComponents_ShapeableImageView = 0x7f1002af;

    @StyleRes
    public static final int Widget_MaterialComponents_Slider = 0x7f1002b0;

    @StyleRes
    public static final int Widget_MaterialComponents_Snackbar = 0x7f1002b1;

    @StyleRes
    public static final int Widget_MaterialComponents_Snackbar_FullWidth = 0x7f1002b2;

    @StyleRes
    public static final int Widget_MaterialComponents_Snackbar_TextView = 0x7f1002b3;

    @StyleRes
    public static final int Widget_MaterialComponents_TabLayout = 0x7f1002b4;

    @StyleRes
    public static final int Widget_MaterialComponents_TabLayout_Colored = 0x7f1002b5;

    @StyleRes
    public static final int Widget_MaterialComponents_TabLayout_PrimarySurface = 0x7f1002b6;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_FilledBox = 0x7f1002b7;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_FilledBox_Dense = 0x7f1002b8;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_OutlinedBox = 0x7f1002b9;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_OutlinedBox_Dense = 0x7f1002ba;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_FilledBox = 0x7f1002bb;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_FilledBox_Dense = 0x7f1002bc;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_FilledBox_Dense_ExposedDropdownMenu = 0x7f1002bd;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_FilledBox_ExposedDropdownMenu = 0x7f1002be;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_OutlinedBox = 0x7f1002bf;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense = 0x7f1002c0;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense_ExposedDropdownMenu = 0x7f1002c1;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_OutlinedBox_ExposedDropdownMenu = 0x7f1002c2;

    @StyleRes
    public static final int Widget_MaterialComponents_TextView = 0x7f1002c3;

    @StyleRes
    public static final int Widget_MaterialComponents_Toolbar = 0x7f1002c4;

    @StyleRes
    public static final int Widget_MaterialComponents_Toolbar_Primary = 0x7f1002c5;

    @StyleRes
    public static final int Widget_MaterialComponents_Toolbar_PrimarySurface = 0x7f1002c6;

    @StyleRes
    public static final int Widget_MaterialComponents_Toolbar_Surface = 0x7f1002c7;

    @StyleRes
    public static final int Widget_MaterialComponents_Tooltip = 0x7f1002c8;

    @StyleRes
    public static final int Widget_Support_CoordinatorLayout = 0x7f1002c9;

    @StyleRes
    public static final int common_style = 0x7f1002ca;

    @StyleRes
    public static final int toolbarTheme = 0x7f1002cb;

    @StyleRes
    public static final int tv_common_style = 0x7f1002cc;

    @StyleRes
    public static final int tv_style = 0x7f1002cd;
  }

  public static final class styleable {
    @StyleableRes
    public static final int ActionBar_background = 0;

    @StyleableRes
    public static final int ActionBar_backgroundSplit = 1;

    @StyleableRes
    public static final int ActionBar_backgroundStacked = 2;

    @StyleableRes
    public static final int ActionBar_contentInsetEnd = 3;

    @StyleableRes
    public static final int ActionBar_contentInsetEndWithActions = 4;

    @StyleableRes
    public static final int ActionBar_contentInsetLeft = 5;

    @StyleableRes
    public static final int ActionBar_contentInsetRight = 6;

    @StyleableRes
    public static final int ActionBar_contentInsetStart = 7;

    @StyleableRes
    public static final int ActionBar_contentInsetStartWithNavigation = 8;

    @StyleableRes
    public static final int ActionBar_customNavigationLayout = 9;

    @StyleableRes
    public static final int ActionBar_displayOptions = 10;

    @StyleableRes
    public static final int ActionBar_divider = 11;

    @StyleableRes
    public static final int ActionBar_elevation = 12;

    @StyleableRes
    public static final int ActionBar_height = 13;

    @StyleableRes
    public static final int ActionBar_hideOnContentScroll = 14;

    @StyleableRes
    public static final int ActionBar_homeAsUpIndicator = 15;

    @StyleableRes
    public static final int ActionBar_homeLayout = 16;

    @StyleableRes
    public static final int ActionBar_icon = 17;

    @StyleableRes
    public static final int ActionBar_indeterminateProgressStyle = 18;

    @StyleableRes
    public static final int ActionBar_itemPadding = 19;

    @StyleableRes
    public static final int ActionBar_logo = 20;

    @StyleableRes
    public static final int ActionBar_navigationMode = 21;

    @StyleableRes
    public static final int ActionBar_popupTheme = 22;

    @StyleableRes
    public static final int ActionBar_progressBarPadding = 23;

    @StyleableRes
    public static final int ActionBar_progressBarStyle = 24;

    @StyleableRes
    public static final int ActionBar_subtitle = 25;

    @StyleableRes
    public static final int ActionBar_subtitleTextStyle = 26;

    @StyleableRes
    public static final int ActionBar_title = 27;

    @StyleableRes
    public static final int ActionBar_titleTextStyle = 28;

    @StyleableRes
    public static final int ActionBarLayout_android_layout_gravity = 0;

    @StyleableRes
    public static final int ActionMenuItemView_android_minWidth = 0;

    @StyleableRes
    public static final int ActionMode_background = 0;

    @StyleableRes
    public static final int ActionMode_backgroundSplit = 1;

    @StyleableRes
    public static final int ActionMode_closeItemLayout = 2;

    @StyleableRes
    public static final int ActionMode_height = 3;

    @StyleableRes
    public static final int ActionMode_subtitleTextStyle = 4;

    @StyleableRes
    public static final int ActionMode_titleTextStyle = 5;

    @StyleableRes
    public static final int ActivityChooserView_expandActivityOverflowButtonDrawable = 0;

    @StyleableRes
    public static final int ActivityChooserView_initialActivityCount = 1;

    @StyleableRes
    public static final int ActivityNavigator_android_name = 0;

    @StyleableRes
    public static final int ActivityNavigator_action = 1;

    @StyleableRes
    public static final int ActivityNavigator_data = 2;

    @StyleableRes
    public static final int ActivityNavigator_dataPattern = 3;

    @StyleableRes
    public static final int ActivityNavigator_targetPackage = 4;

    @StyleableRes
    public static final int AlertDialog_android_layout = 0;

    @StyleableRes
    public static final int AlertDialog_buttonIconDimen = 1;

    @StyleableRes
    public static final int AlertDialog_buttonPanelSideLayout = 2;

    @StyleableRes
    public static final int AlertDialog_listItemLayout = 3;

    @StyleableRes
    public static final int AlertDialog_listLayout = 4;

    @StyleableRes
    public static final int AlertDialog_multiChoiceItemLayout = 5;

    @StyleableRes
    public static final int AlertDialog_showTitle = 6;

    @StyleableRes
    public static final int AlertDialog_singleChoiceItemLayout = 7;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_dither = 0;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_visible = 1;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_variablePadding = 2;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_constantSize = 3;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_enterFadeDuration = 4;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_exitFadeDuration = 5;

    @StyleableRes
    public static final int AnimatedStateListDrawableItem_android_id = 0;

    @StyleableRes
    public static final int AnimatedStateListDrawableItem_android_drawable = 1;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_drawable = 0;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_toId = 1;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_fromId = 2;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_reversible = 3;

    @StyleableRes
    public static final int AppBarLayout_android_background = 0;

    @StyleableRes
    public static final int AppBarLayout_android_touchscreenBlocksFocus = 1;

    @StyleableRes
    public static final int AppBarLayout_android_keyboardNavigationCluster = 2;

    @StyleableRes
    public static final int AppBarLayout_elevation = 3;

    @StyleableRes
    public static final int AppBarLayout_expanded = 4;

    @StyleableRes
    public static final int AppBarLayout_liftOnScroll = 5;

    @StyleableRes
    public static final int AppBarLayout_liftOnScrollTargetViewId = 6;

    @StyleableRes
    public static final int AppBarLayout_statusBarForeground = 7;

    @StyleableRes
    public static final int AppBarLayoutStates_state_collapsed = 0;

    @StyleableRes
    public static final int AppBarLayoutStates_state_collapsible = 1;

    @StyleableRes
    public static final int AppBarLayoutStates_state_liftable = 2;

    @StyleableRes
    public static final int AppBarLayoutStates_state_lifted = 3;

    @StyleableRes
    public static final int AppBarLayout_Layout_layout_scrollFlags = 0;

    @StyleableRes
    public static final int AppBarLayout_Layout_layout_scrollInterpolator = 1;

    @StyleableRes
    public static final int AppCompatImageView_android_src = 0;

    @StyleableRes
    public static final int AppCompatImageView_srcCompat = 1;

    @StyleableRes
    public static final int AppCompatImageView_tint = 2;

    @StyleableRes
    public static final int AppCompatImageView_tintMode = 3;

    @StyleableRes
    public static final int AppCompatSeekBar_android_thumb = 0;

    @StyleableRes
    public static final int AppCompatSeekBar_tickMark = 1;

    @StyleableRes
    public static final int AppCompatSeekBar_tickMarkTint = 2;

    @StyleableRes
    public static final int AppCompatSeekBar_tickMarkTintMode = 3;

    @StyleableRes
    public static final int AppCompatTextHelper_android_textAppearance = 0;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableTop = 1;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableBottom = 2;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableLeft = 3;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableRight = 4;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableStart = 5;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableEnd = 6;

    @StyleableRes
    public static final int AppCompatTextView_android_textAppearance = 0;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeMaxTextSize = 1;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeMinTextSize = 2;

    @StyleableRes
    public static final int AppCompatTextView_autoSizePresetSizes = 3;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeStepGranularity = 4;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeTextType = 5;

    @StyleableRes
    public static final int AppCompatTextView_drawableBottomCompat = 6;

    @StyleableRes
    public static final int AppCompatTextView_drawableEndCompat = 7;

    @StyleableRes
    public static final int AppCompatTextView_drawableLeftCompat = 8;

    @StyleableRes
    public static final int AppCompatTextView_drawableRightCompat = 9;

    @StyleableRes
    public static final int AppCompatTextView_drawableStartCompat = 10;

    @StyleableRes
    public static final int AppCompatTextView_drawableTint = 11;

    @StyleableRes
    public static final int AppCompatTextView_drawableTintMode = 12;

    @StyleableRes
    public static final int AppCompatTextView_drawableTopCompat = 13;

    @StyleableRes
    public static final int AppCompatTextView_firstBaselineToTopHeight = 14;

    @StyleableRes
    public static final int AppCompatTextView_fontFamily = 15;

    @StyleableRes
    public static final int AppCompatTextView_fontVariationSettings = 16;

    @StyleableRes
    public static final int AppCompatTextView_lastBaselineToBottomHeight = 17;

    @StyleableRes
    public static final int AppCompatTextView_lineHeight = 18;

    @StyleableRes
    public static final int AppCompatTextView_textAllCaps = 19;

    @StyleableRes
    public static final int AppCompatTextView_textLocale = 20;

    @StyleableRes
    public static final int AppCompatTheme_android_windowIsFloating = 0;

    @StyleableRes
    public static final int AppCompatTheme_android_windowAnimationStyle = 1;

    @StyleableRes
    public static final int AppCompatTheme_actionBarDivider = 2;

    @StyleableRes
    public static final int AppCompatTheme_actionBarItemBackground = 3;

    @StyleableRes
    public static final int AppCompatTheme_actionBarPopupTheme = 4;

    @StyleableRes
    public static final int AppCompatTheme_actionBarSize = 5;

    @StyleableRes
    public static final int AppCompatTheme_actionBarSplitStyle = 6;

    @StyleableRes
    public static final int AppCompatTheme_actionBarStyle = 7;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTabBarStyle = 8;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTabStyle = 9;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTabTextStyle = 10;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTheme = 11;

    @StyleableRes
    public static final int AppCompatTheme_actionBarWidgetTheme = 12;

    @StyleableRes
    public static final int AppCompatTheme_actionButtonStyle = 13;

    @StyleableRes
    public static final int AppCompatTheme_actionDropDownStyle = 14;

    @StyleableRes
    public static final int AppCompatTheme_actionMenuTextAppearance = 15;

    @StyleableRes
    public static final int AppCompatTheme_actionMenuTextColor = 16;

    @StyleableRes
    public static final int AppCompatTheme_actionModeBackground = 17;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCloseButtonStyle = 18;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCloseDrawable = 19;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCopyDrawable = 20;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCutDrawable = 21;

    @StyleableRes
    public static final int AppCompatTheme_actionModeFindDrawable = 22;

    @StyleableRes
    public static final int AppCompatTheme_actionModePasteDrawable = 23;

    @StyleableRes
    public static final int AppCompatTheme_actionModePopupWindowStyle = 24;

    @StyleableRes
    public static final int AppCompatTheme_actionModeSelectAllDrawable = 25;

    @StyleableRes
    public static final int AppCompatTheme_actionModeShareDrawable = 26;

    @StyleableRes
    public static final int AppCompatTheme_actionModeSplitBackground = 27;

    @StyleableRes
    public static final int AppCompatTheme_actionModeStyle = 28;

    @StyleableRes
    public static final int AppCompatTheme_actionModeWebSearchDrawable = 29;

    @StyleableRes
    public static final int AppCompatTheme_actionOverflowButtonStyle = 30;

    @StyleableRes
    public static final int AppCompatTheme_actionOverflowMenuStyle = 31;

    @StyleableRes
    public static final int AppCompatTheme_activityChooserViewStyle = 32;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogButtonGroupStyle = 33;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogCenterButtons = 34;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogStyle = 35;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogTheme = 36;

    @StyleableRes
    public static final int AppCompatTheme_autoCompleteTextViewStyle = 37;

    @StyleableRes
    public static final int AppCompatTheme_borderlessButtonStyle = 38;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarButtonStyle = 39;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarNegativeButtonStyle = 40;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarNeutralButtonStyle = 41;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarPositiveButtonStyle = 42;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarStyle = 43;

    @StyleableRes
    public static final int AppCompatTheme_buttonStyle = 44;

    @StyleableRes
    public static final int AppCompatTheme_buttonStyleSmall = 45;

    @StyleableRes
    public static final int AppCompatTheme_checkboxStyle = 46;

    @StyleableRes
    public static final int AppCompatTheme_checkedTextViewStyle = 47;

    @StyleableRes
    public static final int AppCompatTheme_colorAccent = 48;

    @StyleableRes
    public static final int AppCompatTheme_colorBackgroundFloating = 49;

    @StyleableRes
    public static final int AppCompatTheme_colorButtonNormal = 50;

    @StyleableRes
    public static final int AppCompatTheme_colorControlActivated = 51;

    @StyleableRes
    public static final int AppCompatTheme_colorControlHighlight = 52;

    @StyleableRes
    public static final int AppCompatTheme_colorControlNormal = 53;

    @StyleableRes
    public static final int AppCompatTheme_colorError = 54;

    @StyleableRes
    public static final int AppCompatTheme_colorPrimary = 55;

    @StyleableRes
    public static final int AppCompatTheme_colorPrimaryDark = 56;

    @StyleableRes
    public static final int AppCompatTheme_colorSwitchThumbNormal = 57;

    @StyleableRes
    public static final int AppCompatTheme_controlBackground = 58;

    @StyleableRes
    public static final int AppCompatTheme_dialogCornerRadius = 59;

    @StyleableRes
    public static final int AppCompatTheme_dialogPreferredPadding = 60;

    @StyleableRes
    public static final int AppCompatTheme_dialogTheme = 61;

    @StyleableRes
    public static final int AppCompatTheme_dividerHorizontal = 62;

    @StyleableRes
    public static final int AppCompatTheme_dividerVertical = 63;

    @StyleableRes
    public static final int AppCompatTheme_dropDownListViewStyle = 64;

    @StyleableRes
    public static final int AppCompatTheme_dropdownListPreferredItemHeight = 65;

    @StyleableRes
    public static final int AppCompatTheme_editTextBackground = 66;

    @StyleableRes
    public static final int AppCompatTheme_editTextColor = 67;

    @StyleableRes
    public static final int AppCompatTheme_editTextStyle = 68;

    @StyleableRes
    public static final int AppCompatTheme_homeAsUpIndicator = 69;

    @StyleableRes
    public static final int AppCompatTheme_imageButtonStyle = 70;

    @StyleableRes
    public static final int AppCompatTheme_listChoiceBackgroundIndicator = 71;

    @StyleableRes
    public static final int AppCompatTheme_listChoiceIndicatorMultipleAnimated = 72;

    @StyleableRes
    public static final int AppCompatTheme_listChoiceIndicatorSingleAnimated = 73;

    @StyleableRes
    public static final int AppCompatTheme_listDividerAlertDialog = 74;

    @StyleableRes
    public static final int AppCompatTheme_listMenuViewStyle = 75;

    @StyleableRes
    public static final int AppCompatTheme_listPopupWindowStyle = 76;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemHeight = 77;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemHeightLarge = 78;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemHeightSmall = 79;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingEnd = 80;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingLeft = 81;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingRight = 82;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingStart = 83;

    @StyleableRes
    public static final int AppCompatTheme_panelBackground = 84;

    @StyleableRes
    public static final int AppCompatTheme_panelMenuListTheme = 85;

    @StyleableRes
    public static final int AppCompatTheme_panelMenuListWidth = 86;

    @StyleableRes
    public static final int AppCompatTheme_popupMenuStyle = 87;

    @StyleableRes
    public static final int AppCompatTheme_popupWindowStyle = 88;

    @StyleableRes
    public static final int AppCompatTheme_radioButtonStyle = 89;

    @StyleableRes
    public static final int AppCompatTheme_ratingBarStyle = 90;

    @StyleableRes
    public static final int AppCompatTheme_ratingBarStyleIndicator = 91;

    @StyleableRes
    public static final int AppCompatTheme_ratingBarStyleSmall = 92;

    @StyleableRes
    public static final int AppCompatTheme_searchViewStyle = 93;

    @StyleableRes
    public static final int AppCompatTheme_seekBarStyle = 94;

    @StyleableRes
    public static final int AppCompatTheme_selectableItemBackground = 95;

    @StyleableRes
    public static final int AppCompatTheme_selectableItemBackgroundBorderless = 96;

    @StyleableRes
    public static final int AppCompatTheme_spinnerDropDownItemStyle = 97;

    @StyleableRes
    public static final int AppCompatTheme_spinnerStyle = 98;

    @StyleableRes
    public static final int AppCompatTheme_switchStyle = 99;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceLargePopupMenu = 100;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceListItem = 101;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceListItemSecondary = 102;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceListItemSmall = 103;

    @StyleableRes
    public static final int AppCompatTheme_textAppearancePopupMenuHeader = 104;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceSearchResultSubtitle = 105;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceSearchResultTitle = 106;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceSmallPopupMenu = 107;

    @StyleableRes
    public static final int AppCompatTheme_textColorAlertDialogListItem = 108;

    @StyleableRes
    public static final int AppCompatTheme_textColorSearchUrl = 109;

    @StyleableRes
    public static final int AppCompatTheme_toolbarNavigationButtonStyle = 110;

    @StyleableRes
    public static final int AppCompatTheme_toolbarStyle = 111;

    @StyleableRes
    public static final int AppCompatTheme_tooltipForegroundColor = 112;

    @StyleableRes
    public static final int AppCompatTheme_tooltipFrameBackground = 113;

    @StyleableRes
    public static final int AppCompatTheme_viewInflaterClass = 114;

    @StyleableRes
    public static final int AppCompatTheme_windowActionBar = 115;

    @StyleableRes
    public static final int AppCompatTheme_windowActionBarOverlay = 116;

    @StyleableRes
    public static final int AppCompatTheme_windowActionModeOverlay = 117;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedHeightMajor = 118;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedHeightMinor = 119;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedWidthMajor = 120;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedWidthMinor = 121;

    @StyleableRes
    public static final int AppCompatTheme_windowMinWidthMajor = 122;

    @StyleableRes
    public static final int AppCompatTheme_windowMinWidthMinor = 123;

    @StyleableRes
    public static final int AppCompatTheme_windowNoTitle = 124;

    @StyleableRes
    public static final int Badge_backgroundColor = 0;

    @StyleableRes
    public static final int Badge_badgeGravity = 1;

    @StyleableRes
    public static final int Badge_badgeTextColor = 2;

    @StyleableRes
    public static final int Badge_horizontalOffset = 3;

    @StyleableRes
    public static final int Badge_maxCharacterCount = 4;

    @StyleableRes
    public static final int Badge_number = 5;

    @StyleableRes
    public static final int Badge_verticalOffset = 6;

    @StyleableRes
    public static final int BottomAppBar_backgroundTint = 0;

    @StyleableRes
    public static final int BottomAppBar_elevation = 1;

    @StyleableRes
    public static final int BottomAppBar_fabAlignmentMode = 2;

    @StyleableRes
    public static final int BottomAppBar_fabAnimationMode = 3;

    @StyleableRes
    public static final int BottomAppBar_fabCradleMargin = 4;

    @StyleableRes
    public static final int BottomAppBar_fabCradleRoundedCornerRadius = 5;

    @StyleableRes
    public static final int BottomAppBar_fabCradleVerticalOffset = 6;

    @StyleableRes
    public static final int BottomAppBar_hideOnScroll = 7;

    @StyleableRes
    public static final int BottomAppBar_paddingBottomSystemWindowInsets = 8;

    @StyleableRes
    public static final int BottomAppBar_paddingLeftSystemWindowInsets = 9;

    @StyleableRes
    public static final int BottomAppBar_paddingRightSystemWindowInsets = 10;

    @StyleableRes
    public static final int BottomNavigationView_backgroundTint = 0;

    @StyleableRes
    public static final int BottomNavigationView_elevation = 1;

    @StyleableRes
    public static final int BottomNavigationView_itemBackground = 2;

    @StyleableRes
    public static final int BottomNavigationView_itemHorizontalTranslationEnabled = 3;

    @StyleableRes
    public static final int BottomNavigationView_itemIconSize = 4;

    @StyleableRes
    public static final int BottomNavigationView_itemIconTint = 5;

    @StyleableRes
    public static final int BottomNavigationView_itemRippleColor = 6;

    @StyleableRes
    public static final int BottomNavigationView_itemTextAppearanceActive = 7;

    @StyleableRes
    public static final int BottomNavigationView_itemTextAppearanceInactive = 8;

    @StyleableRes
    public static final int BottomNavigationView_itemTextColor = 9;

    @StyleableRes
    public static final int BottomNavigationView_labelVisibilityMode = 10;

    @StyleableRes
    public static final int BottomNavigationView_menu = 11;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_android_elevation = 0;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_backgroundTint = 1;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_draggable = 2;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_expandedOffset = 3;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_fitToContents = 4;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_halfExpandedRatio = 5;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_hideable = 6;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_peekHeight = 7;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_saveFlags = 8;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_skipCollapsed = 9;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_gestureInsetBottomIgnored = 10;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_shapeAppearance = 11;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_shapeAppearanceOverlay = 12;

    @StyleableRes
    public static final int ButtonBarLayout_allowStacking = 0;

    @StyleableRes
    public static final int CardView_android_minWidth = 0;

    @StyleableRes
    public static final int CardView_android_minHeight = 1;

    @StyleableRes
    public static final int CardView_cardBackgroundColor = 2;

    @StyleableRes
    public static final int CardView_cardCornerRadius = 3;

    @StyleableRes
    public static final int CardView_cardElevation = 4;

    @StyleableRes
    public static final int CardView_cardMaxElevation = 5;

    @StyleableRes
    public static final int CardView_cardPreventCornerOverlap = 6;

    @StyleableRes
    public static final int CardView_cardUseCompatPadding = 7;

    @StyleableRes
    public static final int CardView_contentPadding = 8;

    @StyleableRes
    public static final int CardView_contentPaddingBottom = 9;

    @StyleableRes
    public static final int CardView_contentPaddingLeft = 10;

    @StyleableRes
    public static final int CardView_contentPaddingRight = 11;

    @StyleableRes
    public static final int CardView_contentPaddingTop = 12;

    @StyleableRes
    public static final int Chip_android_textAppearance = 0;

    @StyleableRes
    public static final int Chip_android_textColor = 1;

    @StyleableRes
    public static final int Chip_android_ellipsize = 2;

    @StyleableRes
    public static final int Chip_android_maxWidth = 3;

    @StyleableRes
    public static final int Chip_android_text = 4;

    @StyleableRes
    public static final int Chip_android_checkable = 5;

    @StyleableRes
    public static final int Chip_checkedIcon = 6;

    @StyleableRes
    public static final int Chip_checkedIconEnabled = 7;

    @StyleableRes
    public static final int Chip_checkedIconTint = 8;

    @StyleableRes
    public static final int Chip_checkedIconVisible = 9;

    @StyleableRes
    public static final int Chip_chipBackgroundColor = 10;

    @StyleableRes
    public static final int Chip_chipCornerRadius = 11;

    @StyleableRes
    public static final int Chip_chipEndPadding = 12;

    @StyleableRes
    public static final int Chip_chipIcon = 13;

    @StyleableRes
    public static final int Chip_chipIconEnabled = 14;

    @StyleableRes
    public static final int Chip_chipIconSize = 15;

    @StyleableRes
    public static final int Chip_chipIconTint = 16;

    @StyleableRes
    public static final int Chip_chipIconVisible = 17;

    @StyleableRes
    public static final int Chip_chipMinHeight = 18;

    @StyleableRes
    public static final int Chip_chipMinTouchTargetSize = 19;

    @StyleableRes
    public static final int Chip_chipStartPadding = 20;

    @StyleableRes
    public static final int Chip_chipStrokeColor = 21;

    @StyleableRes
    public static final int Chip_chipStrokeWidth = 22;

    @StyleableRes
    public static final int Chip_chipSurfaceColor = 23;

    @StyleableRes
    public static final int Chip_closeIcon = 24;

    @StyleableRes
    public static final int Chip_closeIconEnabled = 25;

    @StyleableRes
    public static final int Chip_closeIconEndPadding = 26;

    @StyleableRes
    public static final int Chip_closeIconSize = 27;

    @StyleableRes
    public static final int Chip_closeIconStartPadding = 28;

    @StyleableRes
    public static final int Chip_closeIconTint = 29;

    @StyleableRes
    public static final int Chip_closeIconVisible = 30;

    @StyleableRes
    public static final int Chip_ensureMinTouchTargetSize = 31;

    @StyleableRes
    public static final int Chip_hideMotionSpec = 32;

    @StyleableRes
    public static final int Chip_iconEndPadding = 33;

    @StyleableRes
    public static final int Chip_iconStartPadding = 34;

    @StyleableRes
    public static final int Chip_rippleColor = 35;

    @StyleableRes
    public static final int Chip_shapeAppearance = 36;

    @StyleableRes
    public static final int Chip_shapeAppearanceOverlay = 37;

    @StyleableRes
    public static final int Chip_showMotionSpec = 38;

    @StyleableRes
    public static final int Chip_textEndPadding = 39;

    @StyleableRes
    public static final int Chip_textStartPadding = 40;

    @StyleableRes
    public static final int ChipGroup_checkedChip = 0;

    @StyleableRes
    public static final int ChipGroup_chipSpacing = 1;

    @StyleableRes
    public static final int ChipGroup_chipSpacingHorizontal = 2;

    @StyleableRes
    public static final int ChipGroup_chipSpacingVertical = 3;

    @StyleableRes
    public static final int ChipGroup_selectionRequired = 4;

    @StyleableRes
    public static final int ChipGroup_singleLine = 5;

    @StyleableRes
    public static final int ChipGroup_singleSelection = 6;

    @StyleableRes
    public static final int CollapsingToolbarLayout_collapsedTitleGravity = 0;

    @StyleableRes
    public static final int CollapsingToolbarLayout_collapsedTitleTextAppearance = 1;

    @StyleableRes
    public static final int CollapsingToolbarLayout_contentScrim = 2;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleGravity = 3;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMargin = 4;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginBottom = 5;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginEnd = 6;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginStart = 7;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginTop = 8;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleTextAppearance = 9;

    @StyleableRes
    public static final int CollapsingToolbarLayout_maxLines = 10;

    @StyleableRes
    public static final int CollapsingToolbarLayout_scrimAnimationDuration = 11;

    @StyleableRes
    public static final int CollapsingToolbarLayout_scrimVisibleHeightTrigger = 12;

    @StyleableRes
    public static final int CollapsingToolbarLayout_statusBarScrim = 13;

    @StyleableRes
    public static final int CollapsingToolbarLayout_title = 14;

    @StyleableRes
    public static final int CollapsingToolbarLayout_titleEnabled = 15;

    @StyleableRes
    public static final int CollapsingToolbarLayout_toolbarId = 16;

    @StyleableRes
    public static final int CollapsingToolbarLayout_Layout_layout_collapseMode = 0;

    @StyleableRes
    public static final int CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier = 1;

    @StyleableRes
    public static final int ColorStateListItem_android_color = 0;

    @StyleableRes
    public static final int ColorStateListItem_android_alpha = 1;

    @StyleableRes
    public static final int ColorStateListItem_alpha = 2;

    @StyleableRes
    public static final int CompoundButton_android_button = 0;

    @StyleableRes
    public static final int CompoundButton_buttonCompat = 1;

    @StyleableRes
    public static final int CompoundButton_buttonTint = 2;

    @StyleableRes
    public static final int CompoundButton_buttonTintMode = 3;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_orientation = 0;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_maxWidth = 1;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_maxHeight = 2;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_minWidth = 3;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_minHeight = 4;

    @StyleableRes
    public static final int ConstraintLayout_Layout_barrierAllowsGoneWidgets = 5;

    @StyleableRes
    public static final int ConstraintLayout_Layout_barrierDirection = 6;

    @StyleableRes
    public static final int ConstraintLayout_Layout_chainUseRtl = 7;

    @StyleableRes
    public static final int ConstraintLayout_Layout_constraintSet = 8;

    @StyleableRes
    public static final int ConstraintLayout_Layout_constraint_referenced_ids = 9;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constrainedHeight = 10;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constrainedWidth = 11;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBaseline_creator = 12;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf = 13;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBottom_creator = 14;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf = 15;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBottom_toTopOf = 16;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintCircle = 17;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintCircleAngle = 18;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintCircleRadius = 19;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintDimensionRatio = 20;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintEnd_toEndOf = 21;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintEnd_toStartOf = 22;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintGuide_begin = 23;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintGuide_end = 24;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintGuide_percent = 25;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_default = 26;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_max = 27;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_min = 28;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_percent = 29;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHorizontal_bias = 30;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle = 31;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHorizontal_weight = 32;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintLeft_creator = 33;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf = 34;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintLeft_toRightOf = 35;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintRight_creator = 36;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintRight_toLeftOf = 37;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintRight_toRightOf = 38;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintStart_toEndOf = 39;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintStart_toStartOf = 40;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintTop_creator = 41;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintTop_toBottomOf = 42;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintTop_toTopOf = 43;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintVertical_bias = 44;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintVertical_chainStyle = 45;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintVertical_weight = 46;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_default = 47;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_max = 48;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_min = 49;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_percent = 50;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_editor_absoluteX = 51;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_editor_absoluteY = 52;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginBottom = 53;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginEnd = 54;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginLeft = 55;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginRight = 56;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginStart = 57;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginTop = 58;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_optimizationLevel = 59;

    @StyleableRes
    public static final int ConstraintLayout_placeholder_content = 0;

    @StyleableRes
    public static final int ConstraintLayout_placeholder_emptyVisibility = 1;

    @StyleableRes
    public static final int ConstraintSet_android_orientation = 0;

    @StyleableRes
    public static final int ConstraintSet_android_id = 1;

    @StyleableRes
    public static final int ConstraintSet_android_visibility = 2;

    @StyleableRes
    public static final int ConstraintSet_android_layout_width = 3;

    @StyleableRes
    public static final int ConstraintSet_android_layout_height = 4;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginLeft = 5;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginTop = 6;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginRight = 7;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginBottom = 8;

    @StyleableRes
    public static final int ConstraintSet_android_maxWidth = 9;

    @StyleableRes
    public static final int ConstraintSet_android_maxHeight = 10;

    @StyleableRes
    public static final int ConstraintSet_android_minWidth = 11;

    @StyleableRes
    public static final int ConstraintSet_android_minHeight = 12;

    @StyleableRes
    public static final int ConstraintSet_android_alpha = 13;

    @StyleableRes
    public static final int ConstraintSet_android_transformPivotX = 14;

    @StyleableRes
    public static final int ConstraintSet_android_transformPivotY = 15;

    @StyleableRes
    public static final int ConstraintSet_android_translationX = 16;

    @StyleableRes
    public static final int ConstraintSet_android_translationY = 17;

    @StyleableRes
    public static final int ConstraintSet_android_scaleX = 18;

    @StyleableRes
    public static final int ConstraintSet_android_scaleY = 19;

    @StyleableRes
    public static final int ConstraintSet_android_rotation = 20;

    @StyleableRes
    public static final int ConstraintSet_android_rotationX = 21;

    @StyleableRes
    public static final int ConstraintSet_android_rotationY = 22;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginStart = 23;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginEnd = 24;

    @StyleableRes
    public static final int ConstraintSet_android_translationZ = 25;

    @StyleableRes
    public static final int ConstraintSet_android_elevation = 26;

    @StyleableRes
    public static final int ConstraintSet_barrierAllowsGoneWidgets = 27;

    @StyleableRes
    public static final int ConstraintSet_barrierDirection = 28;

    @StyleableRes
    public static final int ConstraintSet_chainUseRtl = 29;

    @StyleableRes
    public static final int ConstraintSet_constraint_referenced_ids = 30;

    @StyleableRes
    public static final int ConstraintSet_layout_constrainedHeight = 31;

    @StyleableRes
    public static final int ConstraintSet_layout_constrainedWidth = 32;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBaseline_creator = 33;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBaseline_toBaselineOf = 34;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBottom_creator = 35;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBottom_toBottomOf = 36;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBottom_toTopOf = 37;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintCircle = 38;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintCircleAngle = 39;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintCircleRadius = 40;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintDimensionRatio = 41;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintEnd_toEndOf = 42;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintEnd_toStartOf = 43;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintGuide_begin = 44;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintGuide_end = 45;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintGuide_percent = 46;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_default = 47;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_max = 48;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_min = 49;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_percent = 50;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHorizontal_bias = 51;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHorizontal_chainStyle = 52;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHorizontal_weight = 53;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintLeft_creator = 54;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintLeft_toLeftOf = 55;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintLeft_toRightOf = 56;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintRight_creator = 57;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintRight_toLeftOf = 58;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintRight_toRightOf = 59;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintStart_toEndOf = 60;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintStart_toStartOf = 61;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintTop_creator = 62;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintTop_toBottomOf = 63;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintTop_toTopOf = 64;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintVertical_bias = 65;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintVertical_chainStyle = 66;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintVertical_weight = 67;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_default = 68;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_max = 69;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_min = 70;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_percent = 71;

    @StyleableRes
    public static final int ConstraintSet_layout_editor_absoluteX = 72;

    @StyleableRes
    public static final int ConstraintSet_layout_editor_absoluteY = 73;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginBottom = 74;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginEnd = 75;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginLeft = 76;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginRight = 77;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginStart = 78;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginTop = 79;

    @StyleableRes
    public static final int CoordinatorLayout_keylines = 0;

    @StyleableRes
    public static final int CoordinatorLayout_statusBarBackground = 1;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_anchor = 1;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_behavior = 3;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_keyline = 6;

    @StyleableRes
    public static final int DialogFragmentNavigator_android_name = 0;

    @StyleableRes
    public static final int DrawerArrowToggle_arrowHeadLength = 0;

    @StyleableRes
    public static final int DrawerArrowToggle_arrowShaftLength = 1;

    @StyleableRes
    public static final int DrawerArrowToggle_barLength = 2;

    @StyleableRes
    public static final int DrawerArrowToggle_color = 3;

    @StyleableRes
    public static final int DrawerArrowToggle_drawableSize = 4;

    @StyleableRes
    public static final int DrawerArrowToggle_gapBetweenBars = 5;

    @StyleableRes
    public static final int DrawerArrowToggle_spinBars = 6;

    @StyleableRes
    public static final int DrawerArrowToggle_thickness = 7;

    @StyleableRes
    public static final int ExtendedFloatingActionButton_elevation = 0;

    @StyleableRes
    public static final int ExtendedFloatingActionButton_extendMotionSpec = 1;

    @StyleableRes
    public static final int ExtendedFloatingActionButton_hideMotionSpec = 2;

    @StyleableRes
    public static final int ExtendedFloatingActionButton_showMotionSpec = 3;

    @StyleableRes
    public static final int ExtendedFloatingActionButton_shrinkMotionSpec = 4;

    @StyleableRes
    public static final int ExtendedFloatingActionButton_Behavior_Layout_behavior_autoHide = 0;

    @StyleableRes
    public static final int ExtendedFloatingActionButton_Behavior_Layout_behavior_autoShrink = 1;

    @StyleableRes
    public static final int FloatingActionButton_android_enabled = 0;

    @StyleableRes
    public static final int FloatingActionButton_backgroundTint = 1;

    @StyleableRes
    public static final int FloatingActionButton_backgroundTintMode = 2;

    @StyleableRes
    public static final int FloatingActionButton_borderWidth = 3;

    @StyleableRes
    public static final int FloatingActionButton_elevation = 4;

    @StyleableRes
    public static final int FloatingActionButton_ensureMinTouchTargetSize = 5;

    @StyleableRes
    public static final int FloatingActionButton_fabCustomSize = 6;

    @StyleableRes
    public static final int FloatingActionButton_fabSize = 7;

    @StyleableRes
    public static final int FloatingActionButton_hideMotionSpec = 8;

    @StyleableRes
    public static final int FloatingActionButton_hoveredFocusedTranslationZ = 9;

    @StyleableRes
    public static final int FloatingActionButton_maxImageSize = 10;

    @StyleableRes
    public static final int FloatingActionButton_pressedTranslationZ = 11;

    @StyleableRes
    public static final int FloatingActionButton_rippleColor = 12;

    @StyleableRes
    public static final int FloatingActionButton_shapeAppearance = 13;

    @StyleableRes
    public static final int FloatingActionButton_shapeAppearanceOverlay = 14;

    @StyleableRes
    public static final int FloatingActionButton_showMotionSpec = 15;

    @StyleableRes
    public static final int FloatingActionButton_useCompatPadding = 16;

    @StyleableRes
    public static final int FloatingActionButton_Behavior_Layout_behavior_autoHide = 0;

    @StyleableRes
    public static final int FlowLayout_itemSpacing = 0;

    @StyleableRes
    public static final int FlowLayout_lineSpacing = 1;

    @StyleableRes
    public static final int FontFamily_fontProviderAuthority = 0;

    @StyleableRes
    public static final int FontFamily_fontProviderCerts = 1;

    @StyleableRes
    public static final int FontFamily_fontProviderFetchStrategy = 2;

    @StyleableRes
    public static final int FontFamily_fontProviderFetchTimeout = 3;

    @StyleableRes
    public static final int FontFamily_fontProviderPackage = 4;

    @StyleableRes
    public static final int FontFamily_fontProviderQuery = 5;

    @StyleableRes
    public static final int FontFamilyFont_android_font = 0;

    @StyleableRes
    public static final int FontFamilyFont_android_fontWeight = 1;

    @StyleableRes
    public static final int FontFamilyFont_android_fontStyle = 2;

    @StyleableRes
    public static final int FontFamilyFont_android_ttcIndex = 3;

    @StyleableRes
    public static final int FontFamilyFont_android_fontVariationSettings = 4;

    @StyleableRes
    public static final int FontFamilyFont_font = 5;

    @StyleableRes
    public static final int FontFamilyFont_fontStyle = 6;

    @StyleableRes
    public static final int FontFamilyFont_fontVariationSettings = 7;

    @StyleableRes
    public static final int FontFamilyFont_fontWeight = 8;

    @StyleableRes
    public static final int FontFamilyFont_ttcIndex = 9;

    @StyleableRes
    public static final int ForegroundLinearLayout_android_foreground = 0;

    @StyleableRes
    public static final int ForegroundLinearLayout_android_foregroundGravity = 1;

    @StyleableRes
    public static final int ForegroundLinearLayout_foregroundInsidePadding = 2;

    @StyleableRes
    public static final int FragmentNavigator_android_name = 0;

    @StyleableRes
    public static final int GradientColor_android_startColor = 0;

    @StyleableRes
    public static final int GradientColor_android_endColor = 1;

    @StyleableRes
    public static final int GradientColor_android_type = 2;

    @StyleableRes
    public static final int GradientColor_android_centerX = 3;

    @StyleableRes
    public static final int GradientColor_android_centerY = 4;

    @StyleableRes
    public static final int GradientColor_android_gradientRadius = 5;

    @StyleableRes
    public static final int GradientColor_android_tileMode = 6;

    @StyleableRes
    public static final int GradientColor_android_centerColor = 7;

    @StyleableRes
    public static final int GradientColor_android_startX = 8;

    @StyleableRes
    public static final int GradientColor_android_startY = 9;

    @StyleableRes
    public static final int GradientColor_android_endX = 10;

    @StyleableRes
    public static final int GradientColor_android_endY = 11;

    @StyleableRes
    public static final int GradientColorItem_android_color = 0;

    @StyleableRes
    public static final int GradientColorItem_android_offset = 1;

    @StyleableRes
    public static final int Insets_paddingBottomSystemWindowInsets = 0;

    @StyleableRes
    public static final int Insets_paddingLeftSystemWindowInsets = 1;

    @StyleableRes
    public static final int Insets_paddingRightSystemWindowInsets = 2;

    @StyleableRes
    public static final int LinearConstraintLayout_android_orientation = 0;

    @StyleableRes
    public static final int LinearLayoutCompat_android_gravity = 0;

    @StyleableRes
    public static final int LinearLayoutCompat_android_orientation = 1;

    @StyleableRes
    public static final int LinearLayoutCompat_android_baselineAligned = 2;

    @StyleableRes
    public static final int LinearLayoutCompat_android_baselineAlignedChildIndex = 3;

    @StyleableRes
    public static final int LinearLayoutCompat_android_weightSum = 4;

    @StyleableRes
    public static final int LinearLayoutCompat_divider = 5;

    @StyleableRes
    public static final int LinearLayoutCompat_dividerPadding = 6;

    @StyleableRes
    public static final int LinearLayoutCompat_measureWithLargestChild = 7;

    @StyleableRes
    public static final int LinearLayoutCompat_showDividers = 8;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_gravity = 0;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_width = 1;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_height = 2;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_weight = 3;

    @StyleableRes
    public static final int ListPopupWindow_android_dropDownHorizontalOffset = 0;

    @StyleableRes
    public static final int ListPopupWindow_android_dropDownVerticalOffset = 1;

    @StyleableRes
    public static final int MaterialAlertDialog_backgroundInsetBottom = 0;

    @StyleableRes
    public static final int MaterialAlertDialog_backgroundInsetEnd = 1;

    @StyleableRes
    public static final int MaterialAlertDialog_backgroundInsetStart = 2;

    @StyleableRes
    public static final int MaterialAlertDialog_backgroundInsetTop = 3;

    @StyleableRes
    public static final int MaterialAlertDialogTheme_materialAlertDialogBodyTextStyle = 0;

    @StyleableRes
    public static final int MaterialAlertDialogTheme_materialAlertDialogTheme = 1;

    @StyleableRes
    public static final int MaterialAlertDialogTheme_materialAlertDialogTitleIconStyle = 2;

    @StyleableRes
    public static final int MaterialAlertDialogTheme_materialAlertDialogTitlePanelStyle = 3;

    @StyleableRes
    public static final int MaterialAlertDialogTheme_materialAlertDialogTitleTextStyle = 4;

    @StyleableRes
    public static final int MaterialAutoCompleteTextView_android_inputType = 0;

    @StyleableRes
    public static final int MaterialButton_android_background = 0;

    @StyleableRes
    public static final int MaterialButton_android_insetLeft = 1;

    @StyleableRes
    public static final int MaterialButton_android_insetRight = 2;

    @StyleableRes
    public static final int MaterialButton_android_insetTop = 3;

    @StyleableRes
    public static final int MaterialButton_android_insetBottom = 4;

    @StyleableRes
    public static final int MaterialButton_android_checkable = 5;

    @StyleableRes
    public static final int MaterialButton_backgroundTint = 6;

    @StyleableRes
    public static final int MaterialButton_backgroundTintMode = 7;

    @StyleableRes
    public static final int MaterialButton_cornerRadius = 8;

    @StyleableRes
    public static final int MaterialButton_elevation = 9;

    @StyleableRes
    public static final int MaterialButton_icon = 10;

    @StyleableRes
    public static final int MaterialButton_iconGravity = 11;

    @StyleableRes
    public static final int MaterialButton_iconPadding = 12;

    @StyleableRes
    public static final int MaterialButton_iconSize = 13;

    @StyleableRes
    public static final int MaterialButton_iconTint = 14;

    @StyleableRes
    public static final int MaterialButton_iconTintMode = 15;

    @StyleableRes
    public static final int MaterialButton_rippleColor = 16;

    @StyleableRes
    public static final int MaterialButton_shapeAppearance = 17;

    @StyleableRes
    public static final int MaterialButton_shapeAppearanceOverlay = 18;

    @StyleableRes
    public static final int MaterialButton_strokeColor = 19;

    @StyleableRes
    public static final int MaterialButton_strokeWidth = 20;

    @StyleableRes
    public static final int MaterialButtonToggleGroup_checkedButton = 0;

    @StyleableRes
    public static final int MaterialButtonToggleGroup_selectionRequired = 1;

    @StyleableRes
    public static final int MaterialButtonToggleGroup_singleSelection = 2;

    @StyleableRes
    public static final int MaterialCalendar_android_windowFullscreen = 0;

    @StyleableRes
    public static final int MaterialCalendar_dayInvalidStyle = 1;

    @StyleableRes
    public static final int MaterialCalendar_daySelectedStyle = 2;

    @StyleableRes
    public static final int MaterialCalendar_dayStyle = 3;

    @StyleableRes
    public static final int MaterialCalendar_dayTodayStyle = 4;

    @StyleableRes
    public static final int MaterialCalendar_rangeFillColor = 5;

    @StyleableRes
    public static final int MaterialCalendar_yearSelectedStyle = 6;

    @StyleableRes
    public static final int MaterialCalendar_yearStyle = 7;

    @StyleableRes
    public static final int MaterialCalendar_yearTodayStyle = 8;

    @StyleableRes
    public static final int MaterialCalendarItem_android_insetLeft = 0;

    @StyleableRes
    public static final int MaterialCalendarItem_android_insetRight = 1;

    @StyleableRes
    public static final int MaterialCalendarItem_android_insetTop = 2;

    @StyleableRes
    public static final int MaterialCalendarItem_android_insetBottom = 3;

    @StyleableRes
    public static final int MaterialCalendarItem_itemFillColor = 4;

    @StyleableRes
    public static final int MaterialCalendarItem_itemShapeAppearance = 5;

    @StyleableRes
    public static final int MaterialCalendarItem_itemShapeAppearanceOverlay = 6;

    @StyleableRes
    public static final int MaterialCalendarItem_itemStrokeColor = 7;

    @StyleableRes
    public static final int MaterialCalendarItem_itemStrokeWidth = 8;

    @StyleableRes
    public static final int MaterialCalendarItem_itemTextColor = 9;

    @StyleableRes
    public static final int MaterialCardView_android_checkable = 0;

    @StyleableRes
    public static final int MaterialCardView_cardForegroundColor = 1;

    @StyleableRes
    public static final int MaterialCardView_checkedIcon = 2;

    @StyleableRes
    public static final int MaterialCardView_checkedIconTint = 3;

    @StyleableRes
    public static final int MaterialCardView_rippleColor = 4;

    @StyleableRes
    public static final int MaterialCardView_shapeAppearance = 5;

    @StyleableRes
    public static final int MaterialCardView_shapeAppearanceOverlay = 6;

    @StyleableRes
    public static final int MaterialCardView_state_dragged = 7;

    @StyleableRes
    public static final int MaterialCardView_strokeColor = 8;

    @StyleableRes
    public static final int MaterialCardView_strokeWidth = 9;

    @StyleableRes
    public static final int MaterialCheckBox_buttonTint = 0;

    @StyleableRes
    public static final int MaterialCheckBox_useMaterialThemeColors = 1;

    @StyleableRes
    public static final int MaterialEditText_met_accentTypeface = 0;

    @StyleableRes
    public static final int MaterialEditText_met_autoValidate = 1;

    @StyleableRes
    public static final int MaterialEditText_met_baseColor = 2;

    @StyleableRes
    public static final int MaterialEditText_met_bottomTextSize = 3;

    @StyleableRes
    public static final int MaterialEditText_met_clearButton = 4;

    @StyleableRes
    public static final int MaterialEditText_met_errorColor = 5;

    @StyleableRes
    public static final int MaterialEditText_met_floatingLabel = 6;

    @StyleableRes
    public static final int MaterialEditText_met_floatingLabelAlwaysShown = 7;

    @StyleableRes
    public static final int MaterialEditText_met_floatingLabelAnimating = 8;

    @StyleableRes
    public static final int MaterialEditText_met_floatingLabelPadding = 9;

    @StyleableRes
    public static final int MaterialEditText_met_floatingLabelText = 10;

    @StyleableRes
    public static final int MaterialEditText_met_floatingLabelTextColor = 11;

    @StyleableRes
    public static final int MaterialEditText_met_floatingLabelTextSize = 12;

    @StyleableRes
    public static final int MaterialEditText_met_helperText = 13;

    @StyleableRes
    public static final int MaterialEditText_met_helperTextAlwaysShown = 14;

    @StyleableRes
    public static final int MaterialEditText_met_helperTextColor = 15;

    @StyleableRes
    public static final int MaterialEditText_met_hideUnderline = 16;

    @StyleableRes
    public static final int MaterialEditText_met_iconLeft = 17;

    @StyleableRes
    public static final int MaterialEditText_met_iconPadding = 18;

    @StyleableRes
    public static final int MaterialEditText_met_iconRight = 19;

    @StyleableRes
    public static final int MaterialEditText_met_maxCharacters = 20;

    @StyleableRes
    public static final int MaterialEditText_met_minBottomTextLines = 21;

    @StyleableRes
    public static final int MaterialEditText_met_minCharacters = 22;

    @StyleableRes
    public static final int MaterialEditText_met_primaryColor = 23;

    @StyleableRes
    public static final int MaterialEditText_met_singleLineEllipsis = 24;

    @StyleableRes
    public static final int MaterialEditText_met_textColor = 25;

    @StyleableRes
    public static final int MaterialEditText_met_textColorHint = 26;

    @StyleableRes
    public static final int MaterialEditText_met_typeface = 27;

    @StyleableRes
    public static final int MaterialEditText_met_underlineColor = 28;

    @StyleableRes
    public static final int MaterialRadioButton_buttonTint = 0;

    @StyleableRes
    public static final int MaterialRadioButton_useMaterialThemeColors = 1;

    @StyleableRes
    public static final int MaterialShape_shapeAppearance = 0;

    @StyleableRes
    public static final int MaterialShape_shapeAppearanceOverlay = 1;

    @StyleableRes
    public static final int MaterialTextAppearance_android_lineHeight = 0;

    @StyleableRes
    public static final int MaterialTextAppearance_lineHeight = 1;

    @StyleableRes
    public static final int MaterialTextView_android_textAppearance = 0;

    @StyleableRes
    public static final int MaterialTextView_android_lineHeight = 1;

    @StyleableRes
    public static final int MaterialTextView_lineHeight = 2;

    @StyleableRes
    public static final int MenuGroup_android_enabled = 0;

    @StyleableRes
    public static final int MenuGroup_android_id = 1;

    @StyleableRes
    public static final int MenuGroup_android_visible = 2;

    @StyleableRes
    public static final int MenuGroup_android_menuCategory = 3;

    @StyleableRes
    public static final int MenuGroup_android_orderInCategory = 4;

    @StyleableRes
    public static final int MenuGroup_android_checkableBehavior = 5;

    @StyleableRes
    public static final int MenuItem_android_icon = 0;

    @StyleableRes
    public static final int MenuItem_android_enabled = 1;

    @StyleableRes
    public static final int MenuItem_android_id = 2;

    @StyleableRes
    public static final int MenuItem_android_checked = 3;

    @StyleableRes
    public static final int MenuItem_android_visible = 4;

    @StyleableRes
    public static final int MenuItem_android_menuCategory = 5;

    @StyleableRes
    public static final int MenuItem_android_orderInCategory = 6;

    @StyleableRes
    public static final int MenuItem_android_title = 7;

    @StyleableRes
    public static final int MenuItem_android_titleCondensed = 8;

    @StyleableRes
    public static final int MenuItem_android_alphabeticShortcut = 9;

    @StyleableRes
    public static final int MenuItem_android_numericShortcut = 10;

    @StyleableRes
    public static final int MenuItem_android_checkable = 11;

    @StyleableRes
    public static final int MenuItem_android_onClick = 12;

    @StyleableRes
    public static final int MenuItem_actionLayout = 13;

    @StyleableRes
    public static final int MenuItem_actionProviderClass = 14;

    @StyleableRes
    public static final int MenuItem_actionViewClass = 15;

    @StyleableRes
    public static final int MenuItem_alphabeticModifiers = 16;

    @StyleableRes
    public static final int MenuItem_contentDescription = 17;

    @StyleableRes
    public static final int MenuItem_iconTint = 18;

    @StyleableRes
    public static final int MenuItem_iconTintMode = 19;

    @StyleableRes
    public static final int MenuItem_numericModifiers = 20;

    @StyleableRes
    public static final int MenuItem_showAsAction = 21;

    @StyleableRes
    public static final int MenuItem_tooltipText = 22;

    @StyleableRes
    public static final int MenuView_android_windowAnimationStyle = 0;

    @StyleableRes
    public static final int MenuView_android_itemTextAppearance = 1;

    @StyleableRes
    public static final int MenuView_android_horizontalDivider = 2;

    @StyleableRes
    public static final int MenuView_android_verticalDivider = 3;

    @StyleableRes
    public static final int MenuView_android_headerBackground = 4;

    @StyleableRes
    public static final int MenuView_android_itemBackground = 5;

    @StyleableRes
    public static final int MenuView_android_itemIconDisabledAlpha = 6;

    @StyleableRes
    public static final int MenuView_preserveIconSpacing = 7;

    @StyleableRes
    public static final int MenuView_subMenuArrow = 8;

    @StyleableRes
    public static final int NavAction_android_id = 0;

    @StyleableRes
    public static final int NavAction_destination = 1;

    @StyleableRes
    public static final int NavAction_enterAnim = 2;

    @StyleableRes
    public static final int NavAction_exitAnim = 3;

    @StyleableRes
    public static final int NavAction_launchSingleTop = 4;

    @StyleableRes
    public static final int NavAction_popEnterAnim = 5;

    @StyleableRes
    public static final int NavAction_popExitAnim = 6;

    @StyleableRes
    public static final int NavAction_popUpTo = 7;

    @StyleableRes
    public static final int NavAction_popUpToInclusive = 8;

    @StyleableRes
    public static final int NavArgument_android_name = 0;

    @StyleableRes
    public static final int NavArgument_android_defaultValue = 1;

    @StyleableRes
    public static final int NavArgument_argType = 2;

    @StyleableRes
    public static final int NavArgument_nullable = 3;

    @StyleableRes
    public static final int NavDeepLink_android_autoVerify = 0;

    @StyleableRes
    public static final int NavDeepLink_uri = 1;

    @StyleableRes
    public static final int NavGraphNavigator_startDestination = 0;

    @StyleableRes
    public static final int NavHost_navGraph = 0;

    @StyleableRes
    public static final int NavHostFragment_defaultNavHost = 0;

    @StyleableRes
    public static final int NavInclude_graph = 0;

    @StyleableRes
    public static final int NavigationView_android_background = 0;

    @StyleableRes
    public static final int NavigationView_android_fitsSystemWindows = 1;

    @StyleableRes
    public static final int NavigationView_android_maxWidth = 2;

    @StyleableRes
    public static final int NavigationView_elevation = 3;

    @StyleableRes
    public static final int NavigationView_headerLayout = 4;

    @StyleableRes
    public static final int NavigationView_itemBackground = 5;

    @StyleableRes
    public static final int NavigationView_itemHorizontalPadding = 6;

    @StyleableRes
    public static final int NavigationView_itemIconPadding = 7;

    @StyleableRes
    public static final int NavigationView_itemIconSize = 8;

    @StyleableRes
    public static final int NavigationView_itemIconTint = 9;

    @StyleableRes
    public static final int NavigationView_itemMaxLines = 10;

    @StyleableRes
    public static final int NavigationView_itemShapeAppearance = 11;

    @StyleableRes
    public static final int NavigationView_itemShapeAppearanceOverlay = 12;

    @StyleableRes
    public static final int NavigationView_itemShapeFillColor = 13;

    @StyleableRes
    public static final int NavigationView_itemShapeInsetBottom = 14;

    @StyleableRes
    public static final int NavigationView_itemShapeInsetEnd = 15;

    @StyleableRes
    public static final int NavigationView_itemShapeInsetStart = 16;

    @StyleableRes
    public static final int NavigationView_itemShapeInsetTop = 17;

    @StyleableRes
    public static final int NavigationView_itemTextAppearance = 18;

    @StyleableRes
    public static final int NavigationView_itemTextColor = 19;

    @StyleableRes
    public static final int NavigationView_menu = 20;

    @StyleableRes
    public static final int Navigator_android_label = 0;

    @StyleableRes
    public static final int Navigator_android_id = 1;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardBackgroundColor = 0;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardBottomEdges = 1;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardCornerRadius = 2;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardElevation = 3;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardLeftBottomCorner = 4;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardLeftEdges = 5;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardLeftTopCorner = 6;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardMaxElevation = 7;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardPreventCornerOverlap = 8;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardRightBottomCorner = 9;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardRightEdges = 10;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardRightTopCorner = 11;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardTopEdges = 12;

    @StyleableRes
    public static final int OptRoundCardView_optRoundCardUseCompatPadding = 13;

    @StyleableRes
    public static final int OptRoundCardView_optRoundContentPadding = 14;

    @StyleableRes
    public static final int OptRoundCardView_optRoundContentPaddingBottom = 15;

    @StyleableRes
    public static final int OptRoundCardView_optRoundContentPaddingLeft = 16;

    @StyleableRes
    public static final int OptRoundCardView_optRoundContentPaddingRight = 17;

    @StyleableRes
    public static final int OptRoundCardView_optRoundContentPaddingTop = 18;

    @StyleableRes
    public static final int PinEntryEditText_pinAnimationType = 0;

    @StyleableRes
    public static final int PinEntryEditText_pinBackgroundDrawable = 1;

    @StyleableRes
    public static final int PinEntryEditText_pinBackgroundIsSquare = 2;

    @StyleableRes
    public static final int PinEntryEditText_pinCharacterMask = 3;

    @StyleableRes
    public static final int PinEntryEditText_pinCharacterSpacing = 4;

    @StyleableRes
    public static final int PinEntryEditText_pinLineColors = 5;

    @StyleableRes
    public static final int PinEntryEditText_pinLineStroke = 6;

    @StyleableRes
    public static final int PinEntryEditText_pinLineStrokeSelected = 7;

    @StyleableRes
    public static final int PinEntryEditText_pinRepeatedHint = 8;

    @StyleableRes
    public static final int PinEntryEditText_pinTextBottomPadding = 9;

    @StyleableRes
    public static final int PinLockView_dotDiameter = 0;

    @StyleableRes
    public static final int PinLockView_dotEmptyBackground = 1;

    @StyleableRes
    public static final int PinLockView_dotFilledBackground = 2;

    @StyleableRes
    public static final int PinLockView_dotSpacing = 3;

    @StyleableRes
    public static final int PinLockView_keypadButtonBackgroundDrawable = 4;

    @StyleableRes
    public static final int PinLockView_keypadButtonSize = 5;

    @StyleableRes
    public static final int PinLockView_keypadDeleteButtonDrawable = 6;

    @StyleableRes
    public static final int PinLockView_keypadDeleteButtonPressedColor = 7;

    @StyleableRes
    public static final int PinLockView_keypadDeleteButtonSize = 8;

    @StyleableRes
    public static final int PinLockView_keypadHorizontalSpacing = 9;

    @StyleableRes
    public static final int PinLockView_keypadShowDeleteButton = 10;

    @StyleableRes
    public static final int PinLockView_keypadTextColor = 11;

    @StyleableRes
    public static final int PinLockView_keypadTextSize = 12;

    @StyleableRes
    public static final int PinLockView_keypadVerticalSpacing = 13;

    @StyleableRes
    public static final int PinLockView_pinLength = 14;

    @StyleableRes
    public static final int PopupWindow_android_popupBackground = 0;

    @StyleableRes
    public static final int PopupWindow_android_popupAnimationStyle = 1;

    @StyleableRes
    public static final int PopupWindow_overlapAnchor = 2;

    @StyleableRes
    public static final int PopupWindowBackgroundState_state_above_anchor = 0;

    @StyleableRes
    public static final int RangeSlider_values = 0;

    @StyleableRes
    public static final int RecycleListView_paddingBottomNoButtons = 0;

    @StyleableRes
    public static final int RecycleListView_paddingTopNoTitle = 1;

    @StyleableRes
    public static final int RecyclerView_android_orientation = 0;

    @StyleableRes
    public static final int RecyclerView_android_clipToPadding = 1;

    @StyleableRes
    public static final int RecyclerView_android_descendantFocusability = 2;

    @StyleableRes
    public static final int RecyclerView_fastScrollEnabled = 3;

    @StyleableRes
    public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 4;

    @StyleableRes
    public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 5;

    @StyleableRes
    public static final int RecyclerView_fastScrollVerticalThumbDrawable = 6;

    @StyleableRes
    public static final int RecyclerView_fastScrollVerticalTrackDrawable = 7;

    @StyleableRes
    public static final int RecyclerView_layoutManager = 8;

    @StyleableRes
    public static final int RecyclerView_reverseLayout = 9;

    @StyleableRes
    public static final int RecyclerView_spanCount = 10;

    @StyleableRes
    public static final int RecyclerView_stackFromEnd = 11;

    @StyleableRes
    public static final int ScrimInsetsFrameLayout_insetForeground = 0;

    @StyleableRes
    public static final int ScrollingViewBehavior_Layout_behavior_overlapTop = 0;

    @StyleableRes
    public static final int SearchView_android_focusable = 0;

    @StyleableRes
    public static final int SearchView_android_maxWidth = 1;

    @StyleableRes
    public static final int SearchView_android_inputType = 2;

    @StyleableRes
    public static final int SearchView_android_imeOptions = 3;

    @StyleableRes
    public static final int SearchView_closeIcon = 4;

    @StyleableRes
    public static final int SearchView_commitIcon = 5;

    @StyleableRes
    public static final int SearchView_defaultQueryHint = 6;

    @StyleableRes
    public static final int SearchView_goIcon = 7;

    @StyleableRes
    public static final int SearchView_iconifiedByDefault = 8;

    @StyleableRes
    public static final int SearchView_layout = 9;

    @StyleableRes
    public static final int SearchView_queryBackground = 10;

    @StyleableRes
    public static final int SearchView_queryHint = 11;

    @StyleableRes
    public static final int SearchView_searchHintIcon = 12;

    @StyleableRes
    public static final int SearchView_searchIcon = 13;

    @StyleableRes
    public static final int SearchView_submitBackground = 14;

    @StyleableRes
    public static final int SearchView_suggestionRowLayout = 15;

    @StyleableRes
    public static final int SearchView_voiceIcon = 16;

    @StyleableRes
    public static final int ShapeAppearance_cornerFamily = 0;

    @StyleableRes
    public static final int ShapeAppearance_cornerFamilyBottomLeft = 1;

    @StyleableRes
    public static final int ShapeAppearance_cornerFamilyBottomRight = 2;

    @StyleableRes
    public static final int ShapeAppearance_cornerFamilyTopLeft = 3;

    @StyleableRes
    public static final int ShapeAppearance_cornerFamilyTopRight = 4;

    @StyleableRes
    public static final int ShapeAppearance_cornerSize = 5;

    @StyleableRes
    public static final int ShapeAppearance_cornerSizeBottomLeft = 6;

    @StyleableRes
    public static final int ShapeAppearance_cornerSizeBottomRight = 7;

    @StyleableRes
    public static final int ShapeAppearance_cornerSizeTopLeft = 8;

    @StyleableRes
    public static final int ShapeAppearance_cornerSizeTopRight = 9;

    @StyleableRes
    public static final int ShapeableImageView_shapeAppearance = 0;

    @StyleableRes
    public static final int ShapeableImageView_shapeAppearanceOverlay = 1;

    @StyleableRes
    public static final int ShapeableImageView_strokeColor = 2;

    @StyleableRes
    public static final int ShapeableImageView_strokeWidth = 3;

    @StyleableRes
    public static final int Slider_android_enabled = 0;

    @StyleableRes
    public static final int Slider_android_value = 1;

    @StyleableRes
    public static final int Slider_android_stepSize = 2;

    @StyleableRes
    public static final int Slider_android_valueFrom = 3;

    @StyleableRes
    public static final int Slider_android_valueTo = 4;

    @StyleableRes
    public static final int Slider_haloColor = 5;

    @StyleableRes
    public static final int Slider_haloRadius = 6;

    @StyleableRes
    public static final int Slider_labelBehavior = 7;

    @StyleableRes
    public static final int Slider_labelStyle = 8;

    @StyleableRes
    public static final int Slider_thumbColor = 9;

    @StyleableRes
    public static final int Slider_thumbElevation = 10;

    @StyleableRes
    public static final int Slider_thumbRadius = 11;

    @StyleableRes
    public static final int Slider_tickColor = 12;

    @StyleableRes
    public static final int Slider_tickColorActive = 13;

    @StyleableRes
    public static final int Slider_tickColorInactive = 14;

    @StyleableRes
    public static final int Slider_trackColor = 15;

    @StyleableRes
    public static final int Slider_trackColorActive = 16;

    @StyleableRes
    public static final int Slider_trackColorInactive = 17;

    @StyleableRes
    public static final int Slider_trackHeight = 18;

    @StyleableRes
    public static final int Snackbar_snackbarButtonStyle = 0;

    @StyleableRes
    public static final int Snackbar_snackbarStyle = 1;

    @StyleableRes
    public static final int Snackbar_snackbarTextViewStyle = 2;

    @StyleableRes
    public static final int SnackbarLayout_android_maxWidth = 0;

    @StyleableRes
    public static final int SnackbarLayout_actionTextColorAlpha = 1;

    @StyleableRes
    public static final int SnackbarLayout_animationMode = 2;

    @StyleableRes
    public static final int SnackbarLayout_backgroundOverlayColorAlpha = 3;

    @StyleableRes
    public static final int SnackbarLayout_backgroundTint = 4;

    @StyleableRes
    public static final int SnackbarLayout_backgroundTintMode = 5;

    @StyleableRes
    public static final int SnackbarLayout_elevation = 6;

    @StyleableRes
    public static final int SnackbarLayout_maxActionInlineWidth = 7;

    @StyleableRes
    public static final int Spinner_android_entries = 0;

    @StyleableRes
    public static final int Spinner_android_popupBackground = 1;

    @StyleableRes
    public static final int Spinner_android_prompt = 2;

    @StyleableRes
    public static final int Spinner_android_dropDownWidth = 3;

    @StyleableRes
    public static final int Spinner_popupTheme = 4;

    @StyleableRes
    public static final int StateListDrawable_android_dither = 0;

    @StyleableRes
    public static final int StateListDrawable_android_visible = 1;

    @StyleableRes
    public static final int StateListDrawable_android_variablePadding = 2;

    @StyleableRes
    public static final int StateListDrawable_android_constantSize = 3;

    @StyleableRes
    public static final int StateListDrawable_android_enterFadeDuration = 4;

    @StyleableRes
    public static final int StateListDrawable_android_exitFadeDuration = 5;

    @StyleableRes
    public static final int StateListDrawableItem_android_drawable = 0;

    @StyleableRes
    public static final int SwitchCompat_android_textOn = 0;

    @StyleableRes
    public static final int SwitchCompat_android_textOff = 1;

    @StyleableRes
    public static final int SwitchCompat_android_thumb = 2;

    @StyleableRes
    public static final int SwitchCompat_showText = 3;

    @StyleableRes
    public static final int SwitchCompat_splitTrack = 4;

    @StyleableRes
    public static final int SwitchCompat_switchMinWidth = 5;

    @StyleableRes
    public static final int SwitchCompat_switchPadding = 6;

    @StyleableRes
    public static final int SwitchCompat_switchTextAppearance = 7;

    @StyleableRes
    public static final int SwitchCompat_thumbTextPadding = 8;

    @StyleableRes
    public static final int SwitchCompat_thumbTint = 9;

    @StyleableRes
    public static final int SwitchCompat_thumbTintMode = 10;

    @StyleableRes
    public static final int SwitchCompat_track = 11;

    @StyleableRes
    public static final int SwitchCompat_trackTint = 12;

    @StyleableRes
    public static final int SwitchCompat_trackTintMode = 13;

    @StyleableRes
    public static final int SwitchMaterial_useMaterialThemeColors = 0;

    @StyleableRes
    public static final int TabItem_android_icon = 0;

    @StyleableRes
    public static final int TabItem_android_layout = 1;

    @StyleableRes
    public static final int TabItem_android_text = 2;

    @StyleableRes
    public static final int TabLayout_tabBackground = 0;

    @StyleableRes
    public static final int TabLayout_tabContentStart = 1;

    @StyleableRes
    public static final int TabLayout_tabGravity = 2;

    @StyleableRes
    public static final int TabLayout_tabIconTint = 3;

    @StyleableRes
    public static final int TabLayout_tabIconTintMode = 4;

    @StyleableRes
    public static final int TabLayout_tabIndicator = 5;

    @StyleableRes
    public static final int TabLayout_tabIndicatorAnimationDuration = 6;

    @StyleableRes
    public static final int TabLayout_tabIndicatorColor = 7;

    @StyleableRes
    public static final int TabLayout_tabIndicatorFullWidth = 8;

    @StyleableRes
    public static final int TabLayout_tabIndicatorGravity = 9;

    @StyleableRes
    public static final int TabLayout_tabIndicatorHeight = 10;

    @StyleableRes
    public static final int TabLayout_tabInlineLabel = 11;

    @StyleableRes
    public static final int TabLayout_tabMaxWidth = 12;

    @StyleableRes
    public static final int TabLayout_tabMinWidth = 13;

    @StyleableRes
    public static final int TabLayout_tabMode = 14;

    @StyleableRes
    public static final int TabLayout_tabPadding = 15;

    @StyleableRes
    public static final int TabLayout_tabPaddingBottom = 16;

    @StyleableRes
    public static final int TabLayout_tabPaddingEnd = 17;

    @StyleableRes
    public static final int TabLayout_tabPaddingStart = 18;

    @StyleableRes
    public static final int TabLayout_tabPaddingTop = 19;

    @StyleableRes
    public static final int TabLayout_tabRippleColor = 20;

    @StyleableRes
    public static final int TabLayout_tabSelectedTextColor = 21;

    @StyleableRes
    public static final int TabLayout_tabTextAppearance = 22;

    @StyleableRes
    public static final int TabLayout_tabTextColor = 23;

    @StyleableRes
    public static final int TabLayout_tabUnboundedRipple = 24;

    @StyleableRes
    public static final int TextAppearance_android_textSize = 0;

    @StyleableRes
    public static final int TextAppearance_android_typeface = 1;

    @StyleableRes
    public static final int TextAppearance_android_textStyle = 2;

    @StyleableRes
    public static final int TextAppearance_android_textColor = 3;

    @StyleableRes
    public static final int TextAppearance_android_textColorHint = 4;

    @StyleableRes
    public static final int TextAppearance_android_textColorLink = 5;

    @StyleableRes
    public static final int TextAppearance_android_shadowColor = 6;

    @StyleableRes
    public static final int TextAppearance_android_shadowDx = 7;

    @StyleableRes
    public static final int TextAppearance_android_shadowDy = 8;

    @StyleableRes
    public static final int TextAppearance_android_shadowRadius = 9;

    @StyleableRes
    public static final int TextAppearance_android_fontFamily = 10;

    @StyleableRes
    public static final int TextAppearance_android_textFontWeight = 11;

    @StyleableRes
    public static final int TextAppearance_fontFamily = 12;

    @StyleableRes
    public static final int TextAppearance_fontVariationSettings = 13;

    @StyleableRes
    public static final int TextAppearance_textAllCaps = 14;

    @StyleableRes
    public static final int TextAppearance_textLocale = 15;

    @StyleableRes
    public static final int TextInputEditText_textInputLayoutFocusedRectEnabled = 0;

    @StyleableRes
    public static final int TextInputLayout_android_enabled = 0;

    @StyleableRes
    public static final int TextInputLayout_android_textColorHint = 1;

    @StyleableRes
    public static final int TextInputLayout_android_hint = 2;

    @StyleableRes
    public static final int TextInputLayout_boxBackgroundColor = 3;

    @StyleableRes
    public static final int TextInputLayout_boxBackgroundMode = 4;

    @StyleableRes
    public static final int TextInputLayout_boxCollapsedPaddingTop = 5;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusBottomEnd = 6;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusBottomStart = 7;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusTopEnd = 8;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusTopStart = 9;

    @StyleableRes
    public static final int TextInputLayout_boxStrokeColor = 10;

    @StyleableRes
    public static final int TextInputLayout_boxStrokeErrorColor = 11;

    @StyleableRes
    public static final int TextInputLayout_boxStrokeWidth = 12;

    @StyleableRes
    public static final int TextInputLayout_boxStrokeWidthFocused = 13;

    @StyleableRes
    public static final int TextInputLayout_counterEnabled = 14;

    @StyleableRes
    public static final int TextInputLayout_counterMaxLength = 15;

    @StyleableRes
    public static final int TextInputLayout_counterOverflowTextAppearance = 16;

    @StyleableRes
    public static final int TextInputLayout_counterOverflowTextColor = 17;

    @StyleableRes
    public static final int TextInputLayout_counterTextAppearance = 18;

    @StyleableRes
    public static final int TextInputLayout_counterTextColor = 19;

    @StyleableRes
    public static final int TextInputLayout_endIconCheckable = 20;

    @StyleableRes
    public static final int TextInputLayout_endIconContentDescription = 21;

    @StyleableRes
    public static final int TextInputLayout_endIconDrawable = 22;

    @StyleableRes
    public static final int TextInputLayout_endIconMode = 23;

    @StyleableRes
    public static final int TextInputLayout_endIconTint = 24;

    @StyleableRes
    public static final int TextInputLayout_endIconTintMode = 25;

    @StyleableRes
    public static final int TextInputLayout_errorContentDescription = 26;

    @StyleableRes
    public static final int TextInputLayout_errorEnabled = 27;

    @StyleableRes
    public static final int TextInputLayout_errorIconDrawable = 28;

    @StyleableRes
    public static final int TextInputLayout_errorIconTint = 29;

    @StyleableRes
    public static final int TextInputLayout_errorIconTintMode = 30;

    @StyleableRes
    public static final int TextInputLayout_errorTextAppearance = 31;

    @StyleableRes
    public static final int TextInputLayout_errorTextColor = 32;

    @StyleableRes
    public static final int TextInputLayout_helperText = 33;

    @StyleableRes
    public static final int TextInputLayout_helperTextEnabled = 34;

    @StyleableRes
    public static final int TextInputLayout_helperTextTextAppearance = 35;

    @StyleableRes
    public static final int TextInputLayout_helperTextTextColor = 36;

    @StyleableRes
    public static final int TextInputLayout_hintAnimationEnabled = 37;

    @StyleableRes
    public static final int TextInputLayout_hintEnabled = 38;

    @StyleableRes
    public static final int TextInputLayout_hintTextAppearance = 39;

    @StyleableRes
    public static final int TextInputLayout_hintTextColor = 40;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleContentDescription = 41;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleDrawable = 42;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleEnabled = 43;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleTint = 44;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleTintMode = 45;

    @StyleableRes
    public static final int TextInputLayout_placeholderText = 46;

    @StyleableRes
    public static final int TextInputLayout_placeholderTextAppearance = 47;

    @StyleableRes
    public static final int TextInputLayout_placeholderTextColor = 48;

    @StyleableRes
    public static final int TextInputLayout_prefixText = 49;

    @StyleableRes
    public static final int TextInputLayout_prefixTextAppearance = 50;

    @StyleableRes
    public static final int TextInputLayout_prefixTextColor = 51;

    @StyleableRes
    public static final int TextInputLayout_shapeAppearance = 52;

    @StyleableRes
    public static final int TextInputLayout_shapeAppearanceOverlay = 53;

    @StyleableRes
    public static final int TextInputLayout_startIconCheckable = 54;

    @StyleableRes
    public static final int TextInputLayout_startIconContentDescription = 55;

    @StyleableRes
    public static final int TextInputLayout_startIconDrawable = 56;

    @StyleableRes
    public static final int TextInputLayout_startIconTint = 57;

    @StyleableRes
    public static final int TextInputLayout_startIconTintMode = 58;

    @StyleableRes
    public static final int TextInputLayout_suffixText = 59;

    @StyleableRes
    public static final int TextInputLayout_suffixTextAppearance = 60;

    @StyleableRes
    public static final int TextInputLayout_suffixTextColor = 61;

    @StyleableRes
    public static final int ThemeEnforcement_android_textAppearance = 0;

    @StyleableRes
    public static final int ThemeEnforcement_enforceMaterialTheme = 1;

    @StyleableRes
    public static final int ThemeEnforcement_enforceTextAppearance = 2;

    @StyleableRes
    public static final int Toolbar_android_gravity = 0;

    @StyleableRes
    public static final int Toolbar_android_minHeight = 1;

    @StyleableRes
    public static final int Toolbar_buttonGravity = 2;

    @StyleableRes
    public static final int Toolbar_collapseContentDescription = 3;

    @StyleableRes
    public static final int Toolbar_collapseIcon = 4;

    @StyleableRes
    public static final int Toolbar_contentInsetEnd = 5;

    @StyleableRes
    public static final int Toolbar_contentInsetEndWithActions = 6;

    @StyleableRes
    public static final int Toolbar_contentInsetLeft = 7;

    @StyleableRes
    public static final int Toolbar_contentInsetRight = 8;

    @StyleableRes
    public static final int Toolbar_contentInsetStart = 9;

    @StyleableRes
    public static final int Toolbar_contentInsetStartWithNavigation = 10;

    @StyleableRes
    public static final int Toolbar_logo = 11;

    @StyleableRes
    public static final int Toolbar_logoDescription = 12;

    @StyleableRes
    public static final int Toolbar_maxButtonHeight = 13;

    @StyleableRes
    public static final int Toolbar_menu = 14;

    @StyleableRes
    public static final int Toolbar_navigationContentDescription = 15;

    @StyleableRes
    public static final int Toolbar_navigationIcon = 16;

    @StyleableRes
    public static final int Toolbar_popupTheme = 17;

    @StyleableRes
    public static final int Toolbar_subtitle = 18;

    @StyleableRes
    public static final int Toolbar_subtitleTextAppearance = 19;

    @StyleableRes
    public static final int Toolbar_subtitleTextColor = 20;

    @StyleableRes
    public static final int Toolbar_title = 21;

    @StyleableRes
    public static final int Toolbar_titleMargin = 22;

    @StyleableRes
    public static final int Toolbar_titleMarginBottom = 23;

    @StyleableRes
    public static final int Toolbar_titleMarginEnd = 24;

    @StyleableRes
    public static final int Toolbar_titleMarginStart = 25;

    @StyleableRes
    public static final int Toolbar_titleMarginTop = 26;

    @StyleableRes
    public static final int Toolbar_titleMargins = 27;

    @StyleableRes
    public static final int Toolbar_titleTextAppearance = 28;

    @StyleableRes
    public static final int Toolbar_titleTextColor = 29;

    @StyleableRes
    public static final int Tooltip_android_textAppearance = 0;

    @StyleableRes
    public static final int Tooltip_android_padding = 1;

    @StyleableRes
    public static final int Tooltip_android_layout_margin = 2;

    @StyleableRes
    public static final int Tooltip_android_minWidth = 3;

    @StyleableRes
    public static final int Tooltip_android_minHeight = 4;

    @StyleableRes
    public static final int Tooltip_android_text = 5;

    @StyleableRes
    public static final int Tooltip_backgroundTint = 6;

    @StyleableRes
    public static final int View_android_theme = 0;

    @StyleableRes
    public static final int View_android_focusable = 1;

    @StyleableRes
    public static final int View_paddingEnd = 2;

    @StyleableRes
    public static final int View_paddingStart = 3;

    @StyleableRes
    public static final int View_theme = 4;

    @StyleableRes
    public static final int ViewBackgroundHelper_android_background = 0;

    @StyleableRes
    public static final int ViewBackgroundHelper_backgroundTint = 1;

    @StyleableRes
    public static final int ViewBackgroundHelper_backgroundTintMode = 2;

    @StyleableRes
    public static final int ViewPager2_android_orientation = 0;

    @StyleableRes
    public static final int ViewStubCompat_android_id = 0;

    @StyleableRes
    public static final int ViewStubCompat_android_layout = 1;

    @StyleableRes
    public static final int ViewStubCompat_android_inflatedId = 2;
  }
}
