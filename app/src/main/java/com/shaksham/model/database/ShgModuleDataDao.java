package com.shaksham.model.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "SHG_MODULE_DATA".
*/
public class ShgModuleDataDao extends AbstractDao<ShgModuleData, Long> {

    public static final String TABLENAME = "SHG_MODULE_DATA";

    /**
     * Properties of entity ShgModuleData.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property VillgeCode = new Property(1, String.class, "villgeCode", false, "VILLGE_CODE");
        public final static Property ShgCodeforModule = new Property(2, String.class, "shgCodeforModule", false, "SHG_CODEFOR_MODULE");
        public final static Property ModuleId = new Property(3, String.class, "moduleId", false, "MODULE_ID");
        public final static Property ModuleName = new Property(4, String.class, "moduleName", false, "MODULE_NAME");
        public final static Property ModuleCount = new Property(5, String.class, "moduleCount", false, "MODULE_COUNT");
        public final static Property LanguageId = new Property(6, String.class, "languageId", false, "LANGUAGE_ID");
    }


    public ShgModuleDataDao(DaoConfig config) {
        super(config);
    }
    
    public ShgModuleDataDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"SHG_MODULE_DATA\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"VILLGE_CODE\" TEXT," + // 1: villgeCode
                "\"SHG_CODEFOR_MODULE\" TEXT," + // 2: shgCodeforModule
                "\"MODULE_ID\" TEXT," + // 3: moduleId
                "\"MODULE_NAME\" TEXT," + // 4: moduleName
                "\"MODULE_COUNT\" TEXT," + // 5: moduleCount
                "\"LANGUAGE_ID\" TEXT);"); // 6: languageId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"SHG_MODULE_DATA\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, ShgModuleData entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String villgeCode = entity.getVillgeCode();
        if (villgeCode != null) {
            stmt.bindString(2, villgeCode);
        }
 
        String shgCodeforModule = entity.getShgCodeforModule();
        if (shgCodeforModule != null) {
            stmt.bindString(3, shgCodeforModule);
        }
 
        String moduleId = entity.getModuleId();
        if (moduleId != null) {
            stmt.bindString(4, moduleId);
        }
 
        String moduleName = entity.getModuleName();
        if (moduleName != null) {
            stmt.bindString(5, moduleName);
        }
 
        String moduleCount = entity.getModuleCount();
        if (moduleCount != null) {
            stmt.bindString(6, moduleCount);
        }
 
        String languageId = entity.getLanguageId();
        if (languageId != null) {
            stmt.bindString(7, languageId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, ShgModuleData entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String villgeCode = entity.getVillgeCode();
        if (villgeCode != null) {
            stmt.bindString(2, villgeCode);
        }
 
        String shgCodeforModule = entity.getShgCodeforModule();
        if (shgCodeforModule != null) {
            stmt.bindString(3, shgCodeforModule);
        }
 
        String moduleId = entity.getModuleId();
        if (moduleId != null) {
            stmt.bindString(4, moduleId);
        }
 
        String moduleName = entity.getModuleName();
        if (moduleName != null) {
            stmt.bindString(5, moduleName);
        }
 
        String moduleCount = entity.getModuleCount();
        if (moduleCount != null) {
            stmt.bindString(6, moduleCount);
        }
 
        String languageId = entity.getLanguageId();
        if (languageId != null) {
            stmt.bindString(7, languageId);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public ShgModuleData readEntity(Cursor cursor, int offset) {
        ShgModuleData entity = new ShgModuleData( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // villgeCode
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // shgCodeforModule
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // moduleId
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // moduleName
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // moduleCount
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6) // languageId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, ShgModuleData entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setVillgeCode(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setShgCodeforModule(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setModuleId(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setModuleName(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setModuleCount(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setLanguageId(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(ShgModuleData entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(ShgModuleData entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(ShgModuleData entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
