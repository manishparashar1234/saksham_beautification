package com.shaksham.model.PojoData;

public class MainPojo {
    private String dateView,blockView,block,gp,village,shgsParticipatedView,shgMembersParticipatedView,otherMemberParticipatedView,moduleView;

    public String getDateView() {
        return dateView;
    }

    public void setDateView(String dateView) {
        this.dateView = dateView;
    }

    public String getBlockView() {
        return blockView;
    }

    public void setBlockView(String blockView) {
        this.blockView = blockView;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGp() {
        return gp;
    }

    public void setGp(String gp) {
        this.gp = gp;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getShgsParticipatedView() {
        return shgsParticipatedView;
    }

    public void setShgsParticipatedView(String shgsParticipatedView) {
        this.shgsParticipatedView = shgsParticipatedView;
    }

    public String getShgMembersParticipatedView() {
        return shgMembersParticipatedView;
    }

    public void setShgMembersParticipatedView(String shgMembersParticipatedView) {
        this.shgMembersParticipatedView = shgMembersParticipatedView;
    }

    public String getOtherMemberParticipatedView() {
        return otherMemberParticipatedView;
    }

    public void setOtherMemberParticipatedView(String otherMemberParticipatedView) {
        this.otherMemberParticipatedView = otherMemberParticipatedView;
    }

    public String getModuleView() {
        return moduleView;
    }

    public void setModuleView(String moduleView) {
        this.moduleView = moduleView;
    }
}
